interStimuliColor_sRGB = [0 0 0];
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);

Points = [0/255 64/255 128/255 192/255 255/255]; %RGB points
measures = [1:50]; %Number of measures will be made for each point
all_times = [1:50];
realIntensity = []; %Real luminances we are getting
timeStamps = [];

%Call the C7000 controller (TCP client)
!..\C7000_Controller\netcoreapp3.1\C7000_Test &

%Start the TCP server
tcpObj = tcp_Server_Start(); %Wait here until the client connects


%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

realIntensity = []; %Real luminances we are getting
timeStamps = [];

[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', 2);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1);

wPtr = Screen('OpenWindow', 2, interStimuliColor_sRGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, interStimuliColor_sRGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);


stimImg = zeros(screenSizePix2(2), screenSizePix2(1), 3);


for RGB = Points
    stimImg = stimImg + RGB; %Add the new rgb value to the matrix
    
    stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
    Screen('DrawTexture', wPtr, stimTextureId);
    Screen('Flip', wPtr);
    
    [Y, M, D, H, M, S] = datevec(now);
    time_0 = M*60+S;
    for number = measures
        [average_lx, ~, ~] = C7000_Read(1, tcpObj); %Perform n measures and returun the usefull values
        [Y, M, D, H, M, S] = datevec(now);
        timeStamps = [timeStamps, M*60+S-time_0];
        realIntensity = [realIntensity, average_lx];
    end
    measures = [measures; realIntensity]; %Number of measures will be made for each point
    all_times = [all_times; timeStamps];
    timeStamps = [];
    realIntensity = [];
    
   stimImg = stimImg - RGB; %Substract the rgb value so it goes back to 0
end


avr2 = [];
avr3 = [];
avr4 = [];
avr5 = [];
avr6 = [];

S = std(transpose(measures));
S3 = 3*S; %3sigma of each image

for c = [1:50]
avr2 = [avr2, mean(measures(2,:))];
avr3 = [avr3, mean(measures(3,:))];
avr4 = [avr4, mean(measures(4,:))];
avr5 = [avr5, mean(measures(5,:))];
avr6 = [avr6, mean(measures(6,:))];
end

j = 4;
plot(all_times(j,:), measures(j,:), '-o')
hold on
plot(all_times(j,:), avr4, 'r')
%plot(all_times(j,:), vector_3sigma_plus, 'g')
%plot(all_times(j,:), vector_3sigma_minus, 'g')
title('Iluminances for RGB = [128 128 128]')
legend('Measures', 'Average of all measures')
ylabel('Iluminance [lx]')
xlabel('Time elapsed [s]')


xlim([0 max(all_times(j,:))])
ylim([12.4 12.65])

%Program clossing
sca;
C7000_End_program(tcpObj); %Return the C-7000 to its manual control
fclose(tcpObj); %Close the server
%clear all

%Alarm to indicate the program has finished
%load handel
%sound(y,Fs)




















%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server')
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 9999; %Must be a big number
    fopen(tcpObj); %Wait until the client connects
end

function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    
end

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program
end
        %END HERE
%C-7000 controller Functions