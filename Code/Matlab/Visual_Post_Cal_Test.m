imported_data = csvread('Visual_Calibration_curve.csv');
inputGeneratorValue = imported_data(1,:);
realIntensity_avr_cal = imported_data(2,:);
realIntensity_max_cal = imported_data(3,:);
realIntensity_min_cal = imported_data(4,:);
clear imported_data

num_measures = 5;
all_lux = [0:250:30000];

realIntensity_avr = []; %Real luminances we are getting
realIntensity_max = [];
realIntensity_min = [];

interStimuliColor_sRGB = [0 0 0];
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);

%Call the C7000 controller (TCP client)
!..\C7000_Controller\netcoreapp3.1\C7000_Test &
%Start the TCP server
tcpObj = tcp_Server_Start(); %Wait here until the client connects


%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

wPtr = Screen('OpenWindow', screenNumber, interStimuliColor_sRGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, interStimuliColor_sRGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);

for intensity = all_lux
stimImg = GenerateCalibratedImage(intensity, realIntensity_avr_cal, inputGeneratorValue);
ShowStimuli(stimImg, wPtr);
[average_lx, max_lx, min_lx] = C7000_Read(num_measures, tcpObj);

%Store the useful data for later analisis
realIntensity_avr = [realIntensity_avr, average_lx];
realIntensity_max = [realIntensity_max, max_lx-average_lx];
realIntensity_min = [realIntensity_min, average_lx-min_lx];
end


%Program clossing
sca;
C7000_End_program(tcpObj); %Return the C-7000 to its manual control
fclose(tcpObj); %Close the server

%Data plot: 
errorbar(all_lux, realIntensity_avr, realIntensity_min, realIntensity_max, '-o');
hold on
plot(all_lux, all_lux);
title('Calibration curve (at night)');
legend('Experimental points', 'X=Y curve');
ylabel('Iluminance received [lx]');
xlabel('Iluminance desired [lx]');
hold off



%Desviaci� relativa m�xima: 15.2%
%Error relatiu m�xim: 9.0%
desviacio_min = max((100-(realIntensity_avr - realIntensity_min)*100./realIntensity_avr))
desviacio_max = max((100-(realIntensity_avr - realIntensity_max)*100./realIntensity_avr))

if desviacio_min > desviacio_max
    drm = desviacio_min
else
    drm = desviacio_max
end

error = abs(realIntensity_avr - all_lux)./all_lux*100;
error(1) = 0;
erm = max(error)



function stimImg = GenerateCalibratedImage(desired_lux, realIntensity_avr_cal, inputGeneratorValue)
%Creates an image of $desired_lux [lx] based on the calibration curve
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);
screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

lux_values = abs(realIntensity_avr_cal - desired_lux); %Set $desired_lux as the 0 point
[~,Index1] = min(lux_values(:)); %Find the location of the closest point 
                                     %to 0. Because it is the closest point (in
                                     %the calibration curve) to the
                                     %desired_lux value.

lux_values(Index1) = NaN; %"Remove" the lowest value from the array
[~,Index2] = min(lux_values(:)); %Find the index of the second lowest value

%{
%First order interpolation (y = ax+b)
if Index2 > Index1
    a = (inputGeneratorValue(Index2)-inputGeneratorValue(Index1))/(realIntensity_avr_cal(Index2)-realIntensity_avr_cal(Index1));
    b = inputGeneratorValue(Index2) -(a*realIntensity_avr_cal(Index2));
    if isnan(a)
        a = (inputGeneratorValue(Index2)-inputGeneratorValue(Index1-1))/(realIntensity_avr_cal(Index2)-realIntensity_avr_cal(Index1-1));
    end
else
    a = (inputGeneratorValue(Index1)-inputGeneratorValue(Index2))/(realIntensity_avr_cal(Index1)-realIntensity_avr_cal(Index2));
    b = inputGeneratorValue(Index2) -(a*realIntensity_avr_cal(Index2));
    if isnan(a)
        a = (inputGeneratorValue(Index1)-inputGeneratorValue(Index2-1))/(realIntensity_avr_cal(Index1)-realIntensity_avr_cal(Index2-1));
        b = inputGeneratorValue(Index2-1) -(a*realIntensity_avr_cal(Index2-1));
    end
end
%}
inputIntensity = inputGeneratorValue(Index1);
if isnan(inputIntensity)
    inputIntensity = 0;
elseif inputIntensity > 1
    inputIntensity = 1;
elseif inputIntensity < 0
    inputIntensity = 0;
end
        
stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], inputIntensity, 0);
inputIntensity = 0;

end

function ShowStimuli(stimImg, wPtr)
    %Presents image (stimImg) to the second screen)
    stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
    Screen('DrawTexture', wPtr, stimTextureId);
    Screen('Flip', wPtr);
end


%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server')
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 9999;
    fopen(tcpObj); %Wait until the client connects
end

function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    
end

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program
end
        %END HERE