interStimuliColor_sRGB = [0 0 0];
background_RGB = [1 1 1];

screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);



%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

wPtr = Screen('OpenWindow', screenNumber, interStimuliColor_sRGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, interStimuliColor_sRGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);


stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], 128/255, 0, background_RGB);

stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
Screen('DrawTexture', wPtr, stimTextureId);
Screen('Flip', wPtr);
