num_measures = 10; %Number of measures will be made in each cycle of the calibration

interStimuliColor_sRGB = [0 0 0];
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);

%Call the C7000 controller (TCP client)
!..\C7000_Controller\netcoreapp3.1\C7000_Test &
%Start the TCP server
tcpObj = tcp_Server_Start(); %Wait here until the client connects


%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

wPtr = Screen('OpenWindow', screenNumber, interStimuliColor_sRGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, interStimuliColor_sRGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);


%    >>>>Creation of the calibration curve<<<<
calIntensity = [0:1/255:1]; %Input value to the image generator function
realIntensity_avr = []; %Real luminances we are getting
realIntensity_max = [];
realIntensity_min = [];

for intensity = calIntensity
    %Present new image
    stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], intensity, 0); %Create the visual stimuli
    stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
    Screen('DrawTexture', wPtr, stimTextureId);
    Screen('Flip', wPtr);

    %Perform n measures and returun the usefull values
    [average_lx, max_lx, min_lx] = C7000_Read(num_measures, tcpObj);
        
    %Store the useful data for later analisis
    realIntensity_avr = [realIntensity_avr, average_lx];
    realIntensity_max = [realIntensity_max, max_lx-average_lx];
    realIntensity_min = [realIntensity_min, average_lx-min_lx];    
end

errorbar(calIntensity, realIntensity_avr, realIntensity_min, realIntensity_max, '-o');

%Program clossing
sca;
C7000_End_program(tcpObj); %Return the C-7000 to its manual control
fclose(tcpObj); %Close the server

%Data save
export_me = [calIntensity; realIntensity_avr; realIntensity_max; realIntensity_min];
csvwrite('Visual_Calibration_curve.csv', export_me)












%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server')
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 9999;
    fopen(tcpObj); %Wait until the client connects
end

function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    
end

function [X_1nm, Y_1nm, X_5nm, Y_5nm] = C7000_GetSpectrum(tcpObj)
    %Ask the C-7000 to send the spectrum of the last measure made
    %This functuion does not take any new measures of the light, just sends
    %more data to Matlab
    fwrite(tcpObj, int16(-2), 'int16');
    
    X_1nm = fread(tcpObj, 400, 'int32');
    Y_1nm = fread(tcpObj, 400, 'single');
    X_5nm = fread(tcpObj, 80, 'int32');
    Y_5nm = fread(tcpObj, 80, 'single');    
end

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program
end
        %END HERE