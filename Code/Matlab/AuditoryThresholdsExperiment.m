function AuditoryThresholdsExperiment(participantId, sessionId, testMode)
% Script to run the experiments on auditory thresholds

if (exist('participantId','var')==0) && (exist('sessionId','var')==0)
    % Collect Participant's Information
    titleBar = 'Informaci�n del Participante';
    userPrompt = {'N�mero de Participante','Sesi�n','Modo test'};
	defaultInput = {'test','-1','1'};
    caUserInput = inputdlg(userPrompt, titleBar, 1,defaultInput);
    if isempty(caUserInput),return,end % Bail out if they clicked Cancel.
    participantId = caUserInput{1};
    sessionId = caUserInput{2};
	% Activate or deactivate the test mode
	testMode = str2num(caUserInput{3});
end 

% -1 values indicate when the threshold has to be measured
frequencies = [500, 1000, 2000, 4000, -1];
volumes = [60 75 90 105 -1];
repetitions = 1;
durations = 1;
responseTime = 0.8;

intraStimuliInterval = 1;
interStimuliInterval = 3;

% All possible values to calculate thresholds
allFrequencies = [500:100:6000]; 
% allFrequencies = 1000;
allVolumes = [40:2:120];
% allRepetitions = [1:3];
allRepetitions = 1;

% Folder to store results
currDate = datestr(now, 'yyyy-mm-dd');
path = ['Results/AuditoryThreshold/', participantId, '/'];
mkdir(path);
fileName = [participantId, '_', sessionId, '_', currDate];
% xlswrite([path, fileName, '.xlsx'],{'Freq (Hz)','Volume (amplitude)', '# Repetitions', 'Duration (s)', 'Threshold', 'Var4Threshold'},'A1:F1');

% Define conditions
[gridFreq, gridVol, gridRep] = meshgrid(frequencies, volumes, repetitions);
conditions = cat(2, gridFreq(:), gridVol(:), gridRep(:));

% Remove conditions where more than one threshold has to be measured
aux2Remove = (conditions < 0);
aux2Remove = sum(aux2Remove,2);
rows2Remove = ((aux2Remove > 1) | (aux2Remove < 1));
conditions(logical(rows2Remove),:) = [];

% Shuffle conditions randomly
conditions = conditions(randperm(size(conditions,1)),:);
thresholds = zeros(1,size(conditions,1));

% Show instructions
wPtr = DrawInstructionsPage(testMode);

% Establish key mapping: ESCape aborts, Space to continue after showing the
% instructions
KbName('UnifyKeynames');
SPACE = KbName('space');
keyList = zeros(1,256);
keyCode = keyList;
keyList(SPACE) = 1;

% Participant's response
KbQueueCreate([],keyList);
KbQueueStart;
KbQueueFlush;

% Show instructions while space bar is not pressed
while ~keyCode(SPACE)
	% Check keyboard:
	[isDown, keyCode]=KbQueueCheck;
end

KbQueueFlush;
KbQueueStop;
KbQueueRelease;
KbReleaseWait;

for c = 1:size(conditions,1)
	freq = conditions(c,1);
	vol = conditions(c,2);
	rep = conditions(c,3);
	if freq < 0 % Threshold measured on the frequency
		freq = allFrequencies;
		strVar4Threshold = 'Freq';
		var4Threshold = allFrequencies;
		auxFileName = [fileName,'_',num2str(vol),'dBA_',num2str(rep),'Rep'];
	end
	
	if vol < 0 % Threshold measured on the volume
		vol = allVolumes;
		strVar4Threshold = 'Volume';
		var4Threshold = allVolumes;
		auxFileName = [fileName,'_',num2str(freq),'Hz_',num2str(rep),'Rep'];
	end
	
	if rep < 0 % Threshold measured on the repetitions
		rep = allRepetitions;
		strVar4Threshold = 'Repetitions';
		var4Threshold = allRepetitions;
		auxFileName = [fileName,'_',num2str(vol),'dBA_',num2str(freq),'Hz'];
	end
	
	% Close the displayed screen
	Screen('Close',wPtr);
	
	disp(['Measuring threshold ', num2str(c), ' out of ', num2str(size(conditions,1))]);
	[thresholds(c), history, responses, responseTimes] = AuditoryThresholds_v3(participantId,vol,freq,durations,interStimuliInterval,rep,intraStimuliInterval,responseTime,testMode);
	
	% Save results of this condition
	save([path, auxFileName, '.mat'], 'participantId','c','conditions','durations','intraStimuliInterval','interStimuliInterval','thresholds','strVar4Threshold','history','responses','responseTimes');

	% Save all results
	save([path, fileName, '.mat'], 'participantId','conditions','durations','intraStimuliInterval','interStimuliInterval','thresholds');
	
	% Write threshold in xls file
% 	xlswrite([path, fileName, '.xlsx'], cat(2,conditions(c,:),durations,thresholds(c)), ['A',num2str(c+1),':E',num2str(c+1)]);
% 	xlswrite([path, fileName, '.xlsx'], {strVar4Threshold}, ['F',num2str(c+1),':F',num2str(c+1)]);

	wPtr = DrawBreakPage(testMode);
	WaitSecs(5);
	
% 	% Plot psychometric function
% 	% Get values presented and the number of times they have been presented
% 	uniqueVar4Threshold = unique(data4Psychometric);
% 	nCorrect = zeros(1, length(uniqueVar4Threshold));
% 	nTrialsDone = zeros(1, length(uniqueVar4Threshold));
% 	pCorrectHistory = zeros(1,size(data4Psychometric,2));
% 	
% 	for i = 1:length(uniqueVar4Threshold)
% 		id = data4Psychometric==uniqueVar4Threshold(i);
% 		nTrialsDone(i) = sum(id);
% 		nCorrect(i) = sum(responses4Psychometric(id));
% 		pCorrectHistory(id) = nCorrect(i)/nTrialsDone(i);
% 	end
% 	
% 	pCorrect = nCorrect./nTrialsDone;
% 	
% 	figure,
% 	plot(uniqueVar4Threshold, 100*pCorrect, '-k', 'LineWidth', 2);
% 	hold on;
% 	% Loop through each unique value so each data point can have its own
% 	% size
% 	for i = 1:length(uniqueVar4Threshold)
% 		sz = nTrialsDone(i)*10+5;
% 		scatter(uniqueVar4Threshold(i), 100*pCorrect(i), sz, [0 0 0], 'filled', 'HandleVisibility','off');
% 	end
% % 	hold off;
% 	ylim([0 100]);
% 	xlabel(strVar4Threshold);
% 	ylabel('Percentage of Correct');
% 	
% 	% Fit psychometric function using a Weibull function
% 	[alpha,beta,thresh50] = FitWeibYN(uniqueVar4Threshold, nCorrect, nTrialsDone-nCorrect,median(uniqueVar4Threshold),[]);
% 	xWeibull = [min(uniqueVar4Threshold(:)):((max(uniqueVar4Threshold(:))-min(uniqueVar4Threshold(:)))./10000):max(uniqueVar4Threshold(:))];
% 	yWeibull = ComputeWeibYN(xWeibull,alpha,beta);
% 	plot(xWeibull,yWeibull*100, 'LineWidth', 2);
% 	
% 	% Calculate R-Squared
% 	yWeibullPred = ComputeWeibYN(data4Psychometric,alpha,beta);
% % 	rSquaredWeibull = GoF(pCorrectHistory,yWeibullPred);
% 	rSquaredWeibull = corrcoef(pCorrectHistory,yWeibullPred);
% 	rSquaredWeibull = rSquaredWeibull(2,1)^2;
% 	
% 	disp(['R-Squared of the Weibull model: ', num2str(rSquaredWeibull)]);
% 	
% % 	% Fit psychometric function using a General Linear Model
% % 	[logitCoef,dev] = glmfit(uniqueVar4Threshold,[nCorrect' nTrialsDone'],'binomial','link', 'probit');
% % 	logitFit = glmval(logitCoef,uniqueVar4Threshold,'probit','size',nTrialsDone');
% % 	
% % 	figure,
% % 	plot(uniqueVar4Threshold,pCorrect*100,'bs', uniqueVar4Threshold,logitFit*100,'r-');
% % 	xlabel(strVar4Threshold);
% % 	ylabel('Percent of Discomfort');
% 
% 	% Fit psychometric function using a Cumulative Normal Distribution
% 	[muEst, sigmaEst] = FitCumNormYN(uniqueVar4Threshold, nCorrect, nTrialsDone-nCorrect);
% 	xCum = [min(uniqueVar4Threshold(:)):((max(uniqueVar4Threshold(:))-min(uniqueVar4Threshold(:)))./10000):max(uniqueVar4Threshold(:))];
% 	yCum = ComputeCumNormYN(xCum,muEst,sigmaEst);
% 	plot(xCum,yCum*100, 'Color', [0.65 0.84 0.75], 'LineWidth', 2);
% 	% Calculate the confidence intervals
% 	alphaErrors = [0.01, 0.05, 0.1];
% 	quantileFunction = -sqrt(2).*erfinv(alphaErrors-1); % Mix of https://en.wikipedia.org/wiki/Confidence_interval and https://en.wikipedia.org/wiki/Normal_distribution#Cumulative_distribution_function
% 	ciCumLow = muEst - sigmaEst.*quantileFunction;
% 	ciCumHigh = muEst + sigmaEst.*quantileFunction;
% 	str = '';
% 	for i = 1:size(alphaErrors,2)
% 		str = [str,'alpha = ',num2str(alphaErrors(i)),', CI = [',num2str(ciCumLow(i)),',',num2str(ciCumHigh(i)),']; '];
% 	end
% 	disp(str);
% 	
% 	% Calculate R-Squared
% 	yCumPred = 0.5.*(1+erf((data4Psychometric-muEst)/(sigmaEst*sqrt(2))));
% % 	rSquaredCum = GoF(pCorrectHistory,yCumPred);
% 	rSquaredCum = corrcoef(pCorrectHistory,yCumPred);
% 	rSquaredCum = rSquaredCum(2,1)^2;
% 	
% 	disp(['R-Squared of the Cumulative model: ', num2str(rSquaredCum)]);
end

save([path, fileName, '.mat'], 'conditions', 'thresholds');

% Clear the screen
Screen('Close',wPtr);

% Draw instructions page
function wPtr = DrawInstructionsPage(testMode)

	% Open window with a fixation point
	screenColor = [128 128 128];
	screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
	screenFreqTolerance = 5;
	screenNumber = max(Screen('Screens')); %0: single display setting
	screenNumber = 1;
	screenSize = zeros(1,2);
	[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
	if testMode
		PsychDebugWindowConfiguration(0, 0.25)
% 	        wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	else
		clear Screen;
		Screen('Preference', 'ConserveVRAM', 4096);
		Screen('Preference', 'SkipSyncTests', 1 );
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	end

	% Get timing information from screen
	screenRefreshRate = Screen('GetFlipInterval', wPtr);
	disp(['Screen rate: ' num2str(screenRefreshRate)]);

	ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

	Screen('FillRect', wPtr, screenColor);

	% Check the monitor's configuration
	if ~testMode
		if (ScreenFreqDiscrepancy < screenFreqTolerance)
			disp('Refresh rate is the desired one');
		else
			disp(['Refresh frequency should be ' screenFreq]);
			error('Screen was not propperly configured');
		end
	end

	% Set font style in window
	tfont='Arial';
	tsize=15;
	tstyle=1; %1=normal 2=bold 3=italic 4=underline
	vSpacing = 3;
	Screen('TextFont', wPtr, tfont);
	Screen('TextSize', wPtr, tsize);
	Screen('TextStyle', wPtr, tstyle);
	
    % Get coordinates in the window
    windowSize = zeros(1,2);
    [windowSize(1), windowSize(2)] = Screen('WindowSize', wPtr);
    windowCenter = zeros(1,2);
    windowCenter(1) = floor(windowSize(1)/2);
    windowCenter(2) = floor(windowSize(2)/2);

    screenNumber = Screen('WindowScreenNumber', wPtr);

    % Define black and white
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
	
	strInstructions = 'En este experimento se le presentar� un est�mulo auditivo y debe indicar si le causa molestia o no. En caso que le cause molestia, debe pulsar la tecla que se indica como "Molesto", en caso contrario, debe pulsar la tecla "NO Molesto". Debe indicar su respuesta lo m�s r�pido posible cuando la cruz de la pantalla sea de color negro (indicar� que el est�mulo ha terminado de ser presentado). La tecla de la derecha corresponde a la tecla "F" y la de la izquierda a la tecla "A".';
	strInstructions2 = 'Pulse la barra espaciadora para continuar';
	
	DrawFormattedText(wPtr,strInstructions,'center',windowSize(2)*0.25,black, 80, 0, 0, vSpacing);
	DrawFormattedText(wPtr,strInstructions2,'center',windowSize(2)*0.66,black, 80, 0, 0, vSpacing);

    % Flip to the screen
    Screen('Flip', wPtr);


% Draw page to do a mini-break
function wPtr = DrawBreakPage(testMode)
	
	% Open window with a fixation point
	screenColor = [128 128 128];
	screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
	screenFreqTolerance = 5;
	screenNumber = max(Screen('Screens')); %0: single display setting
	screenSize = zeros(1,2);
	[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
	if testMode
		PsychDebugWindowConfiguration(0, 0.25)
% 	        wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	else
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	end

	% Get timing information from screen
	screenRefreshRate = Screen('GetFlipInterval', wPtr);
	disp(['Screen rate: ' num2str(screenRefreshRate)]);

	ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

	Screen('FillRect', wPtr, screenColor);

	% Check the monitor's configuration
	if ~testMode
		if (ScreenFreqDiscrepancy < screenFreqTolerance)
			disp('Refresh rate is the desired one');
		else
			disp(['Refresh frequency should be ' screenFreq]);
			error('Screen was not propperly configured');
		end
	end

	% Set font style in window
	tfont='Arial';
	tsize=14;
	tstyle=1; %1=normal 2=bold 3=italic 4=underline
	Screen('TextFont', wPtr, tfont);
	Screen('TextSize', wPtr, tsize);
	Screen('TextStyle', wPtr, tstyle);
	
    % Get coordinates in the window
    windowSize = zeros(1,2);
    [windowSize(1), windowSize(2)] = Screen('WindowSize', wPtr);
    windowCenter = zeros(1,2);
    windowCenter(1) = floor(windowSize(1)/2);
    windowCenter(2) = floor(windowSize(2)/2);

    screenNumber = Screen('WindowScreenNumber', wPtr);

    % Define black and white
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
	
	DrawFormattedText(wPtr,'5 segundos de pausa...','center','center',black);

    % Flip to the screen
    Screen('Flip', wPtr);