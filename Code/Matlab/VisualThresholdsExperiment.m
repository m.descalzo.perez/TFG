function [threshold, history, responsesStaircase, reactionTimes] = VisualThresholdsExperiment()
% Calculate discomfort thresholds from visual stimuli. A light with a mean
% intensity = meanIntensity will be presented during duration (in secs)
% and repeated = number of repetitions. The participant would response
% whether it causes dicomfort or not.
% The threshold will be measured over the mean intensity

	% Parameters to set before experiment
	viewingDistanceCm = 70; % in cm
	screenSizeCm = [50 30]; % width and height in cm
	
	% Experiment parameters	
	minIntensity = 0; %lx
	maxIntensity = 5000; %lx
	initIntensity = 2000; %lx
	stepSizeIntensity = 250; %lx
	interStimuliColor_sRGB = [0 0 0];
	
	stimulusDuration = 0.2;
	interStimuliInterval = 0;
	intraStimuliInterval = 0;
	repetitions = 1;
	responseDuration = 1; % in secs
	
	fixationSizeDeg = 0.5; % in visual degrees
	buttonsSeparationDeg = 1; % in degrees
	
	fixationColor_sRGB = [0.5 0.5 0.5];
	
	enableCorrections = 1;
    
    %Import calibration curve
    imported_data = csvread('Visual_Calibration_curve.csv');
    inputGeneratorValue = imported_data(1,:);
    realIntensity_avr_cal = imported_data(2,:);
    realIntensity_max_cal = imported_data(3,:);
    realIntensity_min_cal = imported_data(4,:);
    clear imported_data
    
	% Establish key mapping: ESCape aborts, Space toggles between auto-
	% movement of sound source or user mouse controlled movement:
	KbName('UnifyKeynames');
	SPACE = KbName('space');
	ESC = KbName('ESCAPE');
	AKEY = KbName('A');
	FKEY = KbName('F');
	keyList = zeros(1,256);
	keyList(SPACE) = 1;
	keyList(ESC) = 1;
	keyList(AKEY) = 1;
	keyList(FKEY) = 1;
    
    % Open serial port
    delete(instrfind);
    s = serial("COM6", 'BaudRate', 115200, 'TimeOut', 5, 'Terminator', 'LF');
    fopen(s);
	
	% Collect Participant's Information
    titleBar = 'Informaci�n del Participante';
    userPrompt = {'N�mero de Participante','Sesi�n','Modo test','Simular respuestas?'};
	defaultInput = {'test','-1','1','0'};
    caUserInput = inputdlg(userPrompt, titleBar, 1,defaultInput);
    if isempty(caUserInput),return,end % Bail out if they clicked Cancel.
    participantId = caUserInput{1};
    sessionId = caUserInput{2};
	% Activate or deactivate the test mode
	testMode = str2num(caUserInput{3});
	simulateResponses = str2num(caUserInput{4});
	
	interStimuliColor_sRGB = uint8(interStimuliColor_sRGB.*255);
	fixationColor_sRGB = uint8(fixationColor_sRGB.*255);
	
	% Open window and transform units from degrees to pixels
	[wPtr, tFlip] = InitWindow(interStimuliColor_sRGB, testMode);
	
	screenSizePix = zeros(1,2);
	[screenSizePix(1) screenSizePix(2)] = Screen('WindowSize', wPtr);
	pixelsPerDegree = (screenSizePix./2) ./ (atan((screenSizeCm./2)./viewingDistanceCm).*360/(2*pi));
	
	if ~ismembertol(pixelsPerDegree(1),pixelsPerDegree(2))
		warning('Monitor size is not coherent');
	end
	
	fixationSizePix = round(fixationSizeDeg*pixelsPerDegree(1)); % in pixels
	buttonsSeparationPix = round(buttonsSeparationDeg*pixelsPerDegree(1)); % in pixels
    
	% Generate the stimulus presented during the intrastimuli interval
	blankImg = zeros(screenSizePix(2), screenSizePix(1), length(interStimuliColor_sRGB));
	for i = 1:length(interStimuliColor_sRGB)
		blankImg(:,:,i) = interStimuliColor_sRGB(i);
	end
	blankImg = DrawFixationPointInImage(uint8(blankImg.*255), fixationSizePix, fixationColor_sRGB);
	blankTextureId = Screen('MakeTexture', wPtr, blankImg);
	
	[wPtr, tFixation] = DrawFixationPoint(wPtr, fixationSizePix, fixationColor_sRGB, 0, [], buttonsSeparationPix, interStimuliColor_sRGB, 0, tFlip, testMode);
	
	% Set general parameters of SIAM method
	possibleValues = [minIntensity:stepSizeIntensity:maxIntensity];
	initValue = initIntensity;
	stepSize = [16 8 4 2 1];
	trialStepSize = [0:1:size(stepSize,2)-1];
	nReversals = 12;
	nReversalTrials = 4;
	nMaxTrials = 300;
	[nextValue, isFinished, finalValue, methodState] = psychMethodSIAM(possibleValues,initValue,stepSize,trialStepSize,nReversals,nReversalTrials,nMaxTrials);
  
    % Show instructions
    wPtr = DrawInstructionsPage(testMode);
    KbWait;
    
	% To test the script
	if testMode
		warning('Test mode is activated');
		simulatedResponses = zeros(1,nMaxTrials);
		% responses(1:2:end) = 1;
		simulatedResponses((randi(2,1,nMaxTrials)-1)==1) = 1;
	end

	reactionTimes = ones(2, nMaxTrials).*(-1);
	responses = ones(2, nMaxTrials).*(-1);
	trial = 1;
	while ~isFinished
		% Create stimulus to be presented
		stimImg = GenerateCalibratedImage(nextValue, realIntensity_avr_cal, inputGeneratorValue);
        stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
        Screen('DrawTexture', wPtr, stimTextureId);
        Screen('Flip', wPtr);
        whenStimulus = tFixation + interStimuliInterval;
		
		% Set randmonly the key that corresponds to discomfort and
		% non-discomfort, respective
		auxRand = rand;
		if auxRand < 0.5
			aKey = 0; % 'A' key non-discomfort
			fKey = 1; % 'F' key discomfort
			buttonsOrder = [0, 1];
		else
			aKey = 1; % 'A' key discomfort
			fKey = 0; % 'F' key non-discomfort
			buttonsOrder = [1, 0];
		end
		
		% Start playback for these sources:
		for r = 1:repetitions
			Screen('DrawTexture', wPtr, stimTextureId);
			tStimulus = Screen('Flip', wPtr, whenStimulus-tFlip);

			if r~=repetitions
				whenFixation = tStimulus + stimulusDuration;
				Screen('DrawTexture', wPtr, blankTextureId);
				tFixation = Screen('Flip', wPtr, whenFixation-tFlip);
			end
			
			whenStimulus = tFixation + intraStimuliInterval;
		end
		
		% Indicate that's time to respond
		whenResponse = tStimulus + stimulusDuration;
		[wPtr, tStartResponse] = DrawFixationPoint(wPtr, fixationSizePix, fixationColor_sRGB, 1, buttonsOrder, buttonsSeparationPix, interStimuliColor_sRGB, whenResponse, tFlip, testMode);
		
		% Delete stimulus texture
		Screen('Close', stimTextureId);

		% Participant's response
		KbQueueCreate([],keyList);
		KbQueueStart;
		KbQueueFlush;
		timeOut = 0;
		isDown = 0;
		while ((~isDown) && (~timeOut))
			% Check keyboard:
			[isDown, keyCode]=KbQueueCheck;

			% For test purposes
			if testMode
				if simulateResponses
					isDown = 1;
					if simulatedResponses(methodState.num_current_trial)
						keyCode(FKEY) = 1;
					else
						keyCode(AKEY) = 1;
					end
				end
            end
            
            %READ THE SAFETY ADD-ON HERE
                %if not safe -> break everything and close with a warning
                if(s.BytesAvailable > 0)
                    if(fgets(s) == "h")
                       warning("Stimulus not safe.");
                       break;
                    end
                end
                
			% Get participant's response
			if isDown
				tReaction = GetSecs;
				KbQueueFlush;
				
				% Check correction
				if enableCorrections
					isDownCorrection = 0;
					while ((~isDownCorrection) && (~timeOut))
						% Check keyboard:
						[isDownCorrection, keyCodeCorrection]=KbQueueCheck;

						if isDownCorrection
							tReactionCorrection = GetSecs;

							% Save reaction time of the correction
							reactionTimes(2,trial) = tReactionCorrection-tStartResponse;

							if keyCodeCorrection(AKEY) > 0
								if aKey % A key means discomfort
									responses(2,trial) = 1;
								else
									responses(2,trial) = 0;
								end
							end

							if keyCodeCorrection(FKEY) > 0
								if fKey % F key means discomfort
									responses(2,trial) = 1;
								else
									responses(2,trial) = 0;
								end
							end
						end

						if ((~isDownCorrection) && ((GetSecs-tStartResponse) > responseDuration))
							responses(2,trial) = 2;
							reactionTimes(2,trial) = -1;
							timeOut = 1;
						end
					end					
				end % end enableCorrections
				
				if keyCode(ESC) > 0
					warning('Operation terminated by user');
					break;
				end
				
				% Save reaction time
				reactionTimes(1,trial) = tReaction-tStartResponse;

				if keyCode(AKEY) > 0
					if aKey % A key means discomfort
						responses(1,trial) = 1;
					else
						responses(1,trial) = 0;
					end
				end

				if keyCode(FKEY) > 0
					if fKey % F key means discomfort
						responses(1,trial) = 1;
					else
						responses(1,trial) = 0;
					end
				end
				
				if responses(2,trial)==2 % There is no correction done
					[nextValue, isFinished, finalValue, methodState] = psychMethodSIAM(responses(1,trial), methodState);
					
					if responses(1,trial) == 1
						disp(['Stimulus is discomfort: Next value is ', num2str(nextValue)]);
					else
						disp(['Stimulus is NOT discomfort: Next value is ', num2str(nextValue)]);
					end
				else
					disp(['Correction done: Next value is ', num2str(nextValue)]);
				end
				
				KbQueueFlush;
				KbQueueStop;
				KbQueueRelease;
				KbReleaseWait;
			end

			% If the participant has not responded during the response window,
			% it is considered as a missed response and the stimulus is
			% presented again
			if ((~isDown) && ((GetSecs-tStartResponse) > responseDuration))
				responses(1,trial) = 2;
				reactionTimes(trial) = -1;
				disp(['Missed response: Next value is ', num2str(nextValue)]);
				timeOut = 1;
				KbQueueFlush;
				KbQueueStop;
				KbQueueRelease;
				KbReleaseWait;
			end
		end
		
		[wPtr, tFixation] = DrawFixationPoint(wPtr, fixationSizePix, fixationColor_sRGB, 0, [], buttonsSeparationPix, interStimuliColor_sRGB, 0, tFlip, testMode);

		if keyCode(ESC)
			break;
		end

		% Update the value according to the response
		trial = trial + 1;
	end
	
	Screen('Close', wPtr);
    % Close the serial port
    fclose(s);
    delete(s);
    clear s
	
	responses(trial:end) = [];
	reactionTimes(trial:end) = [];

	fig = figure;
	stairs([1:size(methodState.history,2)], methodState.history, 'LineWidth', 2);
	title('Visual Threshold');
	xlabel('# Trial');
	ylabel('Contrast');
	fileName = [participantId,'_VisualThreshold_',num2str(stimulusDuration),'s',num2str(repetitions),'Repetitions_',datestr(now,'dd-mm-yyyy_HH.MM.SS')];
	
	% Output variables
	threshold = finalValue;
	history = methodState.history;
	responsesStaircase = methodState.responses;
	disp(['The threshold is defined at contrast = ', num2str(threshold)]);
	
	% Save results
	pathName = ['Results/VisualThreshold/',participantId,'/'];
	mkdir(pathName);
	saveas(fig,[pathName,fileName,'.png']);
	save([pathName,fileName,'.mat'],'participantId','minIntensity','maxIntensity','initIntensity','stepSizeIntensity','stimulusDuration','interStimuliInterval','repetitions','intraStimuliInterval','responseDuration','responses','reactionTimes','methodState','threshold','history','responsesStaircase');	

	% Wait a bit:
	WaitSecs(0.1);
	
	sca;

	% Done
	return;

function [wPtr, tFlip] = InitWindow(screenColor_RGB, testMode)

	% Open window
	screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
	screenFreqTolerance = 5;
	screenNumber = max(Screen('Screens')); %0: single display setting
	screenSize = zeros(1,2);
	[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
	if testMode
		PsychDebugWindowConfiguration(0, 0.25)
%         wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		wPtr = Screen('OpenWindow',screenNumber,screenColor_RGB);
	else
		Screen('Preference', 'ConserveVRAM', 4096);
% 			Screen('Preference', 'SkipSyncTests', 0);
		Screen('Preference', 'SkipSyncTests', 1);
		wPtr = Screen('OpenWindow',screenNumber,screenColor_RGB);
	end

	% Get timing information from screen
	screenRefreshRate = Screen('GetFlipInterval', wPtr);
	disp(['Screen rate: ' num2str(screenRefreshRate)]);

	ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

	Screen('FillRect', wPtr, screenColor_RGB);

	% Check the monitor's configuration
	if ~testMode
		if (ScreenFreqDiscrepancy < screenFreqTolerance)
			disp('Refresh rate is the desired one');
		else
			disp(['Refresh frequency should be ' screenFreq]);
% 				error('Screen was not propperly configured');
		end
	end

	% Set font style in window
	tfont='Arial';
	tsize=14;
	tstyle=1; %1=normal 2=bold 3=italic 4=underline
	Screen('TextFont', wPtr, tfont);
	Screen('TextSize', wPtr, tsize);
	Screen('TextStyle', wPtr, tstyle);
	
    % Flip to the screen
    [tFlipStart,~,tFlipEnd] = Screen('Flip', wPtr);
	tFlip = tFlipEnd-tFlipStart;

function img = DrawFixationPointInImage(img, fixationSizePix, fixation_sRGB)
	
	% Get coordinates in the image
	imgCenter = floor(size(img)/2);
	
	% Here we set the size of the arms of our fixation cross
	fixCrossDimPix = round(fixationSizePix/2);
	% Set the line width for our fixation cross
	lineWidthPix = 4;
	
	% Draw fixation point
	img(imgCenter(1)-fixCrossDimPix:imgCenter(1)+fixCrossDimPix,imgCenter(2)-lineWidthPix/2:imgCenter(2)+lineWidthPix/2,1) = fixation_sRGB(1);
	img(imgCenter(1)-fixCrossDimPix:imgCenter(1)+fixCrossDimPix,imgCenter(2)-lineWidthPix/2:imgCenter(2)+lineWidthPix/2,2) = fixation_sRGB(2);
	img(imgCenter(1)-fixCrossDimPix:imgCenter(1)+fixCrossDimPix,imgCenter(2)-lineWidthPix/2:imgCenter(2)+lineWidthPix/2,3) = fixation_sRGB(3);
	img(imgCenter(1)-lineWidthPix/2:imgCenter(1)+lineWidthPix/2,imgCenter(2)-fixCrossDimPix:imgCenter(2)+fixCrossDimPix,1) = fixation_sRGB(1);
	img(imgCenter(1)-lineWidthPix/2:imgCenter(1)+lineWidthPix/2,imgCenter(2)-fixCrossDimPix:imgCenter(2)+fixCrossDimPix,2) = fixation_sRGB(2);
	img(imgCenter(1)-lineWidthPix/2:imgCenter(1)+lineWidthPix/2,imgCenter(2)-fixCrossDimPix:imgCenter(2)+fixCrossDimPix,3) = fixation_sRGB(3);
	
function [wPtr, tFixation] = DrawFixationPoint(wPtr, fixationSizePix, fixation_sRGB, showButtons, buttonsOrder, buttonsSeparationPix, background_sRGB, whenFixation, tFlip, testMode)
	
	if exist('wPtr','var')==0 || isempty(wPtr)
		% Open window with a fixation point
		screenColor = [128 128 128];
		screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
		screenFreqTolerance = 5;
		screenNumber = max(Screen('Screens')); %0: single display setting
		screenSize = zeros(1,2);
		[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
		if testMode
			PsychDebugWindowConfiguration(0, 0.25)
			wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		else
			wPtr = Screen('OpenWindow',screenNumber,screenColor);
		end

		% Get timing information from screen
		screenRefreshRate = Screen('GetFlipInterval', wPtr);
		disp(['Screen rate: ' num2str(screenRefreshRate)]);

		ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

		Screen('FillRect', wPtr, screenColor);

		% Check the monitor's configuration
		if ~testMode
			assert(isequal(screenSize, windowSize));
			if (ScreenFreqDiscrepancy < screenFreqTolerance)
				disp('Refresh rate is the desired one');
			else
				disp(['Refresh frequency should be ' screenFreq]);
				error('Screen was not propperly configured');
			end
		end

		% Set font style in window
		tfont='Arial';
		tsize=14;
		tstyle=1; %1=normal 2=bold 3=italic 4=underline
		Screen('TextFont', wPtr, tfont);
		Screen('TextSize', wPtr, tsize);
		Screen('TextStyle', wPtr, tstyle);
	else
		screenNumber = Screen('WindowScreenNumber',wPtr);
	end
	
	if tFlip > whenFixation
		tFlip = 0;
	end
	
	Screen('FillRect', wPtr, background_sRGB);
	
	% Get coordinates in the window
	windowSize = zeros(1,2);
	[windowSize(1), windowSize(2)] = Screen('WindowSize', wPtr);
	windowCenter = zeros(1,2);
	windowCenter(1) = floor(windowSize(1)/2);
	windowCenter(2) = floor(windowSize(2)/2);

	% Here we set the size of the arms of our fixation cross
	fixCrossDimPix = round(fixationSizePix/2);

	% Now we set the coordinates (these are all relative to zero we will let
	% the drawing routine center the cross in the center of our monitor for us)
	xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
	yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
	allCoords = [xCoords; yCoords];

	% Set the line width for our fixation cross
	lineWidthPix = 4;

	% Draw the fixation cross in white, set it to the center of our screen and
	% set good quality antialiasing
	Screen('DrawLines', wPtr, allCoords, lineWidthPix, fixation_sRGB, windowCenter);
	
	% Draw the buttons to guide the participant
	if showButtons		
		% Set font style in window
		tsize=20;
		Screen('TextSize', wPtr, tsize);
		
		if buttonsOrder(1)
			strLeft = 'Molesto';
			strRight = 'NO Molesto';
		else
			strLeft = 'NO Molesto';
			strRight = 'Molesto';
		end
		
		xTextSizePix = tsize*max(strlength(strLeft),strlength(strRight));
		yTextSizePix = round(tsize/2);
		
		DrawFormattedText(wPtr,strLeft,'right',windowCenter(2)+fixCrossDimPix,fixation_sRGB,[],[],[],[],0,[windowCenter(1)-xTextSizePix-buttonsSeparationPix, windowCenter(2)-yTextSizePix, windowCenter(1)-buttonsSeparationPix, windowCenter(2)+yTextSizePix]);
		DrawFormattedText(wPtr,strRight,windowCenter(1)+buttonsSeparationPix,windowCenter(2)+fixCrossDimPix,fixation_sRGB,[],[],[],[],0,[windowCenter(1)+buttonsSeparationPix, windowCenter(2)-yTextSizePix, windowCenter(1)+xTextSizePix+buttonsSeparationPix, windowCenter(2)+yTextSizePix]);
	end

	% Flip to the screen
   	tFixation = Screen('Flip', wPtr, whenFixation-tFlip);

   
function stimImg = GenerateCalibratedImage(desired_lux, realIntensity_avr_cal, inputGeneratorValue)
    %Creates an image of $desired_lux [lx] based on the calibration curve
    screenSizePix2 = zeros(1,2);
    screenSizePix1 = zeros(1,2);
    screenNumber = max(Screen('Screens')); %0: single display setting
    [screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
    [screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

    lux_values = abs(realIntensity_avr_cal - desired_lux); %Set $desired_lux as the 0 point
    [~,Index1] = min(lux_values(:)); %Find the location of the closest point 
                                         %to 0. Because it is the closest point (in
                                         %the calibration curve) to the
                                         %desired_lux value.

    lux_values(Index1) = NaN; %"Remove" the lowest value from the array
    [~,Index2] = min(lux_values(:)); %Find the index of the second lowest value

    %{
    %First order interpolation (y = ax+b)
    if Index2 > Index1
        a = (inputGeneratorValue(Index2)-inputGeneratorValue(Index1))/(realIntensity_avr_cal(Index2)-realIntensity_avr_cal(Index1));
        b = inputGeneratorValue(Index2) -(a*realIntensity_avr_cal(Index2));
        if isnan(a)
            a = (inputGeneratorValue(Index2)-inputGeneratorValue(Index1-1))/(realIntensity_avr_cal(Index2)-realIntensity_avr_cal(Index1-1));
        end
    else
        a = (inputGeneratorValue(Index1)-inputGeneratorValue(Index2))/(realIntensity_avr_cal(Index1)-realIntensity_avr_cal(Index2));
        b = inputGeneratorValue(Index2) -(a*realIntensity_avr_cal(Index2));
        if isnan(a)
            a = (inputGeneratorValue(Index1)-inputGeneratorValue(Index2-1))/(realIntensity_avr_cal(Index1)-realIntensity_avr_cal(Index2-1));
            b = inputGeneratorValue(Index2-1) -(a*realIntensity_avr_cal(Index2-1));
        end
    end
    %}
    
    %Zero order interpollation
    inputIntensity = inputGeneratorValue(Index1);
    
    if isnan(inputIntensity)
        inputIntensity = 0;
    elseif inputIntensity > 1
        inputIntensity = 1;
    elseif inputIntensity < 0
        inputIntensity = 0;
    end

    stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], inputIntensity, 0, [1 1 1]);
    inputIntensity = 0;



function ShowStimuli(stimImg, wPtr)
    %Presents image (stimImg) to the second screen)
    stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
    Screen('DrawTexture', wPtr, stimTextureId);
    Screen('Flip', wPtr);
    
    function wPtr = DrawInstructionsPage(testMode)

	% Open window with a fixation point
	screenColor = [128 128 128];
	screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
	screenFreqTolerance = 5;
	screenNumber = max(Screen('Screens')); %0: single display setting
	screenNumber = 2;
	screenSize = zeros(1,2);
	[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
	if testMode
		PsychDebugWindowConfiguration(0, 0.25)
% 	        wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	else
		clear Screen;
		Screen('Preference', 'ConserveVRAM', 4096);
		Screen('Preference', 'SkipSyncTests', 1 );
		wPtr = Screen('OpenWindow',screenNumber,screenColor);
	end

	% Get timing information from screen
	screenRefreshRate = Screen('GetFlipInterval', wPtr);
	disp(['Screen rate: ' num2str(screenRefreshRate)]);

	ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

	Screen('FillRect', wPtr, screenColor);

	% Check the monitor's configuration
	if ~testMode
		if (ScreenFreqDiscrepancy < screenFreqTolerance)
			disp('Refresh rate is the desired one');
		else
			disp(['Refresh frequency should be ' screenFreq]);
			error('Screen was not propperly configured');
		end
	end

	% Set font style in window
	tfont='Arial';
	tsize=15;
	tstyle=1; %1=normal 2=bold 3=italic 4=underline
	vSpacing = 3;
	Screen('TextFont', wPtr, tfont);
	Screen('TextSize', wPtr, tsize);
	Screen('TextStyle', wPtr, tstyle);
	
    % Get coordinates in the window
    windowSize = zeros(1,2);
    [windowSize(1), windowSize(2)] = Screen('WindowSize', wPtr);
    windowCenter = zeros(1,2);
    windowCenter(1) = floor(windowSize(1)/2);
    windowCenter(2) = floor(windowSize(2)/2);

    screenNumber = Screen('WindowScreenNumber', wPtr);

    % Define black and white
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
	
	strInstructions = 'En este experimento se le presentar� un est�mulo visual y debe indicar si le causa molestia o no. En caso que le cause molestia, debe pulsar la tecla que se indica como "Molesto", en caso contrario, debe pulsar la tecla "NO Molesto". Debe indicar su respuesta lo m�s r�pido posible cuando aparezca la cruz en el centro de la pantalla (indicar� que el est�mulo ha terminado de ser presentado). La respuesta de la derecha corresponde a la tecla "F" y la de la izquierda a la tecla "A".';
	strInstructions2 = 'Pulse la barra espaciadora para continuar';
	
	DrawFormattedText(wPtr,strInstructions,'center',windowSize(2)*0.25,black, 80, 0, 0, vSpacing);
	DrawFormattedText(wPtr,strInstructions2,'center',windowSize(2)*0.66,black, 80, 0, 0, vSpacing);

    % Flip to the screen
    Screen('Flip', wPtr);




%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server')
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 9999;
    fopen(tcpObj); %Wait until the client connects


function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    

    function [X_1nm, Y_1nm, X_5nm, Y_5nm] = C7000_GetSpectrum(tcpObj)
    %Ask the C-7000 to send the spectrum of the last measure made
    %This functuion does not take any new measures of the light, just sends
    %more data to Matlab
    fwrite(tcpObj, int16(-2), 'int16');
    
    Y_1nm = fread(tcpObj, 400, 'single');
    Y_5nm = fread(tcpObj, 80, 'single');
    
    Y_1nm = transpose(Y_1nm);
    Y_5nm = transpose(Y_5nm);
    X_1nm = [380:1:779];
    X_5nm = [380:5:775];

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program

        %END HERE
    