X_1nm = [];
Y_1nm = [];
X_5nm = [];
Y_5nm = [];
StimuliColor_RGB = [1 1 0];
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);

%Call the C7000 controller (TCP client)
!..\C7000_Controller\netcoreapp3.1\C7000_Test &
%Start the TCP server
tcpObj = tcp_Server_Start(); %Wait here until the client connects


%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

wPtr = Screen('OpenWindow', screenNumber, StimuliColor_RGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, StimuliColor_RGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);



%Present new image
stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], 1, 0, StimuliColor_RGB); %Create the visual stimuli
stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
Screen('DrawTexture', wPtr, stimTextureId);
Screen('Flip', wPtr);

%Perform 1 measuresand returun the usefull values
[average_lx, max_lx, min_lx] = C7000_Read(1, tcpObj);
%Get the spectrum of the last measure
[X_1nm, Y_1nm, X_5nm, Y_5nm] = C7000_GetSpectrum(tcpObj);


%Program clossing
sca;
C7000_End_program(tcpObj); %Return the C-7000 to its manual control
fclose(tcpObj); %Close the server

Y_1nm = Y_1nm/max(Y_1nm); %"normalize" the vector
plot(X_1nm, Y_1nm, 'Linewidth', 2);
xlim([380 800]);
xlabel('Wavelength [nm]', 'Fontsize', 14);
ylabel('Amplitude [a.u.]', 'Fontsize', 14);
title('Red+Green stimulus spectrogram', 'Fontsize', 18);




%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server') %Receive data from any ip
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 5; %change me to 9999
    tcpObj.InputBufferSize = 99999;
    fopen(tcpObj); %Wait until the client connects
end

function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    
end

function [X_1nm, Y_1nm, X_5nm, Y_5nm] = C7000_GetSpectrum(tcpObj)
    %Ask the C-7000 to send the spectrum of the last measure made
    %This functuion does not take any new measures of the light, just sends
    %more data to Matlab
    fwrite(tcpObj, int16(-2), 'int16');
    
    Y_1nm = fread(tcpObj, 400, 'single');
    Y_1nm = transpose(Y_1nm);
    Y_5nm = fread(tcpObj, 80, 'single');
    Y_5nm = transpose(Y_5nm);
    
    X_1nm = [380:1:779];
    X_5nm = [380:5:775];
end

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program
end
        %END HERE