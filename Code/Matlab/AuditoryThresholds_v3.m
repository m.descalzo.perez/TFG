function [threshold, history, responsesStaircase, reactionTimes] = AuditoryThresholds_v3(participantId, volumes, frequencies, durations, interStimuliInterval, repetitions, intraStimuliInterval, responseDuration, testMode)
% Calculate discomfort thresholds from auditory stimuli. A pure tone of a
% specified frequency, duration and number of repetitions would be
% presented and the participant would response whether it causes dicomfort
% or not.
% The threshold will be measured on the variable with a length greater than
% 1.
	if nargin < 8
		volumes = [40:5:140]; % in dBA
		frequencies = 1000; % in Hz
		durations = 1; % in seconds
		interStimuliInterval = 3; % in seconds
		repetitions = 1;
		intraStimuliInterval = 1; % in seconds
		responseDuration = 0.8; % in seconds
	end

	if exist('testMode','var') == 0
		testMode = 1;
		simulateResponses = 1;
	else
		simulateResponses = 0;
	end
	simulateResponses = 0;
	
	% if nargin < 1
	% 	error('Participant Id not specified');
	% end
	
	% Parameters to set before experiment
	viewingDistanceCm = 70; % in cm
	screenSizeCm = [50 30]; % width and height in cm
	
	fixationSizeDeg = 0.5; % in visual degrees
	buttonsSeparationDeg = 1; % in degrees
	
	fixationColor_sRGB = [1 1 1];
	responseColor_sRGB = [0 0 0];
	backgroundColor_sRGB = [0.5 0.5 0.5];
	
	enableCorrections = 1;

	% Check variable to measure the threshold
	bVolumeThreshold = 0;
	bFreqThreshold = 0;
	bDurationsThreshold = 0;
	bRepetitionsThreshold = 0;
	if length(volumes) > 1
		disp('Threshold measured on volume');
		bVolumeThreshold = 1;
		variable4Threshold = volumes;
	end
	if length(frequencies) > 1
		disp('Threshold measured on frequencies');
		bFreqThreshold = 1;
		variable4Threshold = frequencies;
	end
	if length(durations) > 1
		disp('Threshold measured on durations');
		bDurationsThreshold = 1;
		variable4Threshold = durations;
	end
	if length(repetitions) > 1
		disp('Threshold measured on repetitions');
		bRepetitionsThreshold = 1;
		variable4Threshold = repetitions;
	end

	if (bVolumeThreshold+bFreqThreshold+bDurationsThreshold+bRepetitionsThreshold) ~= 1
		error('Variable to measure the threshold is not defined properly');
	end

	% Establish key mapping: ESCape aborts, Space toggles between auto-
	% movement of sound source or user mouse controlled movement:
	KbName('UnifyKeynames');
	SPACE = KbName('space');
	ESC = KbName('ESCAPE');
	AKEY = KbName('A');
	FKEY = KbName('F');
	keyList = zeros(1,256);
	keyList(SPACE) = 1;
	keyList(ESC) = 1;
	keyList(AKEY) = 1;
	keyList(FKEY) = 1;
	
	fixationColor_sRGB = uint8(fixationColor_sRGB.*255);
	responseColor_sRGB = uint8(responseColor_sRGB.*255);
	backgroundColor_sRGB = uint8(backgroundColor_sRGB.*255);

	% Perform basic initialization of the sound driver:
	InitializePsychSound(1);

	% Get audio devices

	% 1=Windows/DirectSound, 2=Windows/MME, 11=Windows/WDMKS, 13=Windows/WASAPI, 8=Linux/ALSA, 7=Linux/OSS, 12=Linux/JACK, 5=MacOSX/CoreAudio.
	deviceType = 13;
	devices = PsychPortAudio('GetDevices', deviceType);
	deviceId = devices.DeviceIndex;
	% deviceFreq: Requested playback/capture rate in samples per second (Hz).
	deviceSamplingRate = devices.DefaultSampleRate;
	% deviceChannels: Number of audio channels to use, defaults to 2 for stereo.
	deviceChannels = devices.NrOutputChannels;


	% Mode: Defaults to 1 == sound playback only. Can be set to 2 == audio
	% capture, or 3 for simultaneous capture and playback of sound. Note however that
	% mode 3 (full duplex) does not work reliably on all sound hardware.
	soundMode = 1;
	% ReqLatency: Level 0 means: Don�t care about latency or timing
	% precision. This mode works always and with all settings, plays nicely with other
	% sound applications. Level 1 (the default) means: Try to get the lowest latency
	% that is possible under the constraint of reliable playback, freedom of choice
	% for all parameters and interoperability with other applications. Level 2 means:
	% Take full control over the audio device, even if this causes other sound
	% applications to fail or shutdown. Level 3 means: As level 2, but request the
	% most aggressive settings for the given device. Level 4: Same as 3, but fail if
	% device can�t meet the strictest requirements.
	reqLatency = 2; % should be 2

	try
		pahandle = PsychPortAudio('Open', deviceId, soundMode, reqLatency, deviceSamplingRate, deviceChannels);
	catch
		% Close Audio Port
		try
			PsychPortAudio('Close', pahandle);
		catch
			PsychPortAudio('Close');
		end
		pahandle = PsychPortAudio('Open', deviceId, soundMode, reqLatency, deviceSamplingRate, deviceChannels);
	end
	
	% Open window and transform units from degrees to pixels
	[wPtr, tFlip] = InitWindow(backgroundColor_sRGB, testMode);
	
	screenSizePix = zeros(1,2);
	[screenSizePix(1) screenSizePix(2)] = Screen('WindowSize', wPtr);
	pixelsPerDegree = (screenSizePix./2) ./ (atan((screenSizeCm./2)./viewingDistanceCm).*360/(2*pi));
	
	if ~ismembertol(pixelsPerDegree(1),pixelsPerDegree(2))
		warning('Monitor size is not coherent');
	end
	
	fixationSizePix = round(fixationSizeDeg*pixelsPerDegree(1)); % in pixels
	buttonsSeparationPix = round(buttonsSeparationDeg*pixelsPerDegree(1)); % in pixels
	
% 	wPtr = DrawFixationPoint([], testMode);
	[wPtr, tFixation] = DrawFixationPoint(wPtr, fixationSizePix, fixationColor_sRGB, 0, [], buttonsSeparationPix, backgroundColor_sRGB, 0, tFlip, testMode);
	
	status = PsychPortAudio('GetStatus', pahandle);

	oldRunMode = PsychPortAudio('RunMode', pahandle, 1);

	% A-weights
	% Weights extracted from Harrison, M. (2004). The measurement and behaviour of sound. Vehicle Refinement, 17�73
	aWeights =	[-63.4	-56.7	-50.5	-44.7	-39.4	-34.6	-30.2	-26.2	-22.5	-19.1	-16.1	-13.4	-10.9	-8.6	-6.6	-4.8	-3.2	-1.9	-0.8	0		0.6		1		1.2		1.3		1.2		1		0.5		-0.1	-1.1	-2.5	-4.3	-6.6	-9.3];
	aFreqs =	[12.5	16		20		25		31.5	40		50		63		80		100		125		160		200		250		315		400		500		630		800		1000	1250	1600	2000	2500	3150	4000	5000	6300	8000	10000	12500	16000	20000];

	aXs = linspace(min(aFreqs(:)), max(aFreqs(:)), 10000);
	aYs = interp1(aFreqs, aWeights, aXs);

	% figure,
	% semilogx(aXs, aYs);
	% title('A-Weighting');

	% Set general parameters of SIAM method
	possibleValues = variable4Threshold;
	initValue = variable4Threshold(round(length(variable4Threshold)/4));
	stepSize = [16 8 4 2 1];
	trialStepSize = [0:1:size(stepSize,2)-1];
	nReversals = 20;
	nReversalTrials = 4;
	nMaxTrials = 300;
	[nextValue, isFinished, finalValue, methodState] = psychMethodSIAM(possibleValues,initValue,stepSize,trialStepSize,nReversals,nReversalTrials,nMaxTrials);

	% Set frequency of the stimulus
	if ~bFreqThreshold
		freq = frequencies;
	else
		freq = nextValue;
	end

	% Set volume of the stimulus
	if ~bVolumeThreshold
		vol = volumes;
	else
		vol = nextValue;
	end

	% Set number of repetitions of the stimulus
	if ~bRepetitionsThreshold
		rep = repetitions;
	else
		rep = nextValue;
	end

	% Set duration of the stimulus
	if ~bDurationsThreshold
		dur = durations;
	else
		dur = nextValue;
	end

	% To test the script
	if testMode
		warning('Test mode is activated');
		simulatedResponses = zeros(1,nMaxTrials);
		% responses(1:2:end) = 1;
		simulatedResponses((randi(2,1,nMaxTrials)-1)==1) = 1;
    end

    responses = ones(2, nMaxTrials).*(-1);
	reactionTimes = ones(2, nMaxTrials).*(-1);
	trial = 1;
	while ~isFinished
		% Initialize sound variable
		stimulusSampling = round(dur*deviceSamplingRate);
	% 	intraStimuliIntervalSampling = round(intraStimuliInterval*deviceSamplingRate);
	% 	auditoryStimulusLength = stimulusSampling+intraStimuliIntervalSampling;
		auditoryStimulusLength = stimulusSampling;

		% From volume (dBA) to amplitude
		weight = interp1(aFreqs, aWeights, freq);
		amp = 20e-6*10^((vol-weight)/20);

		% Create pure tone
		auditoryStimulus = zeros(1,auditoryStimulusLength);
		auditoryStimulus(1,1:stimulusSampling) = MakeBeep_v2(freq, dur, deviceSamplingRate, 1);
		auditoryStimulus = repmat(auditoryStimulus, [deviceChannels 1]); % repeat it to do it stereo

		% From amplitude to volume (dBA)
		% Set volume of the stimulus
		PsychPortAudio('Volume', pahandle, amp);

	% 	Volume in dB, considering the air reference (20 microPascals).
	% 	Amplitude ratio to dB conversion: GdB = 20 log10(A2 / A1), where
	% 	A2 is the amplitude level, A1 is the referenced amplitude level and
	% 	GdB is the amplitude ratio or gain in dB. Extracted from https://www.rapidtables.com/electric/decibel.html
		volumeDB = 20*log10(amp/(20e-6));
		volumeDBA = volumeDB + interp1(aFreqs, aWeights, freq);

		disp(['Stimulus'' ', 'volume values: ', num2str(volumeDB), ' dB (', num2str(freq), ' Hz), i.e. ', num2str(volumeDBA), ' dBA'])

		% Fill the audio playback buffer with the audio data:
		PsychPortAudio('FillBuffer', pahandle, auditoryStimulus);
		
		% Set randmonly the key that corresponds to discomfort and
		% non-discomfort, respective
		auxRand = rand;
		if auxRand < 0.5
			aKey = 0; % 'A' key non-discomfort
			fKey = 1; % 'F' key discomfort
			buttonsOrder = [0, 1];
		else
			aKey = 1; % 'A' key discomfort
			fKey = 0; % 'F' key non-discomfort
			buttonsOrder = [1, 0];
		end

		% Start playback for these sources:
		for r = 1:rep
			startTime = PsychPortAudio('Start', pahandle);

			% Stop playback of all sources:
			[startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1); % wait until the stimulus is finished
			if r~=rep
				WaitSecs(intraStimuliInterval);
			end
		end
		
		% Draw response page
		[wPtr, tStartResponse] = DrawFixationPoint(wPtr, fixationSizePix, responseColor_sRGB, 1, buttonsOrder, buttonsSeparationPix, backgroundColor_sRGB, 0, tFlip, testMode);
		
		% Participant's response
		KbQueueCreate([],keyList);
		KbQueueStart;
		KbQueueFlush;
		timeOut = 0;
		isDown = 0;

		while ((~isDown) && (~timeOut))
			% Check keyboard:
			[isDown, keyCode]=KbQueueCheck;

			% For test purposes
			if testMode
				if simulateResponses
					isDown = 1;
					if simulatedResponses(trial)
						keyCode(FKEY) = 1;
					else
						keyCode(AKEY) = 1;
					end
				end
			end

			% Get participant's response
			if isDown
				tReaction = GetSecs;
				KbQueueFlush;
				
				% Check correction
				if enableCorrections
					isDownCorrection = 0;
					while ((~isDownCorrection) && (~timeOut))
						% Check keyboard:
						[isDownCorrection, keyCodeCorrection]=KbQueueCheck;

						if isDownCorrection
							tReactionCorrection = GetSecs;

							% Save reaction time of the correction
							reactionTimes(2,trial) = tReactionCorrection-tStartResponse;

							if keyCodeCorrection(AKEY) > 0
								if aKey % A key means discomfort
									responses(2,trial) = 1;
								else
									responses(2,trial) = 0;
								end
							end

							if keyCodeCorrection(FKEY) > 0
								if fKey % F key means discomfort
									responses(2,trial) = 1;
								else
									responses(2,trial) = 0;
								end
							end
						end

						if ((~isDownCorrection) && ((GetSecs-tStartResponse) > responseDuration))
							responses(2,trial) = 2;
							reactionTimes(2,trial) = -1;
							timeOut = 1;
						end
					end					
				end % end enableCorrections
				
				if keyCode(ESC) > 0
					warning('Operation terminated by user');
					break;
				end

				if keyCode(AKEY) > 0
					reactionTimes(1,trial) = tReaction-tStartResponse;
					if aKey % A key means discomfort
						responses(1,trial) = 1;
					else
						responses(1,trial) = 0;
					end
				end

				if keyCode(FKEY) > 0
					reactionTimes(1,trial) = tReaction-tStartResponse;
					if fKey % F key means discomfort
						responses(1,trial) = 1;
					else
						responses(1,trial) = 0;
					end
				end
				
				if responses(2,trial)==2 % There is no correction done
					[nextValue, isFinished, finalValue, methodState] = psychMethodSIAM(responses(1,trial), methodState);
					
					if responses(1,trial) == 1
						disp(['Stimulus is discomfort: Next value is ', num2str(nextValue)]);
					else
						disp(['Stimulus is NOT discomfort: Next value is ', num2str(nextValue)]);
					end
				else
					disp(['Correction done: Next value is ', num2str(nextValue)]);
				end
				
				KbQueueFlush;
				KbQueueStop;
				KbQueueRelease;
				KbReleaseWait;
			end

			% If the participant has not responded during the response window,
			% it is considered as a missed response and the stimulus is
			% presented again
			if ((~isDown) && ((GetSecs-tStartResponse) > responseDuration))
				responses(1,trial) = 2;
				reactionTimes(1,trial) = -1;
				disp(['Missed response: Next value is ', num2str(nextValue)]);
				timeOut = 1;
				KbQueueFlush;
				KbQueueStop;
				KbQueueRelease;
				KbReleaseWait;
			end
		end
		
		[wPtr, tStartResponse] = DrawFixationPoint(wPtr, fixationSizePix, fixationColor_sRGB, 0, [], buttonsSeparationPix, backgroundColor_sRGB, 0, tFlip, testMode);

		if keyCode(ESC)
			break;
		end

		% Update the value according to the response
		if bFreqThreshold
			freq = nextValue;
		else
			if bVolumeThreshold
				vol = nextValue;
			else
				if bRepetitionsThreshold
					rep = nextValue;
				else
					if bDurationsThreshold
						dur = nextValue;
					else
						error('Variable to measure the threshold is undefined');
					end
				end
			end
		end

		% Delete buffer:
		result = PsychPortAudio('DeleteBuffer');

		trial = trial + 1;
		WaitSecs(interStimuliInterval);
	end
	
	responses(:,trial:end) = [];
	reactionTimes(:,trial:end) = [];
	
	Screen('Close',wPtr);

	fig = figure;
	stairs([1:size(methodState.history,2)], methodState.history, 'LineWidth', 2);
	title('Auditory Threshold');
	xlabel('# Trial');
	if bFreqThreshold
		ylabel('Frequency (Hz)');
		strDisp = ' Hz';
		fileName = [participantId,'_FrequencyThreshold_',num2str(volumes),'_dBA_',num2str(durations),'s',num2str(rep),'Repetitions'];
	else
		if bVolumeThreshold
			ylabel('Volume (dBA)');
			strDisp = ' amplitude';
			fileName = [participantId,'_VolumeThreshold_',num2str(freq),'Hz_',num2str(durations),'s_',num2str(rep),'Repetitions'];
		else
			if bRepetitionsThreshold
				ylabel('# Repetitions');
				strDisp = ' repetitions';
				fileName = [participantId,'_RepetitionsThreshold_',num2str(volumes),'_dBA_',num2str(freq),'Hz_',num2str(durations),'s'];
			else
				if bDurationsThreshold
					ylabel('Duration (s)');
					strDisp = ' s';
					fileName = [participantId,'_DurationThreshold_',num2str(volumes),'_dBA_',num2str(freq),'Hz_',num2str(rep),'Repetitions'];
				else
					error('Variable to measure the threshold is not defined properly');
				end
			end
		end
	end

	% Output variables
	threshold = finalValue;
	history = methodState.history;
	responsesStaircase = methodState.responses;
	disp(['The threshold is defined at ', num2str(threshold), strDisp]);
	
	% Save results
	pathName = ['Results/AuditoryThreshold/',participantId,'/'];
	saveas(fig,[pathName,fileName,'.png']);
	save([pathName,fileName,'.mat'],'participantId','volumes','frequencies','durations','interStimuliInterval','repetitions','intraStimuliInterval','responseDuration','deviceType','deviceId','deviceSamplingRate','deviceChannels','methodState','threshold','history','responsesStaircase','responses','reactionTimes');	

	% Wait a bit:
	WaitSecs(0.1);

	% Close Audio Port
	PsychPortAudio('Close', pahandle);
	
	sca;

	% Done
	return;
	
function [wPtr, tFlip] = InitWindow(screenColor_RGB, testMode)

	% Open window
	screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
	screenFreqTolerance = 5;
	screenNumber = max(Screen('Screens')); %0: single display setting
	screenSize = zeros(1,2);
	[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
	if testMode
		PsychDebugWindowConfiguration(0, 0.25)
%         wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		wPtr = Screen('OpenWindow',screenNumber,screenColor_RGB);
	else
		Screen('Preference', 'ConserveVRAM', 4096);
% 			Screen('Preference', 'SkipSyncTests', 0);
		Screen('Preference', 'SkipSyncTests', 1);
		wPtr = Screen('OpenWindow',screenNumber,screenColor_RGB);
	end

	% Get timing information from screen
	screenRefreshRate = Screen('GetFlipInterval', wPtr);
	disp(['Screen rate: ' num2str(screenRefreshRate)]);

	ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

	Screen('FillRect', wPtr, screenColor_RGB);

	% Check the monitor's configuration
	if ~testMode
		if (ScreenFreqDiscrepancy < screenFreqTolerance)
			disp('Refresh rate is the desired one');
		else
			disp(['Refresh frequency should be ' screenFreq]);
% 				error('Screen was not propperly configured');
		end
	end

	% Set font style in window
	tfont='Arial';
	tsize=14;
	tstyle=1; %1=normal 2=bold 3=italic 4=underline
	Screen('TextFont', wPtr, tfont);
	Screen('TextSize', wPtr, tsize);
	Screen('TextStyle', wPtr, tstyle);
	
    % Flip to the screen
    [tFlipStart,~,tFlipEnd] = Screen('Flip', wPtr);
	tFlip = tFlipEnd-tFlipStart;
	
function [wPtr, tFixation] = DrawFixationPoint(wPtr, fixationSizePix, fixation_sRGB, showButtons, buttonsOrder, buttonsSeparationPix, background_sRGB, whenFixation, tFlip, testMode)
	
	if exist('wPtr','var')==0 || isempty(wPtr)
		% Open window with a fixation point
		screenColor = [128 128 128];
		screenFreq = 60; %120;%120;%In Hz (desired freq) %60 for testing purposes
		screenFreqTolerance = 5;
		screenNumber = max(Screen('Screens')); %0: single display setting
		screenSize = zeros(1,2);
		[screenSize(1), screenSize(2)] = Screen('WindowSize', screenNumber);
		if testMode
			PsychDebugWindowConfiguration(0, 0.25)
			wPtr = Screen('OpenWindow', screenNumber, screenColor, [1 1 screenSize(1) screenSize(2)*0.25]);
		else
			wPtr = Screen('OpenWindow',screenNumber,screenColor);
		end

		% Get timing information from screen
		screenRefreshRate = Screen('GetFlipInterval', wPtr);
		disp(['Screen rate: ' num2str(screenRefreshRate)]);

		ScreenFreqDiscrepancy = screenFreq - 1/screenRefreshRate;

		Screen('FillRect', wPtr, screenColor);

		% Check the monitor's configuration
		if ~testMode
			assert(isequal(screenSize, windowSize));
			if (ScreenFreqDiscrepancy < screenFreqTolerance)
				disp('Refresh rate is the desired one');
			else
				disp(['Refresh frequency should be ' screenFreq]);
				error('Screen was not propperly configured');
			end
		end

		% Set font style in window
		tfont='Arial';
		tsize=14;
		tstyle=1; %1=normal 2=bold 3=italic 4=underline
		Screen('TextFont', wPtr, tfont);
		Screen('TextSize', wPtr, tsize);
		Screen('TextStyle', wPtr, tstyle);
	else
		screenNumber = Screen('WindowScreenNumber',wPtr);
	end
	
	if tFlip > whenFixation
		tFlip = 0;
	end
	
	Screen('FillRect', wPtr, background_sRGB);
	
	% Get coordinates in the window
	windowSize = zeros(1,2);
	[windowSize(1), windowSize(2)] = Screen('WindowSize', wPtr);
	windowCenter = zeros(1,2);
	windowCenter(1) = floor(windowSize(1)/2);
	windowCenter(2) = floor(windowSize(2)/2);

	% Here we set the size of the arms of our fixation cross
	fixCrossDimPix = round(fixationSizePix/2);

	% Now we set the coordinates (these are all relative to zero we will let
	% the drawing routine center the cross in the center of our monitor for us)
	xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
	yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
	allCoords = [xCoords; yCoords];

	% Set the line width for our fixation cross
	lineWidthPix = 4;

	% Draw the fixation cross in white, set it to the center of our screen and
	% set good quality antialiasing
	Screen('DrawLines', wPtr, allCoords, lineWidthPix, fixation_sRGB, windowCenter);
	
	% Draw the buttons to guide the participant
	if showButtons		
		% Set font style in window
		tsize=20;
		Screen('TextSize', wPtr, tsize);
		
		if buttonsOrder(1)
			strLeft = 'Molesto';
			strRight = 'NO Molesto';
		else
			strLeft = 'NO Molesto';
			strRight = 'Molesto';
		end
		
		xTextSizePix = tsize*max(strlength(strLeft),strlength(strRight));
		yTextSizePix = round(tsize/2);
		
		DrawFormattedText(wPtr,strLeft,'right',windowCenter(2)+fixCrossDimPix,fixation_sRGB,[],[],[],[],0,[windowCenter(1)-xTextSizePix-buttonsSeparationPix, windowCenter(2)-yTextSizePix, windowCenter(1)-buttonsSeparationPix, windowCenter(2)+yTextSizePix]);
		DrawFormattedText(wPtr,strRight,windowCenter(1)+buttonsSeparationPix,windowCenter(2)+fixCrossDimPix,fixation_sRGB,[],[],[],[],0,[windowCenter(1)+buttonsSeparationPix, windowCenter(2)-yTextSizePix, windowCenter(1)+xTextSizePix+buttonsSeparationPix, windowCenter(2)+yTextSizePix]);
	end

	% Flip to the screen
   	tFixation = Screen('Flip', wPtr, whenFixation-tFlip);