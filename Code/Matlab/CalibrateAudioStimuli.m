function CalibrateAudioStimuli()
% Script to measure the difference (in terms of intensity) between the
% sent and the presented stimuli using the sonometer.

	% Stimuli parameters
	volumes = [30:2:60]; % in dBA
	frequencies = 1000; % in Hz
	durations = 3; % in seconds
	interStimuliInterval = 3; % in seconds
	stimulusRepetitions = 5;
	intraStimuliInterval = 6; % in seconds
	
	calibrationRepetitions = 3;

	% Perform basic initialization of the sound driver:
	InitializePsychSound(1);

	% Get audio devices
	% 1=Windows/DirectSound, 2=Windows/MME, 11=Windows/WDMKS, 13=Windows/WASAPI, 8=Linux/ALSA, 7=Linux/OSS, 12=Linux/JACK, 5=MacOSX/CoreAudio.
	deviceType = 3;
	devices = PsychPortAudio('GetDevices', deviceType);
	deviceId = devices.DeviceIndex;
	% deviceFreq: Requested playback/capture rate in samples per second (Hz).
	deviceSamplingRate = devices.DefaultSampleRate;
	% deviceChannels: Number of audio channels to use, defaults to 2 for stereo.
	deviceChannels = devices.NrOutputChannels;

	% Mode: Defaults to 1 == sound playback only. Can be set to 2 == audio
	% capture, or 3 for simultaneous capture and playback of sound. Note however that
	% mode 3 (full duplex) does not work reliably on all sound hardware.
	soundMode = 1;
	% ReqLatency: Level 0 means: Don�t care about latency or timing
	% precision. This mode works always and with all settings, plays nicely with other
	% sound applications. Level 1 (the default) means: Try to get the lowest latency
	% that is possible under the constraint of reliable playback, freedom of choice
	% for all parameters and interoperability with other applications. Level 2 means:
	% Take full control over the audio device, even if this causes other sound
	% applications to fail or shutdown. Level 3 means: As level 2, but request the
	% most aggressive settings for the given device. Level 4: Same as 3, but fail if
	% device can�t meet the strictest requirements.
	reqLatency = 2; % should be 2

	try
		pahandle = PsychPortAudio('Open', deviceId, soundMode, reqLatency, deviceSamplingRate, deviceChannels);
	catch
		% Close Audio Port
		try
			PsychPortAudio('Close', pahandle);
		catch
			PsychPortAudio('Close');
		end
		pahandle = PsychPortAudio('Open', deviceId, soundMode, reqLatency, deviceSamplingRate, deviceChannels);
	end

	status = PsychPortAudio('GetStatus', pahandle);

	oldRunMode = PsychPortAudio('RunMode', pahandle, 1);

	% A-weights
	% Weights extracted from Harrison, M. (2004). The measurement and behaviour of sound. Vehicle Refinement, 17�73
	aWeights =	[-63.4	-56.7	-50.5	-44.7	-39.4	-34.6	-30.2	-26.2	-22.5	-19.1	-16.1	-13.4	-10.9	-8.6	-6.6	-4.8	-3.2	-1.9	-0.8	0		0.6		1		1.2		1.3		1.2		1		0.5		-0.1	-1.1	-2.5	-4.3	-6.6	-9.3];
	aFreqs =	[12.5	16		20		25		31.5	40		50		63		80		100		125		160		200		250		315		400		500		630		800		1000	1250	1600	2000	2500	3150	4000	5000	6300	8000	10000	12500	16000	20000];

	aXs = linspace(min(aFreqs(:)), max(aFreqs(:)), 10000);
	aYs = interp1(aFreqs, aWeights, aXs);

	% figure,
	% semilogx(aXs, aYs);
	% title('A-Weighting');

	calibrationCurve = zeros(calibrationRepetitions,size(volumes,2));
	
	for c = 1:calibrationRepetitions
		for trial = 1:size(volumes,2)

			% Initialize sound variable
			stimulusSampling = round(durations*deviceSamplingRate);
		% 	intraStimuliIntervalSampling = round(intraStimuliInterval*deviceSamplingRate);
		% 	auditoryStimulusLength = stimulusSampling+intraStimuliIntervalSampling;
			auditoryStimulusLength = stimulusSampling;

			% From volume (dBA) to amplitude
			weight = interp1(aFreqs, aWeights, frequencies);
			vol = volumes(trial);
			amp = 20e-6*10^((vol-weight)/20);

			% Create pure tone
			auditoryStimulus = zeros(1,auditoryStimulusLength);
			auditoryStimulus(1,1:stimulusSampling) = MakeBeep_v2(frequencies, durations, deviceSamplingRate, 1);
			auditoryStimulus = repmat(auditoryStimulus, [deviceChannels 1]); % repeat it ti di it stereo

			% From amplitude to volume (dBA)
			% Set volume of the stimulus
			PsychPortAudio('Volume', pahandle, amp);

		% 	Volume in dB, considering the air reference (20 microPascals).
		% 	Amplitude ratio to dB conversion: GdB = 20 log10(A2 / A1), where
		% 	A2 is the amplitude level, A1 is the referenced amplitude level and
		% 	GdB is the amplitude ratio or gain in dB. Extracted from https://www.rapidtables.com/electric/decibel.html
			volumeDB = 20*log10(amp/(20e-6));
			volumeDBA = volumeDB + interp1(aFreqs, aWeights, frequencies);

			disp(['Stimulus'' ', 'volume values: ', num2str(volumeDB), ' dB (', num2str(frequencies), ' Hz), i.e. ', num2str(volumeDBA), ' dBA'])

			% Fill the audio playback buffer with the audio data:
			PsychPortAudio('FillBuffer', pahandle, auditoryStimulus);

			% Start playback for these sources:
			for r = 1:stimulusRepetitions
				startTime = PsychPortAudio('Start', pahandle);

				% Stop playback of all sources:
				[startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1); % wait until the stimulus is finished
				if r~=stimulusRepetitions
					WaitSecs(intraStimuliInterval);
				end
			end

			% Collect measured intensity (in dBA)
			titleBar = 'Calibraci�n de los est�mulos';
			userPrompt = {'Intensidad observada (en dBA)'};
			caUserInput = inputdlg(userPrompt, titleBar, 1);
			if isempty(caUserInput),return,end % Bail out if they clicked Cancel.
			calibrationCurve(c,trial) = str2double(caUserInput{1});

			% Delete buffer:
			result = PsychPortAudio('DeleteBuffer');

			WaitSecs(interStimuliInterval);
		end
	end
	
	% Save results
	currDate = datestr(now, 'yyyy-mm-dd');
	save(['Results/AudioCalibration/',currDate,'_',num2str(frequencies),'Hz.mat'],'volumes','calibrationCurve','frequencies','durations');

	figure,
	hold on;
	errorbar(volumes, mean(calibrationCurve,1), std(calibrationCurve,0,1), 'Color', [0.25 0.25 0.25], 'LineWidth', 2);
% 	plot(volumes, mean(calibrationCurve,1), 'Color', [0.25 0.25 0.25], 'LineWidth', 2);
	scatter(volumes, mean(calibrationCurve,1), 40, [0 0 0]);
	hold off;
	xlim([min(min(volumes(:)),min(calibrationCurve(:))) max(max(volumes(:)),max(calibrationCurve(:)))]);
	ylim([min(min(volumes(:)),min(calibrationCurve(:))) max(max(volumes(:)),max(calibrationCurve(:)))]);
	xlabel('Sent intensity (in dBA)');
	ylabel('Measured intensity (in dBA)');
	title('Intensity calibration of audio stimuli');
	
	disp('Calibration done');
	
	% Wait a bit:
	WaitSecs(0.1);

	% Close Audio Port
	PsychPortAudio('Close', pahandle);

	% Done
	return;