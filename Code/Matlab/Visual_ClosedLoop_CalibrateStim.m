num_measures = 10; %Number of measures will be made in each cycle of the calibration
error_max = 1; %max error (in %) permited during calibration

iterations = 0;
interStimuliColor_sRGB = [0 0 0];
screenSizePix2 = zeros(1,2);
screenSizePix1 = zeros(1,2);

%Call the C7000 controller (TCP client)
!..\C7000_Controller\netcoreapp3.1\C7000_Test &
%Start the TCP server
tcpObj = tcp_Server_Start(); %Wait here until the client connects


%Screen('Preference', 'ScreenToHead', 0, 1, 1);
Screen('Preference', 'SkipSyncTests', 1); 

screenNumber = max(Screen('Screens')); %0: single display setting
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', screenNumber);
[screenSizePix1(1) screenSizePix1(2)] = Screen('WindowSize', 1); %Principal monitor size

wPtr = Screen('OpenWindow', screenNumber, interStimuliColor_sRGB, [screenSizePix1(1) 1 screenSizePix1(1)+screenSizePix2(1) screenSizePix2(2)]);
Screen('FillRect', wPtr, interStimuliColor_sRGB);
[screenSizePix2(1) screenSizePix2(2)] = Screen('WindowSize', wPtr);

%        >>>>>Start of the calibration<<<<
%Present the first image
stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], 0.3, 0);
stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
Screen('DrawTexture', wPtr, stimTextureId);
Screen('Flip', wPtr);

[average1, ~, ~] = C7000_Read(num_measures, tcpObj); %Perform n measures and returun the usefull values
Screen('Close', stimTextureId);

%Present the second image
stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], 0.5, 0);
stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
Screen('DrawTexture', wPtr, stimTextureId);
Screen('Flip', wPtr);

[average2, ~, ~] = C7000_Read(num_measures, tcpObj); %Perform n measures and returun the usefull values
Screen('Close', stimTextureId);

%Lineal law: y = ax + b
a = (0.5-0.3)/(average2-average1);
b = (0.3)-(a*average1);


%    >>>>Creation of the calibration curve<<<<
calIntensity = [0:250:30000]; %Luminances we are aiming
realIntensity_avr = []; %Real luminances we are getting
realIntensity_max = [];
realIntensity_min = [];
useful_intensity = [];

for intensity = calIntensity
    meanIntensity = intensity*a + b;
    
    while iterations < 10
        if isnan(meanIntensity)
            meanIntensity = 0;
        elseif meanIntensity > 1
            meanIntensity = 1;
        elseif meanIntensity < 0
            meanIntensity = 0;
        end
        
        %Present new image
        stimImg = GenerateNoisyImage([screenSizePix2(2) screenSizePix2(1)], meanIntensity, 0);
        stimTextureId = Screen('MakeTexture', wPtr, uint8(stimImg.*255));
        Screen('DrawTexture', wPtr, stimTextureId);
        Screen('Flip', wPtr);

        %Perform n measures and returun the usefull values
        [average_lx, max_lx, min_lx] = C7000_Read(num_measures, tcpObj);
        
        error = (average_lx - intensity)/intensity*100; %Porcentual deviation of the measure from its desired value
        
        if abs(error) < error_max %If error is |low| enough, go to the next point
            break;
        end
        
        if meanIntensity == 1 | meanIntensity == 0  %If be hit the limit already, go to the next point
            break;
        end
            
        meanIntensity = meanIntensity - a*(error/100*intensity) %Image generator correction
        iterations = iterations + 1;
    end

    %Store the useful data for later analisis
    realIntensity_avr = [realIntensity_avr, average_lx];
    realIntensity_max = [realIntensity_max, max_lx-average_lx];
    realIntensity_min = [realIntensity_min, average_lx-min_lx];
    useful_intensity = [useful_intensity, meanIntensity];
    
    iterations = 0;
end

errorbar(calIntensity, realIntensity_avr, realIntensity_min, realIntensity_max, '-o');
hold on
plot(calIntensity, calIntensity);
title('Calibration curve');
legend('Experimental points', 'X=Y curve');
ylabel('Iluminance received [lx]');
xlabel('Iluminance desired [lx]');
hold off


%Program clossing
sca;
C7000_End_program(tcpObj); %Return the C-7000 to its manual control
fclose(tcpObj); %Close the server



%Data save
export_me = [calIntensity; useful_intensity];
csvwrite('Calibration_curve.csv', export_me)












%C-7000 controller Functions 
        %START HERE
function tcpObj = tcp_Server_Start()
    %Opens a TCP/IP server and waits until a client connects to it
    port = 30000;
    tcpObj = tcpip('0.0.0.0', port, 'NetworkRole', 'Server')
    tcpObj.ByteOrder = 'littleEndian';
    tcpObj.Timeout = 9999;
    fopen(tcpObj); %Wait until the client connects
end

function [average_lx, max_lx, min_lx] = C7000_Read(n, tcpObj)
    %Ask the C-7000 to make n measures and returns average, max and min lux
    %of all the measures
    fwrite(tcpObj, int16(n), 'int16');
    
    average_lx = fread(tcpObj, 1, 'double');
    max_lx = fread(tcpObj, 1, 'double');
    min_lx = fread(tcpObj, 1, 'double');    
end

function C7000_End_program(tcpObj)
    fwrite(tcpObj, int16(-1), 'int16'); %Ask the C-7000 to finish the program
end
        %END HERE