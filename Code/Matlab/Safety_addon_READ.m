delete(instrfind);

data = [];
time = [];
s = serial("COM6", 'BaudRate', 115200, 'TimeOut', 5, 'Terminator', 'LF');
fopen(s);

for a = 1:100
    data = [data, str2double(fgets(s))]; %You don't know if the first read vale is a time or a data value
    time = [time, str2double(fgets(s))];       %If you guessed wrong trnaspose the X and the Y axis befor plot
end

fclose(s);
delete(s);
clear s

data = data*5/1023;
time = time - min(time);

plot(time, data, 'Linewidth', 1);
%plot(data, time, 'Linewidth', 1);
ylim([-0.1 5.1]);
xlim([0 63]);
xlabel('Time [ms]', 'Fontsize', 14);
ylabel('Voltage out [V]', 'Fontsize', 14);
title('Light sensor V(out)', 'Fontsize', 18);
