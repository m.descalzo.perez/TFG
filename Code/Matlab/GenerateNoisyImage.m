function rgbImage = GenerateNoisyImage( imSize, meanIntensity, standardDeviation, rgbColor, circularImage )
% This functions generates an image composed by Gaussian noise of
% mean=meanIntensity and size=imSize. Optionally, the user can specify the
% rgbColor (black and white by default) of the noise and whether the image
% has circular shape (rectangular shape by default).
	
	% Check that all parameters are well defined
% 	if nargin < 2
% 		error('Number of arguments is insufficient');
% 	end
	
	if length(imSize) < 2
		error('Image size should be defined by a vector of length equal to two (nrows, ncols)');
	end
	
	if (meanIntensity > 1) || (meanIntensity < 0)
		error('The mean intensity must be between 0 and 1');
	end
	
	if exist('standardDeviation','var')==0 || isempty(standardDeviation)
% 		standardDeviation = min(meanIntensity,1-meanIntensity);
		standardDeviation = 0.1;
	end
	
	if exist('rgbColor','var')==0 || isempty(rgbColor)
		rgbColor = [0 0 0];
	end
	
	if exist('circularImage','var')==0 || isempty(circularImage)
		circularImage = 0;
	end
	
	% Transform RGB color to HSV to be able to just change the intensity
	hsvColor = rgb2hsv(rgbColor);
	
	% Assign the chromatic values of the image
	hsvImage = zeros(imSize(1), imSize(2), 3);
	hsvImage(:,:,1) = hsvColor(1);
	hsvImage(:,:,2) = hsvColor(2);
	
	% To do the circular image
	imageCenter = imSize / 2;
	distanceMatrix = zeros(imSize(1),imSize(2));
	randomVector = zeros(1,imSize(1)*imSize(2));
	threshold = min(imageCenter);
	if circularImage
		for i = 1:imSize(1)
			for j = 1:imSize(2)
				distanceMatrix(i,j) = sqrt((i-imageCenter(1))^2 + (j-imageCenter(2))^2);
			end
		end
		randomVector = zeros(1,length(distanceMatrix(distanceMatrix<threshold)));		
	end
	
	% Randomize the intensity plane of the image
	 randomVector = random('norm',meanIntensity,standardDeviation,1,length(randomVector));
	 auxVector = zeros(imSize(1), imSize(2));
	 auxVector(distanceMatrix(:)<threshold) = randomVector;
	 hsvImage(:,:,3) = reshape(auxVector,imSize(1),imSize(2));
	
	% Transform the image back to RGB color space
	rgbImage = hsv2rgb(hsvImage);
	
	% Present the image
% 	figure, imshow(rgbImage);
end