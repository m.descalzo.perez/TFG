function [next_value,is_finished,final_value,method_state] = psychMethodSIAM(varargin)
% Implements the method of single-interval adjustment matrix (SIAM) procedure
% defined in Kaernbach, J. Acoust. Soc. Am., 1990. It only considers the
% condition where no noise is presented. Thus, the step size only depends
% on the trial, but not on the response.
%
% This function, once initialised with a list of values to test, iterates
% through the list, returning one value at a time.
%
% If you want to run several interleaved psychometric methods, you can store
% the state variable returned when the function is initialised, initialise
% several psychometric functions, and pass in different state variables
% in an interleaved, alternating fashion.
%
% The second mode of operation is intended to be used if you have multiple
% parallel procedures running simultaneously. A state structure is initialised
% with the number of steps to take (num_values) and passed to the argument: An
% updated argument is returned from the function containing the next value to
% test. In this manner multiple state variables can be maintained and updated
% in parallell.
%
% To change
% Parameters:
% ===========
% possible_values - A vector of values to return one at a time
% init_value - A scalar number with the initial value
% stepsize - A vector of step sizes (if the step size is variable) or a scalar
% value (if the step size is constant)
% reversal2ChangeStepsize - A vector indicating when (the number of the reversal) the
% step size has to be changed. Empty or zero if the step_size is constant.
% num_reversals - Number of reversals to do
% num_reversal_trials - Number of trials to consider for the final result
% num_max_trials - Number of maximal trials to do
% history - History of presented values to the participant
% responses - History of responses done by the participant

% -----------------------------------------------------------------------------

	% WE USE THIS STRUCT TO STORE OUR DATA BETWEEN FUNCTION CALLS
	persistent PsychMethodSIAM_state

	% -----------------------------------------------------------------------------
	% CASE 1 - INITIALISATION
	% -----------------------------------------------------------------------------
	switch nargin
		case 7 % BUILD SOME STATE KNOWLEDGE
			method_state.possible_values = varargin{1};
			method_state.init_value = varargin{2};
			method_state.allStepsizes = varargin{3};
			method_state.reversal2ChangeStepsize = varargin{4};
			method_state.num_reversals = varargin{5};
			method_state.num_current_reversal = 0;
			method_state.num_current_trial = 1;
			method_state.num_reversal_trials = varargin{6};
			method_state.num_max_trials = varargin{7};
			
			% Get current index from current value
			method_state.current_index = find((round(method_state.possible_values*10000)/10000)==(round(method_state.init_value*10000)/10000));
			
			% Get the stepsizes from the specified stepsizes and the number
			% of trial to change the stepsize
			if (isempty(method_state.reversal2ChangeStepsize)) % Constant step size
				method_state.reversal2ChangeStepsize = 0;
			end
			if (min(method_state.reversal2ChangeStepsize(:)) ~= 0) || (max(method_state.reversal2ChangeStepsize(:)) > method_state.num_reversals)
				error('The number of reversals to change the step size should be between 0 (initial step size) and number of reversals');
			end
			if nonzeros(size(method_state.reversal2ChangeStepsize) ~= size(method_state.allStepsizes))
				error('Step sizes and number of reversals to change the step size do not agree')
			end
			% Construct the stepsize for each trial
			method_state.stepsize = method_state.allStepsizes(method_state.reversal2ChangeStepsize==method_state.num_current_reversal);
			
			% Check whether the initial value is a possible value
			if isempty(method_state.current_index)
			  error(['The init value ',  num2str(method_state.init_value),' is not a possible value']);
			end
			method_state.num_possible_values = numel(method_state.possible_values);
			method_state.current_value = method_state.possible_values( method_state.current_index );
			method_state.history = zeros(1, method_state.num_max_trials);
			method_state.history(1) = method_state.current_value;
			method_state.reversalHistory = zeros(1, method_state.num_max_trials);
			method_state.responses = ones(1, method_state.num_max_trials) .* (-1);
			method_state.final_value = nan;
			method_state.is_finished = false;

			% FILL IN RETURN VALUES
			next_value  = method_state.current_value;
			is_finished = false;
			final_value = nan;

			% UPDATE THE INTERNAL STATE
			PsychMethodSIAM_state = method_state;

		case {1, 2} % CONTINUATION
			if nargin == 1
				response = varargin{1};
				old_state = PsychMethodSIAM_state;
				[next_value, is_finished, final_value, method_state] = continue_method(response, old_state);
				PsychMethodSIAM_state = method_state;
			else
				response = varargin{1};
				old_state = varargin{2};
				[next_value, is_finished, final_value, method_state] = continue_method(response, old_state);
				PsychMethodSIAM_state = method_state;
			end

		otherwise
			error('type help PscyhMethodAscendingLimits for guidance.');

	end

% -----------------------------------------------------------------------------
% CONTINUING ------------------------------------------------------------------
% -----------------------------------------------------------------------------
function [next_value,is_finished,final_value,method_state] = continue_method(response, old_state)

	if ~checkFields(old_state)
		error('invalid PsychMethodAscendingLimits state - check that the method has been initialised correctly.');
	end
	
	method_state = old_state;
		
	method_state.responses(method_state.num_current_trial) = response;
	method_state.reversalHistory(method_state.num_current_trial) = method_state.num_current_reversal;

	% If the response is in the other direction than the previous
	% response, it means that a reversal has occurred
	if (method_state.num_current_trial > 1)
		if (method_state.responses(method_state.num_current_trial) ~= method_state.responses(method_state.num_current_trial-1))
			method_state.num_current_reversal = method_state.num_current_reversal + 1;
			if (sum(method_state.reversal2ChangeStepsize == method_state.num_current_reversal) > 0)
				method_state.stepsize = method_state.allStepsizes(method_state.reversal2ChangeStepsize == method_state.num_current_reversal);
				disp(['New step size: ', num2str(method_state.stepsize)]);
			end
		end
	end

	% NOT YET FINISHED
	if (method_state.num_current_trial < method_state.num_max_trials) && (method_state.num_current_reversal < method_state.num_reversals)
	
		% UPDATE OUR STATE KNOWLEDGE
		switch response
			case 0
				method_state.current_index      = old_state.current_index + old_state.stepsize;
			case 1
				method_state.current_index      = old_state.current_index - old_state.stepsize;
			otherwise
				error('Invalid response');
		end
		
		% Cast to limits
		if (method_state.current_index > method_state.num_possible_values)
			method_state.current_index = method_state.num_possible_values;
		end
		if (method_state.current_index < 1)
			method_state.current_index = 1;
		end
		
		method_state.num_current_trial = old_state.num_current_trial + 1;
		method_state.current_value = old_state.possible_values( method_state.current_index );
		method_state.history(method_state.num_current_trial) = method_state.current_value;

		% FILL IN RETURN VALUES
		next_value  = method_state.current_value;
		is_finished = false;
		final_value = nan;
		
		isFinished = 0;
		
		% Check ceiling or floor effect
		if method_state.num_current_trial > method_state.num_reversals
			% Ceiling effect
			if all(method_state.history(method_state.num_current_trial-method_state.num_reversals:method_state.num_current_trial)==max(method_state.possible_values))
				isFinished = 1;
				method_state.num_current_trial = method_state.num_current_trial-1;
				method_state.final_value = max(method_state.possible_values);
				disp('Stopping execution due to a ceiling effect');
				undesiredEffect = 1;
			end
			% Floor effect
			if all(method_state.history(method_state.num_current_trial-method_state.num_reversals:method_state.num_current_trial)==min(method_state.possible_values))
				isFinished = 1;
				method_state.num_current_trial = method_state.num_current_trial-1;
				method_state.final_value = min(method_state.possible_values);
				disp('Stopping execution due to a floor effect');
				undesiredEffect = 1;
			end
		end
	
	else
		isFinished = 1;
		undesiredEffect = 0;
	end
	
	if isFinished
		% Remove values which have not been used
		method_state.responses(method_state.num_current_trial+1:end) = [];
		method_state.history(method_state.num_current_trial+1:end) = [];
		method_state.reversalHistory(method_state.num_current_trial+1:end) = [];
		
		% UPDATE OUR STATE KNOWLEDGE
		method_state.current_value      = nan;
		if ~undesiredEffect
			method_state.final_value        = mean(method_state.history(method_state.reversalHistory>(method_state.num_reversals-method_state.num_reversal_trials-1)));
		end
		method_state.is_finished        = true;

		% FILL IN RETURN VALUES
		next_value  = nan;
		is_finished = true;
		final_value = method_state.final_value;
	end
  
	
% -----------------------------------------------------------------------------
% Check fields ------------------------------------------------------------------
% -----------------------------------------------------------------------------
function correct = checkFields(state)
	correct = 1;
	
	if (~isfield(state,'possible_values')		...
		|| ~isfield(state,'stepsize')			...
		|| ~isfield(state,'num_reversals')		...
		|| ~isfield(state,'num_current_reversal')...
		|| ~isfield(state,'num_max_trials')		...
		|| ~isfield(state,'num_current_trial')	...
		|| ~isfield(state,'num_reversal_trials')...
		|| ~isfield(state,'current_index')      ...
		|| ~isfield(state,'num_possible_values')...
		|| ~isfield(state, 'history')			...
		|| ~isfield(state,'current_value')      ...
		|| ~isfield(state,'final_value')        ...
		|| ~isfield(state,'is_finished'))
	
		correct = 0;
	end
