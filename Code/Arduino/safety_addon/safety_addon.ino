const unsigned short int buffer_size = 50; //Size of the buffer to calculate the average lx readings
const unsigned int lx_threshold = 51; // 51/1023 -> 0.25V/5V
const unsigned int time_threshold = 240; //number of miliseconds a stimulus can be on screen and be considered safe

unsigned int fsm_state = 0; //Current state of the FSM
unsigned int measures[buffer_size];
unsigned long int timeA;
unsigned long int timeB;
unsigned long int times;
unsigned int average;

void setup() {
  Serial.begin(115200);
  pinMode(A3, INPUT);
  for(unsigned short int i = 0; i < buffer_size; i++){
    measures[i] = 0;
  }
}
 
void loop() {
    for(unsigned short int i = 0; i < buffer_size-1; i++){
      measures[i] = measures[i+1];
      average = average + measures[i];
    }
    measures[buffer_size - 1] = analogRead(A3);
    times = millis();
    average = average + measures[buffer_size - 1];
    average = average / buffer_size;
    
    if(average > lx_threshold){ //If the average of the measures if greater than the threshold
      if(fsm_state xor 1){ //If fsm_state == 0;
        timeA = times; //Save the first timestamp
        fsm_state = 1;  
      }
    }else{ //If the average of the measures if leser than the threshold
      if(!(fsm_state xor 1)){ //If fsm_state == 1;
        timeB = times; //Save the second timestamp

        if((timeB-timeA)>time_threshold){
          Serial.println("h"); //Hazardous stimulus
        }else{
          Serial.println("s"); //Safe stimulus
        }
        fsm_state = 0;  
      }
   }
   average = 0;
}
