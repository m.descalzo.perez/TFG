Imports System.Net
Imports System.Net.Sockets
Module Program
    Dim server_ip As String = "192.168.1.2"
    Dim port As Integer = 30000

    Dim TCPClientz As Sockets.TcpClient
    Dim TCPClientStream As Sockets.NetworkStream

    Sub Main(args As String())
        Console.WriteLine("Waiting for the conenction")
        TCPClientz = New Sockets.TcpClient(server_ip, port) 'Connect to the matlab server
        TCPClientStream = TCPClientz.GetStream() 'Check the data comming
        Console.WriteLine("Connected to the Server")

        'Send data:
        Dim sendbytes() As Byte = System.Text.Encoding.ASCII.GetBytes("Hello from Visual Basic") 'codifie the message to be sent
        TCPClientz.Client.Send(sendbytes) 'Send the message to the server

        'System.Threading.Thread.Sleep(2000)

        'Receive data:
        Dim rcvbytes(TCPClientz.ReceiveBufferSize) As Byte
        TCPClientStream.Read(rcvbytes, 0, CInt(TCPClientz.ReceiveBufferSize))
        Console.WriteLine("New data received:")
        Console.WriteLine(System.Text.Encoding.ASCII.GetString(rcvbytes))

    End Sub
End Module
