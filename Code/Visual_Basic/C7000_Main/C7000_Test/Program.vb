Imports System
Imports System.Net
Imports System.Net.Sockets


Public Module GlobalVariables
    Public C7000Device As New C7000.SDK                                                ' C-7000 SDK Instance

    Public DeviceInfo As C7000.SDK.SKS_DEVICE_INFO                                     ' Device Information
    Public Config As C7000.SDK.SKS_MEAS_CONFIG                                         ' Measuring Configration
    Public MeasResult As C7000.SDK.SKS_MEAS_RESULT                                     ' Measurement Result
    Public Sequence As SequenceCtrKey = SequenceCtrKey.IDLE                            ' Sequence Control Key
    Public Enum SequenceCtrKey As Byte                                                 ' Sequence Control Key Code
        IDLE = 0                                                                        '   Idle Sequence
        FLASH_STANDBY                                                                   '   Flash Standby
        MEASURING                                                                       '   Measuring
    End Enum

    Public Result_Illmi As String
    Public Spectrum_1nm(C7000.SDK.SKF_WAVELENGTH_1NM_COUNT - 1) As Single
    Public Spectrum_5nm(C7000.SDK.SKF_WAVELENGTH_5NM_COUNT - 1) As Single
End Module

Module Program
    Sub Main(args As String())
        Dim SDKResult As Long
        Dim Label_Status As String
        Dim Label_Ring As String

        Dim server_ip As String
        Dim Port As Integer = 30000 'Port where the server is allocated
        Dim VBClient As Sockets.TcpClient
        Dim VBClientStream As Sockets.NetworkStream

        Dim num_measures As Int16 = 0
        Dim index As Int16 = 0
        Dim average_lx As Double
        Dim min_lx As Double
        Dim max_lx As Double

        '   >>>>>  Initialize Array of Measurement Results  <<<<<
        ReDim MeasResult.SpectralData_1nm(C7000.SDK.SKF_WAVELENGTH_1NM_COUNT - 1)
        ReDim MeasResult.SpectralData_5nm(C7000.SDK.SKF_WAVELENGTH_5NM_COUNT - 1)
        ReDim MeasResult.ColorRenditionIndexes.Ri(C7000.SDK.SKF_CRI_COUNT - 1)
        ReDim MeasResult.TM30.HueBins(C7000.SDK.SKF_TM30_BINS_COUNT - 1)    '#1101

        Console.WriteLine("            >>>>> C-7000 Controller - Client <<<<<   ")

        'Server connection secuence:
        'Console.WriteLine("Enter server IP") 'IP of the server
        'server_ip = Console.ReadLine()
        Console.WriteLine("Connecting to the server...")
        VBClient = New Sockets.TcpClient("localhost", Port)
        VBClientStream = VBClient.GetStream() 'Check the data for incomming data
        Console.WriteLine("Connection to the server sucessful")

        'Connection of the C-7000 secuence:
        SDKResult = C7000Device.SK_Connect() 'Start connection to the device
        SDKResult = C7000Device.SK_GetDeviceInfo(DeviceInfo) 'Get basic info of the device (Ring position, status...)
        SDKResult = C7000Device.SK_SetRemoteMode(C7000.SDK.SKF_REMOTE.REMOTE_ON) 'Prepare the device to work in Remote Mode (via software)
        SDKResult = C7000Device.SK_GetMeasConfig(Config) 'Get the current config values (exposure time, field of view...)
        'If SDKResult is always 0 --> success, else error happened
        Console.WriteLine("Successful connection whith the C-7000")

        'SDKResult = C7000Device.SK_SetMeasConfig(Config) 'Change the Measurmen configuration here

        While True 'Main Loop
            While True <> VBClientStream.DataAvailable 'While there's no new data
                'Wait
            End While
            Dim data_rx(VBClient.ReceiveBufferSize) As Byte
            VBClientStream.Read(data_rx, 0, CInt(VBClient.ReceiveBufferSize))
            num_measures = BitConverter.ToInt16(data_rx, 0)

            If -1 = num_measures Then 'If -1 is received from the server
                Exit While      'Finish the program
            ElseIf -2 = num_measures Then 'If -2 is received from the server
                'Send the spectrum of the last measure made
                Console.WriteLine("Sending spectrum of the last measure...")

                'Build the data of the packet
                Dim temp_array As Byte()
                ReDim temp_array(4)

                Dim Y_1nm_byte As Byte()
                ReDim Y_1nm_byte((C7000.SDK.SKF_WAVELENGTH_1NM_COUNT - 1) * 4)
                For a_index As Integer = 0 To Spectrum_1nm.Length - 1
                    temp_array = BitConverter.GetBytes(Spectrum_1nm(a_index))
                    Y_1nm_byte(a_index * 4) = temp_array(0)
                    If 400 > a_index Then
                        Y_1nm_byte((a_index * 4) + 1) = temp_array(1)
                        Y_1nm_byte((a_index * 4) + 2) = temp_array(2)
                        Y_1nm_byte((a_index * 4) + 3) = temp_array(3)
                    End If
                Next

                Dim Y_5nm_byte As Byte()
                ReDim Y_5nm_byte((C7000.SDK.SKF_WAVELENGTH_5NM_COUNT - 1) * 4)
                For a_index As Integer = 0 To Spectrum_5nm.Length - 1
                    temp_array = BitConverter.GetBytes(Spectrum_5nm(a_index))
                    Y_5nm_byte(a_index * 4) = temp_array(0)
                    If 80 > a_index Then
                        Y_5nm_byte((a_index * 4) + 1) = temp_array(1)
                        Y_5nm_byte((a_index * 4) + 2) = temp_array(2)
                        Y_5nm_byte((a_index * 4) + 3) = temp_array(3)
                    End If
                Next


                Dim data_to_send As Byte() = Y_1nm_byte.Concat(Y_5nm_byte).ToArray 'Concatenate the 2 arrays
                VBClient.Client.Send(data_to_send) 'Send the packet

            ElseIf num_measures > 0 Then 'If a positive number of measures is requested, take thous measures
                Dim Lux(num_measures - 1) As Double 'Array of illuminances mesuared


                Console.WriteLine()
                Console.WriteLine("Starting " + Convert.ToString(num_measures) + " measures...")
                While num_measures > 0
                    While True
                        If True = Check_Status(C7000Device.SK_GetDeviceInfo(DeviceInfo)) Then           ' If everything is OK
                            Label_Status = Get_DeveiceStats(DeviceInfo)                            ' Get Device Status
                            Label_Ring = Get_RingStatus(DeviceInfo)                                ' Get Light Selection Ring

                            Select Case Sequence
                                Case SequenceCtrKey.IDLE
                                    Sequence = Start_Measurement()
                                Case SequenceCtrKey.MEASURING
                                    If C7000.SDK.SKF_STATUS_DEVICE.BUSY_MEASURING <> DeviceInfo.Status Then 'If measurement is finished
                                        Call Monitor_MeasuringResult() 'Take the results
                                        Sequence = SequenceCtrKey.IDLE 'Indicate we are back to IDLE
                                        Exit While 'Exit While loop
                                    End If
                            End Select
                        End If
                    End While
                    If "Under" = Result_Illmi Then 'Translate the "too low for reading" to "0"
                        Result_Illmi = "0"
                    End If
                    Lux(index) = CDbl(Result_Illmi) 'Cast from string to Double
                    Console.WriteLine("  Measure " + Convert.ToString(index + 1) + ": " + Result_Illmi + " lx")
                    index += 1
                    num_measures -= 1
                End While

                Console.WriteLine("Measures completed")
                Console.WriteLine("")
                average_lx = 0
                For i As Int16 = 0 To (index - 1)
                    average_lx += Lux(i)
                Next i
                average_lx = average_lx / index
                max_lx = Lux.Max()
                min_lx = Lux.Min()

                Console.Write("Mean Value: ")
                Console.WriteLine(average_lx)
                Console.Write("Max Value: ")
                Console.WriteLine(max_lx)
                Console.Write("Min Value: ")
                Console.WriteLine(min_lx)
                index = 0

                'Creation of the packet to be send to the server
                Dim mean_byte As Byte() = BitConverter.GetBytes(average_lx)
                Dim max_byte As Byte() = BitConverter.GetBytes(max_lx)
                Dim min_byte As Byte() = BitConverter.GetBytes(min_lx)
                Dim data_tx() As Byte = mean_byte.Concat(max_byte.Concat(min_byte).ToArray).ToArray 'Concatenate the 3 values

                VBClient.Client.Send(data_tx) 'Send it
            End If



        End While
        'Disconnection secuence:
        SDKResult = C7000Device.SK_SetRemoteMode(C7000.SDK.SKF_REMOTE.REMOTE_OFF) 'Back to manual mode (pressing buttons)
        SDKResult = C7000Device.SK_Disconnect() 'Stop the connection to the device
        'If SDKResult is always 0 --> success, else error happened
    End Sub














    Private Function Get_DeveiceStats(ByVal DeviceInfor As C7000.SDK.SKS_DEVICE_INFO) As String
        Select Case DeviceInfor.Status
            Case C7000.SDK.SKF_STATUS_DEVICE.ERROR_HW                                       ' H/W Error
                Get_DeveiceStats = "H/W Error"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_MEASURING                                 ' Measuring
                Get_DeveiceStats = "Measuring"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_FLASH_STANDBY                             ' Flash standby
                Get_DeveiceStats = "Flash Standby"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_DARK_CALIBRATION                          ' Dark Calibration
                Get_DeveiceStats = "Dark Calibration"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_INITIALIZING                              ' Initilizing
                Get_DeveiceStats = "Initilize"
            Case C7000.SDK.SKF_STATUS_DEVICE.IDLE_OUT_MEAS                                  ' Out of Measurement Range
                Get_DeveiceStats = "Idle (Out of Measurement Range)"
            Case C7000.SDK.SKF_STATUS_DEVICE.IDLE                                           ' Idle
                Get_DeveiceStats = "Idle"
            Case Else                                                                       ' else Status
                Get_DeveiceStats = "Other"
        End Select
    End Function

    Private Function Get_RingStatus(ByVal DeviceInfor As C7000.SDK.SKS_DEVICE_INFO) As String
        Select Case DeviceInfor.Ring
            Case C7000.SDK.SKF_STATUS_RING.UNPOSITIONED                                     ' Unpositioned
                Get_RingStatus = "Light Selection Ring : Unpositioned"
            Case C7000.SDK.SKF_STATUS_RING.CAL                                              ' Cal
                Get_RingStatus = "Light Selection Ring : Cal"
            Case C7000.SDK.SKF_STATUS_RING.LOW                                              ' Low
                Get_RingStatus = "Light Selection Ring : Low"
            Case C7000.SDK.SKF_STATUS_RING.HIGH                                             ' High
                Get_RingStatus = "Light Selection Ring : High"
            Case Else                                                                       ' else 
                Get_RingStatus = "Other"
        End Select
    End Function

    Private Function Check_Status(ByVal Status As Long) As Boolean
        Dim Message As String

        Select Case Status
            Case C7000.SDK.SKF_STATUS_COM.SUCCESS                                           ' Successful Completion
                Message = ""
            Case C7000.SDK.SKF_STATUS_COM.EXECUTE_ERR                                       ' API Execution Error
                Message = "API Execution Error"
            Case C7000.SDK.SKF_STATUS_COM.NO_DEVICE                                         ' No Device to Detect
                Message = "No Device to Detect"
            Case C7000.SDK.SKF_STATUS_COM.HANDLE_ERR                                        ' Handle Error
                Message = "Handle Error"
            Case C7000.SDK.SKF_STATUS_COM.PARAM_ERR                                         ' Parameter Error
                Message = "Parameter Error"
            Case C7000.SDK.SKF_STATUS_COM.MEM_ALLOC_ERR                                     ' Memory Allocation Failed
                Message = "Memory Allocation Failed"
            Case C7000.SDK.SKF_STATUS_COM.CREATE_EVENT_ERR                                  ' Event Creation Failed
                Message = "Event Creation Failed"
            Case C7000.SDK.SKF_STATUS_COM.WAIT_TIMEOUT                                      ' Timeout
                Message = "Timeout"
            Case C7000.SDK.SKF_STATUS_COM.ACK_NAK_ERR                                       ' ACK/NAK Response Error
                Message = "ACK/NAK Response Error"
            Case C7000.SDK.SKF_STATUS_COM.NAK_COMMAND                                       ' NAK Error (Command Error) 
                Message = "NAK Error (Command Error)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_PARAM                                         ' NAK Error (Parameter Error)
                Message = "NAK Error (Parameter Error)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_UNSUPPORTED_MODEL                             ' NAK Error (Unsupported Model)
                Message = "NAK Error (Unsupported Model)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_UNSUPPORTED_COMMAND                           ' NAK Error (Unsupported Command)
                Message = "NAK Error (Unsupported Command)"
            Case Else                                                                       ' else Error
                Message = "Status Code = " & Status.ToString("X8")
        End Select

        If Message <> "" Then                                                               ' Error Message Display
            Console.WriteLine(Message)
            Call C7000Device.SK_Disconnect()
        End If

        Check_Status = (C7000.SDK.SKF_STATUS_COM.SUCCESS = Status)
    End Function

    Private Function Start_Measurement() As SequenceCtrKey
        Dim CtrNum As Integer = 0                                                       ' Sequence Control Number
        Dim SDKResult As Long                                                           ' SDK Result
        Dim RingPosition As Boolean = True                                              ' Light Selection Ring Position Check Flag (Position OK!)

        Start_Measurement = SequenceCtrKey.IDLE                                         ' Next Sequence = Idle

        Select Case DeviceInfo.Ring                                                     ' Light Selection Ring Position Check
            Case C7000.SDK.SKF_STATUS_RING.LOW

            Case C7000.SDK.SKF_STATUS_RING.HIGH
                If C7000.SDK.SKF_MEASURING_MODE.AMBIENT = Config.MeasuringMode Then
                    RingPosition = False                                                ' Position Check NG !
                End If

            Case Else
                RingPosition = False                                                    ' Position Check NG !
        End Select

        If True = RingPosition Then                                                     ' Light Selection Ring Position Check OK ?!
            Do
                Select Case CtrNum
                    Case 0  '   >>>>>  Set Measurement Configration  <<<<<
                        SDKResult = C7000Device.SK_SetMeasConfig(Config)

                    Case 1  '   >>>>>  Start Measurement  <<<<<
                        SDKResult = C7000Device.SK_StartMeasuring()

                    Case 2
                        Select Case Config.MeasuringMode                                ' Next Sequence Set
                            Case C7000.SDK.SKF_MEASURING_MODE.AMBIENT
                                Start_Measurement = SequenceCtrKey.MEASURING            ' Ambient then Next Sequence = Measuring

                            Case C7000.SDK.SKF_MEASURING_MODE.CORDLESS_FLASH
                                Start_Measurement = SequenceCtrKey.FLASH_STANDBY        ' Cordless then Next Sequence = Flash standby

                            Case C7000.SDK.SKF_MEASURING_MODE.CORD_FLASH                ' Cordin then Next Sequence = Measuring
                                Start_Measurement = SequenceCtrKey.MEASURING
                        End Select
                End Select

                CtrNum += 1
            Loop Until (Check_Status(SDKResult) = False Or CtrNum > 2)
        Else
            Console.WriteLine("Please check Light Selection Ring position.")
        End If
    End Function

    Private Function Monitor_MeasuringResult() As Boolean
        Dim SDKResult As Long                                                           ' SDK Result

        SDKResult = C7000Device.SK_GetMeasuringResult(MeasResult)                       ' Get Measurement Result
        Monitor_MeasuringResult = Check_Status(SDKResult)

        With MeasResult
            Dim pt As Integer = 0
            If True = Monitor_MeasuringResult And C7000.SDK.SKF_RESULT_VALUE.VALUE_ON = .ResultFlag Then                   ' If measurment is finished
                Result_Illmi = .Illuminance.LUX
                'Set Spectrum 1nm
                For wl As Integer = 380 To 780
                    'Add  XY to 1nm arrat(wl, p)
                    Spectrum_1nm(pt) = .SpectralData_1nm(pt)
                    pt += 1
                Next
                'Set Spectrum 5nm
                pt = 0
                For wl As Integer = 380 To 780 Step 5
                    'Add  XY to 1nm arrat(wl, p)
                    Spectrum_5nm(pt) = .SpectralData_5nm(pt)
                    pt += 1
                Next

            Else
                Result_Illmi = ""
                'Add dummy data to the spectrum arrays
                Spectrum_1nm(0) = 0
                Spectrum_5nm(0) = 0.8
            End If
        End With
    End Function

End Module