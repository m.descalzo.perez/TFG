﻿'
'****************************************************************************************
'**********                                                                    **********
'**********                SEKONIC C-7000 SDK Sample Program                   **********
'**********                                                                    **********
'**********                        2017/04/05  12:00                           **********
'**********                                                                    **********
'**********                      by SEKONIC CORPORATION                        **********
'**********                                                                    **********
'****************************************************************************************
'
'                              <<< Version History >>>
'
'       2017/04/05 Ver1.00
'           ・Release start
'
'
Public Class frm_Main
    Private C7000Device As New C7000.SDK                                                ' C-7000 SDK Instance

    Private DeviceInfo As C7000.SDK.SKS_DEVICE_INFO                                     ' Device Information
    Private Config As C7000.SDK.SKS_MEAS_CONFIG                                         ' Measuring Configration
    Private MeasResult As C7000.SDK.SKS_MEAS_RESULT                                     ' Measurement Result 
    Private Sequence As SequenceCtrKey = SequenceCtrKey.IDLE                            ' Sequence Control Key

    Private Enum SequenceCtrKey As Byte                                                 ' Sequence Control Key Code
        IDLE = 0                                                                        '   Idle Sequence
        FLASH_STANDBY                                                                   '   Flash Standby
        MEASURING                                                                       '   Measuring
    End Enum

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        '   >>>>>  Title & Version Display  <<<<<
        With My.Application.Info
            Me.Text = .Title & "  [Ver" &
                      .Version.Major & "." &
                      .Version.Minor.ToString("D2") & "]"
        End With

        '   >>>>>  Initialize Array of Measurement Results  <<<<<
        ReDim MeasResult.SpectralData_1nm(C7000.SDK.SKF_WAVELENGTH_1NM_COUNT - 1)
        ReDim MeasResult.SpectralData_5nm(C7000.SDK.SKF_WAVELENGTH_5NM_COUNT - 1)
        ReDim MeasResult.ColorRenditionIndexes.Ri(C7000.SDK.SKF_CRI_COUNT - 1)
        ReDim MeasResult.TM30.HueBins(C7000.SDK.SKF_TM30_BINS_COUNT - 1)    '#1101
    End Sub

    Private Sub frm_Main_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        Call Button_Disconnect_Click(Nothing, Nothing)                                  ' Device Disconnect
    End Sub

    Private Sub Button_Connect_Click(sender As System.Object, e As System.EventArgs) Handles Button_Connect.Click
        Dim CtrNum As Integer = 0                                                       ' Sequence Control Number
        Dim SDKResult As Long                                                           ' SDK Result

        Do
            Select Case CtrNum
                Case 0  '   >>>>>  Device Connect  <<<<<
                    SDKResult = C7000Device.SK_Connect()

                Case 1  '   >>>>>  Get Device Information  <<<<<
                    SDKResult = C7000Device.SK_GetDeviceInfo(DeviceInfo)

                Case 2  '   >>>>>  Now Initilizeing ?!  <<<<<
                    If DeviceInfo.Status = C7000.SDK.SKF_STATUS_DEVICE.BUSY_INITIALIZING Then ' if Initializeing then Connect Control Abort
                        MessageBox.Show("Now Initilizeing...",
                                        My.Application.Info.Title,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information)
                        CtrNum = 5                                                      ' Process End !
                    End If

                Case 3  '   >>>>>  Remote Mode On  <<<<<
                    SDKResult = C7000Device.SK_SetRemoteMode(C7000.SDK.SKF_REMOTE.REMOTE_ON)

                Case 4  '   >>>>>  Get Measurement Configration  <<<<<
                    SDKResult = C7000Device.SK_GetMeasConfig(Config)

                Case 5  '   >>>>>  GUI Setting  <<<<<
                    Combo_FieldView.SelectedIndex = Config.FieldOfView                  ' Set Measurement Configration
                    Combo_MeasuringMode.SelectedIndex = Config.MeasuringMode
                    Combo_ExposureTime.SelectedIndex = Config.ExposureTime
                    Combo_ShutterSpeed.SelectedIndex = Config.ShutterSpeed
                    Combo_MeasMethod.SelectedIndex = Config.MeasuringMethod
                    Call Combo_MeasuringMode_SelectedIndexChanged(Nothing, Nothing)

                    Group_Configuration.Enabled = True

                    Button_Connect.Enabled = False
                    Button_DisConnect.Enabled = True
                    Button_MeasStart.Enabled = True
                    Button_StandbyStop.Enabled = False

                    Sequence = SequenceCtrKey.IDLE
                    IntervalTimer.Enabled = True                                        ' Interval Timer Start
            End Select

            CtrNum += 1                                                                 ' Next Sequence
        Loop Until (Check_Status(SDKResult) = False Or CtrNum > 5)
    End Sub

    Private Sub Button_Disconnect_Click(sender As System.Object, e As System.EventArgs) Handles Button_DisConnect.Click
        Dim CtrNum As Integer = 0                                                       ' Sequence Control Number
        Dim SDKResult As Long                                                           ' SDK Result

        IntervalTimer.Enabled = False                                                   ' Interval Timer Stop
        Do
            Select Case CtrNum
                Case 0  '   >>>>>  Remote Mode Off  <<<<<
                    SDKResult = C7000Device.SK_SetRemoteMode(C7000.SDK.SKF_REMOTE.REMOTE_OFF)

                Case 1  '   >>>>>  Device Disconnect  <<<<<
                    SDKResult = C7000Device.SK_Disconnect()
            End Select

            CtrNum += 1                                                                 ' Next Sequence Control Number Set
        Loop Until (Check_Status(SDKResult) = False Or CtrNum > 1)

        Group_Configuration.Enabled = False

        Button_Connect.Enabled = True
        Button_DisConnect.Enabled = False
        Button_MeasStart.Enabled = False
        Button_StandbyStop.Enabled = False

        Label_Status.Text = "-"                                                         ' Monitor Device Status
        Label_Ring.Text = "-"                                                           ' Monitor Light Selection Ring
    End Sub

    Private Sub Button_MeasStart_Click(sender As System.Object, e As System.EventArgs) Handles Button_MeasStart.Click
        Sequence = Start_Measurement()                                                  ' Measurement Start
    End Sub

    Private Sub Button_StandbyStop_Click(sender As System.Object, e As System.EventArgs) Handles Button_StandbyStop.Click
        Check_Status(C7000Device.SK_StopStandby())                                      ' Measurement Stop
    End Sub

    Private Sub Combo_FieldView_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Combo_FieldView.SelectedIndexChanged
        Config.FieldOfView = CType(Combo_FieldView.SelectedIndex, C7000.SDK.SKF_FIELD_OF_VIEW)
    End Sub

    Private Sub Combo_MeasuringMode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Combo_MeasuringMode.SelectedIndexChanged
        Config.MeasuringMode = CType(Combo_MeasuringMode.SelectedIndex, C7000.SDK.SKF_MEASURING_MODE)

        Label_ExposureTime.Enabled = (Config.MeasuringMode = C7000.SDK.SKF_MEASURING_MODE.AMBIENT)
        Combo_ExposureTime.Enabled = (Config.MeasuringMode = C7000.SDK.SKF_MEASURING_MODE.AMBIENT)
        Label_MeasMethod.Enabled = (Config.MeasuringMode = C7000.SDK.SKF_MEASURING_MODE.AMBIENT)    '#1101
        Combo_MeasMethod.Enabled = (Config.MeasuringMode = C7000.SDK.SKF_MEASURING_MODE.AMBIENT)    '#1101
        Label_ShutterSpeed.Enabled = (Config.MeasuringMode <> C7000.SDK.SKF_MEASURING_MODE.AMBIENT)
        Combo_ShutterSpeed.Enabled = (Config.MeasuringMode <> C7000.SDK.SKF_MEASURING_MODE.AMBIENT)
    End Sub

    Private Sub Combo_ExposureTime_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Combo_ExposureTime.SelectedIndexChanged
        Config.ExposureTime = CType(Combo_ExposureTime.SelectedIndex, C7000.SDK.SKF_EXPOSURE_TIME)
    End Sub

    Private Sub Combo_ShutterSpeed_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Combo_ShutterSpeed.SelectedIndexChanged
        Config.ShutterSpeed = CType(Combo_ShutterSpeed.SelectedIndex, C7000.SDK.SKF_SHUTTER_SPEED)
    End Sub

    Private Sub Combo_MeasMethod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo_MeasMethod.SelectedIndexChanged
        Config.MeasuringMethod = CType(Combo_MeasMethod.SelectedIndex, C7000.SDK.SKF_MEASURING_METHOD)
    End Sub

    Private Sub IntervalTimer_Tick(sender As System.Object, e As System.EventArgs) Handles IntervalTimer.Tick
        If True = Check_Status(C7000Device.SK_GetDeviceInfo(DeviceInfo)) Then           ' Check Device Status Code
            Label_Status.Text = Get_DeveiceStats(DeviceInfo)                            ' Get Device Status
            Label_Ring.Text = Get_RingStatus(DeviceInfo)                                ' Get Light Selection Ring

            Select Case Sequence
                Case SequenceCtrKey.IDLE
                    If 0 <> (C7000.SDK.SKF_STATUS_BUTTON.MEASURING And DeviceInfo.Button) Then ' Measuring Button ON ?!
                        Sequence = Start_Measurement()
                    End If
                Case SequenceCtrKey.FLASH_STANDBY
                    If C7000.SDK.SKF_STATUS_DEVICE.BUSY_FLASH_STANDBY <> DeviceInfo.Status Then
                        Sequence = SequenceCtrKey.MEASURING
                    End If
                Case SequenceCtrKey.MEASURING
                    If C7000.SDK.SKF_STATUS_DEVICE.BUSY_MEASURING <> DeviceInfo.Status Then
                        Call Monitor_MeasuringResult()

                        Button_DisConnect.Enabled = True
                        Button_MeasStart.Enabled = True
                        Button_StandbyStop.Enabled = False

                        Sequence = SequenceCtrKey.IDLE
                    End If
            End Select
        End If
    End Sub

    Private Function Start_Measurement() As SequenceCtrKey
        Dim CtrNum As Integer = 0                                                       ' Sequence Control Number
        Dim SDKResult As Long                                                           ' SDK Result
        Dim RingPosition As Boolean = True                                              ' Light Selection Ring Position Check Flag (Position OK!)

        Start_Measurement = SequenceCtrKey.IDLE                                         ' Next Sequence = Idle

        Select Case DeviceInfo.Ring                                                     ' Light Selection Ring Position Check
            Case C7000.SDK.SKF_STATUS_RING.LOW

            Case C7000.SDK.SKF_STATUS_RING.HIGH
                If C7000.SDK.SKF_MEASURING_MODE.AMBIENT = Config.MeasuringMode Then
                    RingPosition = False                                                ' Position Check NG !
                End If

            Case Else
                RingPosition = False                                                    ' Position Check NG !
        End Select

        If True = RingPosition Then                                                     ' Light Selection Ring Position Check OK ?!
            Do
                Select Case CtrNum
                    Case 0  '   >>>>>  Set Measurement Configration  <<<<<
                        SDKResult = C7000Device.SK_SetMeasConfig(Config)

                    Case 1  '   >>>>>  Start Measurement  <<<<<
                        SDKResult = C7000Device.SK_StartMeasuring()

                    Case 2
                        Button_DisConnect.Enabled = False
                        Button_MeasStart.Enabled = False
                        Select Case Config.MeasuringMode                                ' Next Sequence Set
                            Case C7000.SDK.SKF_MEASURING_MODE.AMBIENT
                                Start_Measurement = SequenceCtrKey.MEASURING            ' Ambient then Next Sequence = Measuring

                            Case C7000.SDK.SKF_MEASURING_MODE.CORDLESS_FLASH
                                Start_Measurement = SequenceCtrKey.FLASH_STANDBY        ' Cordless then Next Sequence = Flash standby
                                Button_StandbyStop.Enabled = True

                            Case C7000.SDK.SKF_MEASURING_MODE.CORD_FLASH                ' Cordin then Next Sequence = Measuring
                                Start_Measurement = SequenceCtrKey.MEASURING
                        End Select
                End Select

                CtrNum += 1
            Loop Until (Check_Status(SDKResult) = False Or CtrNum > 2)
        Else
            MessageBox.Show("Please check Light Selection Ring position.",
                            My.Application.Info.Title,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information)
        End If
    End Function

    Private Function Monitor_MeasuringResult() As Boolean
        Dim SDKResult As Long                                                           ' SDK Result

        SDKResult = C7000Device.SK_GetMeasuringResult(MeasResult)                       ' Get Measurement Result
        Monitor_MeasuringResult = Check_Status(SDKResult)

        'With ChartSpectrum.Series
        '    .Clear()
        '    .Add("Dummy")
        '    .Item("Dummy").Points.AddXY(0, 1)
        'End With

        With MeasResult
            If True = Monitor_MeasuringResult And
               C7000.SDK.SKF_RESULT_VALUE.VALUE_ON = .ResultFlag Then                   ' Measurement Result OK ?!

                Result_Tcp.Text = .ColorTemperature.Tcp
                Result_Delta_uv.Text = .ColorTemperature.Delta_uv
                Result_Illmi.Text = .Illuminance.LUX
                Result_PeakWaveLength.Text = .PeakWavelength
                Result_X.Text = .Tristimulus.X_Value
                Result_Y.Text = .Tristimulus.Y_Value
                Result_Z.Text = .Tristimulus.Z_Value
                Result_CIE1931x.Text = .CIE1931.x_Value
                Result_CIE1931y.Text = .CIE1931.y_Value
                Result_CIE1931z.Text = .CIE1931.z_Value
                Result_CIE1976u.Text = .CIE1976.ud_Value
                Result_CIE1976v.Text = .CIE1976.vd_Value
                Result_DWL.Text = .DWL.WAVELENGTH
                Result_Purity.Text = .DWL.EXCITATION_PURITY
                Result_PPFD.Text = .PPFD

                Result_CRI_Ra.Text = .ColorRenditionIndexes.Ra
                Result_CRI_R1.Text = .ColorRenditionIndexes.Ri(0)
                Result_CRI_R2.Text = .ColorRenditionIndexes.Ri(1)
                Result_CRI_R3.Text = .ColorRenditionIndexes.Ri(2)
                Result_CRI_R4.Text = .ColorRenditionIndexes.Ri(3)
                Result_CRI_R5.Text = .ColorRenditionIndexes.Ri(4)
                Result_CRI_R6.Text = .ColorRenditionIndexes.Ri(5)
                Result_CRI_R7.Text = .ColorRenditionIndexes.Ri(6)
                Result_CRI_R8.Text = .ColorRenditionIndexes.Ri(7)
                Result_CRI_R9.Text = .ColorRenditionIndexes.Ri(8)
                Result_CRI_R10.Text = .ColorRenditionIndexes.Ri(9)
                Result_CRI_R11.Text = .ColorRenditionIndexes.Ri(10)
                Result_CRI_R12.Text = .ColorRenditionIndexes.Ri(11)
                Result_CRI_R13.Text = .ColorRenditionIndexes.Ri(12)
                Result_CRI_R14.Text = .ColorRenditionIndexes.Ri(13)
                Result_CRI_R15.Text = .ColorRenditionIndexes.Ri(14)

                Result_TM30Rf.Text = .TM30.Rf
                Result_TM30Rg.Text = .TM30.Rg
                Result_SSIt.Text = .SSI.SSIt
                Result_SSId.Text = .SSI.SSId
                Result_SSI1.Text = .SSI.SSI1
                Result_SSI2.Text = .SSI.SSI2
                Result_TLCI.Text = .TLCI.TLCI
                Result_TLMF.Text = .TLCI.TLMF

                With ChartSpectrum
                    Dim Series_1nm As String = "1nm"
                    Dim Series_5nm As String = "5nm"
                    Dim pt As Integer = 0
                    'Item Clear
                    .Series.Clear()
                    'Add Spectrum Item
                    .Series.Add(Series_5nm)
                    .Series.Add(Series_1nm)
                    'Set Item Format
                    With .Series.Item(Series_1nm)
                        .ChartType = DataVisualization.Charting.SeriesChartType.Line
                        .BorderWidth = 3
                    End With
                    With .Series.Item(Series_5nm)
                        .ChartType = DataVisualization.Charting.SeriesChartType.Column
                    End With
                    'Set Spectrum 1nm
                    For wl As Integer = 380 To 780
                        Dim p As Single = MeasResult.SpectralData_1nm(pt)
                        '
                        .Series.Item(Series_1nm).Points.AddXY(wl, p)
                        pt += 1
                    Next
                    'Set Spectrum 5nm
                    pt = 0
                    For wl As Integer = 380 To 780 Step 5
                        Dim p As Single = MeasResult.SpectralData_5nm(pt)
                        '
                        .Series.Item(Series_5nm).Points.AddXY(wl, p)
                        pt += 1
                    Next
                End With

                With ChartTM30
                    'Item Clear
                    .Series.Item(0).Points.Clear()
                    .Series.Item(1).Points.Clear()
                    'Plot TM-30-18 Hue Bin Data
                    For pt As Integer = 0 To 16
                        Dim hue_dat As Integer = If(pt < 16, pt, 0)
                        Dim test_x As Single = MeasResult.TM30.HueBins(hue_dat).Test_X
                        Dim test_y As Single = MeasResult.TM30.HueBins(hue_dat).Test_Y
                        Dim refer_x As Single = MeasResult.TM30.HueBins(hue_dat).Refer_X
                        Dim refer_y As Single = MeasResult.TM30.HueBins(hue_dat).Refer_Y
                        '
                        .Series.Item(0).Points.AddXY(test_x, test_y)
                        .Series.Item(1).Points.AddXY(refer_x, refer_y)
                    Next
                End With
            Else
                Result_Tcp.Text = ""                                                    ' Measurement Result Clear
                Result_Delta_uv.Text = ""
                Result_Illmi.Text = ""
                Result_PeakWaveLength.Text = ""
                Result_X.Text = ""
                Result_Y.Text = ""
                Result_Z.Text = ""
                Result_CIE1931x.Text = ""
                Result_CIE1931y.Text = ""
                Result_CIE1931z.Text = ""
                Result_CIE1976u.Text = ""
                Result_CIE1976v.Text = ""
                Result_DWL.Text = ""
                Result_Purity.Text = ""
                Result_PPFD.Text = ""

                Result_CRI_Ra.Text = ""
                Result_CRI_R1.Text = ""
                Result_CRI_R2.Text = ""
                Result_CRI_R3.Text = ""
                Result_CRI_R4.Text = ""
                Result_CRI_R5.Text = ""
                Result_CRI_R6.Text = ""
                Result_CRI_R7.Text = ""
                Result_CRI_R8.Text = ""
                Result_CRI_R9.Text = ""
                Result_CRI_R10.Text = ""
                Result_CRI_R11.Text = ""
                Result_CRI_R12.Text = ""
                Result_CRI_R13.Text = ""
                Result_CRI_R14.Text = ""
                Result_CRI_R15.Text = ""

                Result_TM30Rf.Text = ""
                Result_TM30Rg.Text = ""
                Result_SSIt.Text = ""
                Result_SSId.Text = ""
                Result_SSI1.Text = ""
                Result_SSI2.Text = ""
                Result_TLCI.Text = ""
                Result_TLMF.Text = ""

                With ChartSpectrum
                    Dim Series_1nm As String = "1nm"
                    Dim Series_5nm As String = "5nm"
                    'Item Clear
                    .Series.Clear()
                    'Add Spectrum Item
                    .Series.Add(Series_5nm)
                    .Series.Add(Series_1nm)
                    'Add Dummy Data
                    .Series.Item(Series_5nm).Points.AddXY(0.0, 0.0)
                    .Series.Item(Series_1nm).Points.AddXY(0.0, 0.8)
                End With

                With ChartTM30
                    'Item Clear
                    .Series.Item(0).Points.Clear()
                    .Series.Item(1).Points.Clear()
                    'Plot TM-30-18 Hue Bin Data
                    .Series.Item(0).Points.AddXY(-2.0, -2.0)
                End With
            End If
        End With
    End Function

    Private Function Check_Status(ByVal Status As Long) As Boolean
        Dim Message As String

        Select Case Status
            Case C7000.SDK.SKF_STATUS_COM.SUCCESS                                           ' Successful Completion
                Message = ""
            Case C7000.SDK.SKF_STATUS_COM.EXECUTE_ERR                                       ' API Execution Error
                Message = "API Execution Error"
            Case C7000.SDK.SKF_STATUS_COM.NO_DEVICE                                         ' No Device to Detect
                Message = "No Device to Detect"
            Case C7000.SDK.SKF_STATUS_COM.HANDLE_ERR                                        ' Handle Error
                Message = "Handle Error"
            Case C7000.SDK.SKF_STATUS_COM.PARAM_ERR                                         ' Parameter Error
                Message = "Parameter Error"
            Case C7000.SDK.SKF_STATUS_COM.MEM_ALLOC_ERR                                     ' Memory Allocation Failed
                Message = "Memory Allocation Failed"
            Case C7000.SDK.SKF_STATUS_COM.CREATE_EVENT_ERR                                  ' Event Creation Failed
                Message = "Event Creation Failed"
            Case C7000.SDK.SKF_STATUS_COM.WAIT_TIMEOUT                                      ' Timeout
                Message = "Timeout"
            Case C7000.SDK.SKF_STATUS_COM.ACK_NAK_ERR                                       ' ACK/NAK Response Error
                Message = "ACK/NAK Response Error"
            Case C7000.SDK.SKF_STATUS_COM.NAK_COMMAND                                       ' NAK Error (Command Error) 
                Message = "NAK Error (Command Error)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_PARAM                                         ' NAK Error (Parameter Error)
                Message = "NAK Error (Parameter Error)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_UNSUPPORTED_MODEL                             ' NAK Error (Unsupported Model)
                Message = "NAK Error (Unsupported Model)"
            Case C7000.SDK.SKF_STATUS_COM.NAK_UNSUPPORTED_COMMAND                           ' NAK Error (Unsupported Command)
                Message = "NAK Error (Unsupported Command)"
            Case Else                                                                       ' else Error
                Message = "Status Code = " & Status.ToString("X8")
        End Select

        If Message <> "" Then                                                               ' Error Message Display
            IntervalTimer.Enabled = False

            MessageBox.Show(Message,
                            My.Application.Info.Title,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)

            Call C7000Device.SK_Disconnect()

            Group_Configuration.Enabled = False

            Button_Connect.Enabled = True
            Button_DisConnect.Enabled = False
            Button_MeasStart.Enabled = False
            Button_StandbyStop.Enabled = False

            Label_Status.Text = "-"                                                         ' Monitor Device Status
            Label_Ring.Text = "-"                                                           ' Monitor Light Selection Ring
        End If

        Check_Status = (C7000.SDK.SKF_STATUS_COM.SUCCESS = Status)
    End Function

    Private Function Get_DeveiceStats(ByVal DeviceInfor As C7000.SDK.SKS_DEVICE_INFO) As String
        Select Case DeviceInfor.Status
            Case C7000.SDK.SKF_STATUS_DEVICE.ERROR_HW                                       ' H/W Error
                Get_DeveiceStats = "H/W Error"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_MEASURING                                 ' Measuring
                Get_DeveiceStats = "Measuring"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_FLASH_STANDBY                             ' Flash standby
                Get_DeveiceStats = "Flash Standby"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_DARK_CALIBRATION                          ' Dark Calibration
                Get_DeveiceStats = "Dark Calibration"
            Case C7000.SDK.SKF_STATUS_DEVICE.BUSY_INITIALIZING                              ' Initilizing
                Get_DeveiceStats = "Initilize"
            Case C7000.SDK.SKF_STATUS_DEVICE.IDLE_OUT_MEAS                                  ' Out of Measurement Range
                Get_DeveiceStats = "Idle (Out of Measurement Range)"
            Case C7000.SDK.SKF_STATUS_DEVICE.IDLE                                           ' Idle
                Get_DeveiceStats = "Idle"
            Case Else                                                                       ' else Status
                Get_DeveiceStats = "Other"
        End Select
    End Function

    Private Function Get_RingStatus(ByVal DeviceInfor As C7000.SDK.SKS_DEVICE_INFO) As String
        Select Case DeviceInfor.Ring
            Case C7000.SDK.SKF_STATUS_RING.UNPOSITIONED                                     ' Unpositioned
                Get_RingStatus = "Light Selection Ring : Unpositioned"
            Case C7000.SDK.SKF_STATUS_RING.CAL                                              ' Cal
                Get_RingStatus = "Light Selection Ring : Cal"
            Case C7000.SDK.SKF_STATUS_RING.LOW                                              ' Low
                Get_RingStatus = "Light Selection Ring : Low"
            Case C7000.SDK.SKF_STATUS_RING.HIGH                                             ' High
                Get_RingStatus = "Light Selection Ring : High"
            Case Else                                                                       ' else 
                Get_RingStatus = "Other"
        End Select
    End Function
End Class
