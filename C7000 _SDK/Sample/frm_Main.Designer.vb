﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_Main
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint1 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(0R, 0.8R)
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint2 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(-2.0R, -2.0R)
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint3 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 2.0R)
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Main))
        Me.LayoutPanel_Form = New System.Windows.Forms.TableLayoutPanel()
        Me.Group_Configuration = New System.Windows.Forms.GroupBox()
        Me.LayoutPanel_Condition = New System.Windows.Forms.TableLayoutPanel()
        Me.Label_FieldViewe = New System.Windows.Forms.Label()
        Me.Label_MeasuringMode = New System.Windows.Forms.Label()
        Me.Label_ExposureTime = New System.Windows.Forms.Label()
        Me.Label_ShutterSpeed = New System.Windows.Forms.Label()
        Me.Combo_FieldView = New System.Windows.Forms.ComboBox()
        Me.Combo_MeasuringMode = New System.Windows.Forms.ComboBox()
        Me.Combo_ExposureTime = New System.Windows.Forms.ComboBox()
        Me.Combo_ShutterSpeed = New System.Windows.Forms.ComboBox()
        Me.Button_Connect = New System.Windows.Forms.Button()
        Me.Button_DisConnect = New System.Windows.Forms.Button()
        Me.Button_MeasStart = New System.Windows.Forms.Button()
        Me.Button_StandbyStop = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPageText = New System.Windows.Forms.TabPage()
        Me.LayoutPanel_Result = New System.Windows.Forms.TableLayoutPanel()
        Me.Result_TLMF = New System.Windows.Forms.Label()
        Me.Result_TLCI = New System.Windows.Forms.Label()
        Me.Result_SSI2 = New System.Windows.Forms.Label()
        Me.Result_SSI1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Result_SSId = New System.Windows.Forms.Label()
        Me.Result_SSIt = New System.Windows.Forms.Label()
        Me.Result_TM30Rg = New System.Windows.Forms.Label()
        Me.Result_TM30Rf = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label_Tcp = New System.Windows.Forms.Label()
        Me.Result_Tcp = New System.Windows.Forms.Label()
        Me.Label_Delta_uv = New System.Windows.Forms.Label()
        Me.Label_Illmi = New System.Windows.Forms.Label()
        Me.Label_PeakWaveLength = New System.Windows.Forms.Label()
        Me.Label_X = New System.Windows.Forms.Label()
        Me.Label_Y = New System.Windows.Forms.Label()
        Me.Label_CIE1931x = New System.Windows.Forms.Label()
        Me.Label_Z = New System.Windows.Forms.Label()
        Me.Label_CIE1931y = New System.Windows.Forms.Label()
        Me.Label_CIE1931z = New System.Windows.Forms.Label()
        Me.Label_CIE1976u = New System.Windows.Forms.Label()
        Me.Label_CIE1976v = New System.Windows.Forms.Label()
        Me.Label_DWL = New System.Windows.Forms.Label()
        Me.Label_Purity = New System.Windows.Forms.Label()
        Me.Label_PPFD = New System.Windows.Forms.Label()
        Me.Result_Delta_uv = New System.Windows.Forms.Label()
        Me.Result_Illmi = New System.Windows.Forms.Label()
        Me.Result_PeakWaveLength = New System.Windows.Forms.Label()
        Me.Result_X = New System.Windows.Forms.Label()
        Me.Result_Y = New System.Windows.Forms.Label()
        Me.Result_Z = New System.Windows.Forms.Label()
        Me.Result_CIE1931x = New System.Windows.Forms.Label()
        Me.Result_CIE1931y = New System.Windows.Forms.Label()
        Me.Result_CIE1931z = New System.Windows.Forms.Label()
        Me.Result_CIE1976u = New System.Windows.Forms.Label()
        Me.Result_CIE1976v = New System.Windows.Forms.Label()
        Me.Result_DWL = New System.Windows.Forms.Label()
        Me.Result_Purity = New System.Windows.Forms.Label()
        Me.Result_PPFD = New System.Windows.Forms.Label()
        Me.Result_CRI_Ra = New System.Windows.Forms.Label()
        Me.Result_CRI_R1 = New System.Windows.Forms.Label()
        Me.Result_CRI_R2 = New System.Windows.Forms.Label()
        Me.Result_CRI_R3 = New System.Windows.Forms.Label()
        Me.Result_CRI_R4 = New System.Windows.Forms.Label()
        Me.Result_CRI_R5 = New System.Windows.Forms.Label()
        Me.Result_CRI_R6 = New System.Windows.Forms.Label()
        Me.Result_CRI_R7 = New System.Windows.Forms.Label()
        Me.Result_CRI_R8 = New System.Windows.Forms.Label()
        Me.Result_CRI_R9 = New System.Windows.Forms.Label()
        Me.Result_CRI_R10 = New System.Windows.Forms.Label()
        Me.Result_CRI_R11 = New System.Windows.Forms.Label()
        Me.Result_CRI_R12 = New System.Windows.Forms.Label()
        Me.Result_CRI_R13 = New System.Windows.Forms.Label()
        Me.Result_CRI_R14 = New System.Windows.Forms.Label()
        Me.Result_CRI_R15 = New System.Windows.Forms.Label()
        Me.Label_CRI_Ra = New System.Windows.Forms.Label()
        Me.Label_CRI_R1 = New System.Windows.Forms.Label()
        Me.Label_CRI_R2 = New System.Windows.Forms.Label()
        Me.Label_CRI_R3 = New System.Windows.Forms.Label()
        Me.Label_CRI_R4 = New System.Windows.Forms.Label()
        Me.Label_CRI_R5 = New System.Windows.Forms.Label()
        Me.Label_CRI_R6 = New System.Windows.Forms.Label()
        Me.Label_CRI_R7 = New System.Windows.Forms.Label()
        Me.Label_CRI_R8 = New System.Windows.Forms.Label()
        Me.Label_CRI_R9 = New System.Windows.Forms.Label()
        Me.Label_CRI_R10 = New System.Windows.Forms.Label()
        Me.Label_CRI_R11 = New System.Windows.Forms.Label()
        Me.Label_CRI_R12 = New System.Windows.Forms.Label()
        Me.Label_CRI_R13 = New System.Windows.Forms.Label()
        Me.Label_CRI_R14 = New System.Windows.Forms.Label()
        Me.Label_CRI_R15 = New System.Windows.Forms.Label()
        Me.TabPageSpectrum = New System.Windows.Forms.TabPage()
        Me.ChartSpectrum = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabPageTM30 = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanelTM30 = New System.Windows.Forms.TableLayoutPanel()
        Me.ChartTM30 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label_Ring = New System.Windows.Forms.ToolStripStatusLabel()
        Me.IntervalTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Combo_MeasMethod = New System.Windows.Forms.ComboBox()
        Me.Label_MeasMethod = New System.Windows.Forms.Label()
        Me.LayoutPanel_Form.SuspendLayout()
        Me.Group_Configuration.SuspendLayout()
        Me.LayoutPanel_Condition.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPageText.SuspendLayout()
        Me.LayoutPanel_Result.SuspendLayout()
        Me.TabPageSpectrum.SuspendLayout()
        CType(Me.ChartSpectrum, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageTM30.SuspendLayout()
        Me.TableLayoutPanelTM30.SuspendLayout()
        CType(Me.ChartTM30, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LayoutPanel_Form
        '
        Me.LayoutPanel_Form.ColumnCount = 4
        Me.LayoutPanel_Form.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.LayoutPanel_Form.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.LayoutPanel_Form.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.LayoutPanel_Form.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.LayoutPanel_Form.Controls.Add(Me.Group_Configuration, 0, 0)
        Me.LayoutPanel_Form.Controls.Add(Me.Button_Connect, 2, 0)
        Me.LayoutPanel_Form.Controls.Add(Me.Button_DisConnect, 3, 0)
        Me.LayoutPanel_Form.Controls.Add(Me.Button_MeasStart, 2, 1)
        Me.LayoutPanel_Form.Controls.Add(Me.Button_StandbyStop, 3, 1)
        Me.LayoutPanel_Form.Controls.Add(Me.TabControl1, 0, 2)
        Me.LayoutPanel_Form.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutPanel_Form.Location = New System.Drawing.Point(0, 0)
        Me.LayoutPanel_Form.Name = "LayoutPanel_Form"
        Me.LayoutPanel_Form.RowCount = 3
        Me.LayoutPanel_Form.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.30404!))
        Me.LayoutPanel_Form.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.30404!))
        Me.LayoutPanel_Form.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 71.39193!))
        Me.LayoutPanel_Form.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.LayoutPanel_Form.Size = New System.Drawing.Size(754, 547)
        Me.LayoutPanel_Form.TabIndex = 0
        '
        'Group_Configuration
        '
        Me.LayoutPanel_Form.SetColumnSpan(Me.Group_Configuration, 2)
        Me.Group_Configuration.Controls.Add(Me.LayoutPanel_Condition)
        Me.Group_Configuration.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Group_Configuration.Enabled = False
        Me.Group_Configuration.Location = New System.Drawing.Point(10, 5)
        Me.Group_Configuration.Margin = New System.Windows.Forms.Padding(10, 5, 10, 0)
        Me.Group_Configuration.Name = "Group_Configuration"
        Me.LayoutPanel_Form.SetRowSpan(Me.Group_Configuration, 2)
        Me.Group_Configuration.Size = New System.Drawing.Size(356, 151)
        Me.Group_Configuration.TabIndex = 0
        Me.Group_Configuration.TabStop = False
        Me.Group_Configuration.Text = "Configuration"
        '
        'LayoutPanel_Condition
        '
        Me.LayoutPanel_Condition.ColumnCount = 2
        Me.LayoutPanel_Condition.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.99396!))
        Me.LayoutPanel_Condition.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.00604!))
        Me.LayoutPanel_Condition.Controls.Add(Me.Label_MeasMethod, 0, 4)
        Me.LayoutPanel_Condition.Controls.Add(Me.Combo_MeasMethod, 1, 4)
        Me.LayoutPanel_Condition.Controls.Add(Me.Label_FieldViewe, 0, 0)
        Me.LayoutPanel_Condition.Controls.Add(Me.Label_MeasuringMode, 0, 1)
        Me.LayoutPanel_Condition.Controls.Add(Me.Label_ExposureTime, 0, 2)
        Me.LayoutPanel_Condition.Controls.Add(Me.Label_ShutterSpeed, 0, 3)
        Me.LayoutPanel_Condition.Controls.Add(Me.Combo_FieldView, 1, 0)
        Me.LayoutPanel_Condition.Controls.Add(Me.Combo_MeasuringMode, 1, 1)
        Me.LayoutPanel_Condition.Controls.Add(Me.Combo_ExposureTime, 1, 2)
        Me.LayoutPanel_Condition.Controls.Add(Me.Combo_ShutterSpeed, 1, 3)
        Me.LayoutPanel_Condition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutPanel_Condition.Location = New System.Drawing.Point(3, 15)
        Me.LayoutPanel_Condition.Name = "LayoutPanel_Condition"
        Me.LayoutPanel_Condition.RowCount = 5
        Me.LayoutPanel_Condition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.LayoutPanel_Condition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.LayoutPanel_Condition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.LayoutPanel_Condition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.LayoutPanel_Condition.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.LayoutPanel_Condition.Size = New System.Drawing.Size(350, 133)
        Me.LayoutPanel_Condition.TabIndex = 0
        '
        'Label_FieldViewe
        '
        Me.Label_FieldViewe.AutoSize = True
        Me.Label_FieldViewe.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_FieldViewe.Location = New System.Drawing.Point(3, 0)
        Me.Label_FieldViewe.Name = "Label_FieldViewe"
        Me.Label_FieldViewe.Size = New System.Drawing.Size(140, 26)
        Me.Label_FieldViewe.TabIndex = 0
        Me.Label_FieldViewe.Text = "Field of Vew"
        Me.Label_FieldViewe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_MeasuringMode
        '
        Me.Label_MeasuringMode.AutoSize = True
        Me.Label_MeasuringMode.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_MeasuringMode.Location = New System.Drawing.Point(3, 26)
        Me.Label_MeasuringMode.Name = "Label_MeasuringMode"
        Me.Label_MeasuringMode.Size = New System.Drawing.Size(140, 26)
        Me.Label_MeasuringMode.TabIndex = 0
        Me.Label_MeasuringMode.Text = "Measuring Mode"
        Me.Label_MeasuringMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_ExposureTime
        '
        Me.Label_ExposureTime.AutoSize = True
        Me.Label_ExposureTime.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_ExposureTime.Location = New System.Drawing.Point(3, 52)
        Me.Label_ExposureTime.Name = "Label_ExposureTime"
        Me.Label_ExposureTime.Size = New System.Drawing.Size(140, 26)
        Me.Label_ExposureTime.TabIndex = 0
        Me.Label_ExposureTime.Text = "Exposure Time"
        Me.Label_ExposureTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_ShutterSpeed
        '
        Me.Label_ShutterSpeed.AutoSize = True
        Me.Label_ShutterSpeed.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_ShutterSpeed.Enabled = False
        Me.Label_ShutterSpeed.Location = New System.Drawing.Point(3, 78)
        Me.Label_ShutterSpeed.Name = "Label_ShutterSpeed"
        Me.Label_ShutterSpeed.Size = New System.Drawing.Size(140, 26)
        Me.Label_ShutterSpeed.TabIndex = 0
        Me.Label_ShutterSpeed.Text = "Shutter Speed"
        Me.Label_ShutterSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Combo_FieldView
        '
        Me.Combo_FieldView.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Combo_FieldView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_FieldView.FormattingEnabled = True
        Me.Combo_FieldView.Items.AddRange(New Object() {"2ﾟ", "10ﾟ"})
        Me.Combo_FieldView.Location = New System.Drawing.Point(149, 3)
        Me.Combo_FieldView.Name = "Combo_FieldView"
        Me.Combo_FieldView.Size = New System.Drawing.Size(198, 20)
        Me.Combo_FieldView.TabIndex = 1
        '
        'Combo_MeasuringMode
        '
        Me.Combo_MeasuringMode.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Combo_MeasuringMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_MeasuringMode.FormattingEnabled = True
        Me.Combo_MeasuringMode.Items.AddRange(New Object() {"Ambient", "Cordless Flash", "Cord (PC) Flash"})
        Me.Combo_MeasuringMode.Location = New System.Drawing.Point(149, 29)
        Me.Combo_MeasuringMode.Name = "Combo_MeasuringMode"
        Me.Combo_MeasuringMode.Size = New System.Drawing.Size(198, 20)
        Me.Combo_MeasuringMode.TabIndex = 2
        '
        'Combo_ExposureTime
        '
        Me.Combo_ExposureTime.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Combo_ExposureTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_ExposureTime.FormattingEnabled = True
        Me.Combo_ExposureTime.Items.AddRange(New Object() {"Auto", "0.1 sec", "1.0 sec"})
        Me.Combo_ExposureTime.Location = New System.Drawing.Point(149, 55)
        Me.Combo_ExposureTime.Name = "Combo_ExposureTime"
        Me.Combo_ExposureTime.Size = New System.Drawing.Size(198, 20)
        Me.Combo_ExposureTime.TabIndex = 2
        '
        'Combo_ShutterSpeed
        '
        Me.Combo_ShutterSpeed.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Combo_ShutterSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_ShutterSpeed.Enabled = False
        Me.Combo_ShutterSpeed.FormattingEnabled = True
        Me.Combo_ShutterSpeed.Items.AddRange(New Object() {"1 sec", "1/2 sec", "1/4 sec", "1/8 sec", "1/15 sec", "1/30 sec", "1/60 sec", "1/125 sec", "1/250 sec", "1/500 sec"})
        Me.Combo_ShutterSpeed.Location = New System.Drawing.Point(149, 81)
        Me.Combo_ShutterSpeed.Name = "Combo_ShutterSpeed"
        Me.Combo_ShutterSpeed.Size = New System.Drawing.Size(198, 20)
        Me.Combo_ShutterSpeed.TabIndex = 2
        '
        'Button_Connect
        '
        Me.Button_Connect.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Connect.Location = New System.Drawing.Point(379, 3)
        Me.Button_Connect.Name = "Button_Connect"
        Me.Button_Connect.Size = New System.Drawing.Size(182, 72)
        Me.Button_Connect.TabIndex = 3
        Me.Button_Connect.Text = "Connect"
        Me.Button_Connect.UseVisualStyleBackColor = True
        '
        'Button_DisConnect
        '
        Me.Button_DisConnect.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_DisConnect.Enabled = False
        Me.Button_DisConnect.Location = New System.Drawing.Point(567, 3)
        Me.Button_DisConnect.Name = "Button_DisConnect"
        Me.Button_DisConnect.Size = New System.Drawing.Size(184, 72)
        Me.Button_DisConnect.TabIndex = 4
        Me.Button_DisConnect.Text = "Disconnect"
        Me.Button_DisConnect.UseVisualStyleBackColor = True
        '
        'Button_MeasStart
        '
        Me.Button_MeasStart.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_MeasStart.Enabled = False
        Me.Button_MeasStart.Location = New System.Drawing.Point(379, 81)
        Me.Button_MeasStart.Name = "Button_MeasStart"
        Me.Button_MeasStart.Size = New System.Drawing.Size(182, 72)
        Me.Button_MeasStart.TabIndex = 5
        Me.Button_MeasStart.Text = "Measurement Start"
        Me.Button_MeasStart.UseVisualStyleBackColor = True
        '
        'Button_StandbyStop
        '
        Me.Button_StandbyStop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_StandbyStop.Enabled = False
        Me.Button_StandbyStop.Location = New System.Drawing.Point(567, 81)
        Me.Button_StandbyStop.Name = "Button_StandbyStop"
        Me.Button_StandbyStop.Size = New System.Drawing.Size(184, 72)
        Me.Button_StandbyStop.TabIndex = 6
        Me.Button_StandbyStop.Text = "Flash Standby Stop"
        Me.Button_StandbyStop.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.LayoutPanel_Form.SetColumnSpan(Me.TabControl1, 4)
        Me.TabControl1.Controls.Add(Me.TabPageText)
        Me.TabControl1.Controls.Add(Me.TabPageSpectrum)
        Me.TabControl1.Controls.Add(Me.TabPageTM30)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 159)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(748, 385)
        Me.TabControl1.TabIndex = 7
        '
        'TabPageText
        '
        Me.TabPageText.Controls.Add(Me.LayoutPanel_Result)
        Me.TabPageText.Location = New System.Drawing.Point(4, 22)
        Me.TabPageText.Name = "TabPageText"
        Me.TabPageText.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageText.Size = New System.Drawing.Size(740, 359)
        Me.TabPageText.TabIndex = 0
        Me.TabPageText.Text = "Text"
        Me.TabPageText.UseVisualStyleBackColor = True
        '
        'LayoutPanel_Result
        '
        Me.LayoutPanel_Result.ColumnCount = 6
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.LayoutPanel_Result.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.0!))
        Me.LayoutPanel_Result.Controls.Add(Me.Result_TLMF, 5, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_TLCI, 5, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_SSI2, 5, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_SSI1, 5, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Label8, 4, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Label7, 4, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_SSId, 5, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_SSIt, 5, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_TM30Rg, 5, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_TM30Rf, 5, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Label6, 4, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Label5, 4, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Label4, 4, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Label3, 4, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Label2, 4, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Label1, 4, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Tcp, 0, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Tcp, 1, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Delta_uv, 0, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Illmi, 0, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_PeakWaveLength, 0, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_X, 0, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Y, 0, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CIE1931x, 0, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Z, 0, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CIE1931y, 0, 8)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CIE1931z, 0, 9)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CIE1976u, 0, 10)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CIE1976v, 0, 11)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_DWL, 0, 12)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_Purity, 0, 13)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_PPFD, 0, 14)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Delta_uv, 1, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Illmi, 1, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_PeakWaveLength, 1, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_X, 1, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Y, 1, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Z, 1, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CIE1931x, 1, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CIE1931y, 1, 8)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CIE1931z, 1, 9)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CIE1976u, 1, 10)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CIE1976v, 1, 11)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_DWL, 1, 12)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_Purity, 1, 13)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_PPFD, 1, 14)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_Ra, 3, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R1, 3, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R2, 3, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R3, 3, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R4, 3, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R5, 3, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R6, 3, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R7, 3, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R8, 3, 8)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R9, 3, 9)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R10, 3, 10)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R11, 3, 11)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R12, 3, 12)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R13, 3, 13)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R14, 3, 14)
        Me.LayoutPanel_Result.Controls.Add(Me.Result_CRI_R15, 3, 15)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_Ra, 2, 0)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R1, 2, 1)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R2, 2, 2)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R3, 2, 3)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R4, 2, 4)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R5, 2, 5)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R6, 2, 6)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R7, 2, 7)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R8, 2, 8)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R9, 2, 9)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R10, 2, 10)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R11, 2, 11)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R12, 2, 12)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R13, 2, 13)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R14, 2, 14)
        Me.LayoutPanel_Result.Controls.Add(Me.Label_CRI_R15, 2, 15)
        Me.LayoutPanel_Result.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutPanel_Result.Location = New System.Drawing.Point(3, 3)
        Me.LayoutPanel_Result.Name = "LayoutPanel_Result"
        Me.LayoutPanel_Result.RowCount = 16
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.25!))
        Me.LayoutPanel_Result.Size = New System.Drawing.Size(734, 353)
        Me.LayoutPanel_Result.TabIndex = 2
        '
        'Result_TLMF
        '
        Me.Result_TLMF.AutoSize = True
        Me.Result_TLMF.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_TLMF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_TLMF.Location = New System.Drawing.Point(633, 157)
        Me.Result_TLMF.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_TLMF.Name = "Result_TLMF"
        Me.Result_TLMF.Size = New System.Drawing.Size(91, 16)
        Me.Result_TLMF.TabIndex = 18
        Me.Result_TLMF.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_TLCI
        '
        Me.Result_TLCI.AutoSize = True
        Me.Result_TLCI.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_TLCI.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_TLCI.Location = New System.Drawing.Point(633, 135)
        Me.Result_TLCI.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_TLCI.Name = "Result_TLCI"
        Me.Result_TLCI.Size = New System.Drawing.Size(91, 16)
        Me.Result_TLCI.TabIndex = 17
        Me.Result_TLCI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_SSI2
        '
        Me.Result_SSI2.AutoSize = True
        Me.Result_SSI2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_SSI2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_SSI2.Location = New System.Drawing.Point(633, 113)
        Me.Result_SSI2.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_SSI2.Name = "Result_SSI2"
        Me.Result_SSI2.Size = New System.Drawing.Size(91, 16)
        Me.Result_SSI2.TabIndex = 16
        Me.Result_SSI2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_SSI1
        '
        Me.Result_SSI1.AutoSize = True
        Me.Result_SSI1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_SSI1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_SSI1.Location = New System.Drawing.Point(633, 91)
        Me.Result_SSI1.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_SSI1.Name = "Result_SSI1"
        Me.Result_SSI1.Size = New System.Drawing.Size(91, 16)
        Me.Result_SSI1.TabIndex = 15
        Me.Result_SSI1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Location = New System.Drawing.Point(516, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 22)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "TLMF"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Location = New System.Drawing.Point(516, 132)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 22)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "TLCI"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_SSId
        '
        Me.Result_SSId.AutoSize = True
        Me.Result_SSId.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_SSId.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_SSId.Location = New System.Drawing.Point(633, 69)
        Me.Result_SSId.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_SSId.Name = "Result_SSId"
        Me.Result_SSId.Size = New System.Drawing.Size(91, 16)
        Me.Result_SSId.TabIndex = 12
        Me.Result_SSId.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_SSIt
        '
        Me.Result_SSIt.AutoSize = True
        Me.Result_SSIt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_SSIt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_SSIt.Location = New System.Drawing.Point(633, 47)
        Me.Result_SSIt.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_SSIt.Name = "Result_SSIt"
        Me.Result_SSIt.Size = New System.Drawing.Size(91, 16)
        Me.Result_SSIt.TabIndex = 11
        Me.Result_SSIt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_TM30Rg
        '
        Me.Result_TM30Rg.AutoSize = True
        Me.Result_TM30Rg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_TM30Rg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_TM30Rg.Location = New System.Drawing.Point(633, 25)
        Me.Result_TM30Rg.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_TM30Rg.Name = "Result_TM30Rg"
        Me.Result_TM30Rg.Size = New System.Drawing.Size(91, 16)
        Me.Result_TM30Rg.TabIndex = 10
        Me.Result_TM30Rg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_TM30Rf
        '
        Me.Result_TM30Rf.AutoSize = True
        Me.Result_TM30Rf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_TM30Rf.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_TM30Rf.Location = New System.Drawing.Point(633, 3)
        Me.Result_TM30Rf.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_TM30Rf.Name = "Result_TM30Rf"
        Me.Result_TM30Rf.Size = New System.Drawing.Size(91, 16)
        Me.Result_TM30Rf.TabIndex = 9
        Me.Result_TM30Rf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Location = New System.Drawing.Point(516, 110)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 22)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "SSI2"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Location = New System.Drawing.Point(516, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 22)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "SSI1"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Location = New System.Drawing.Point(516, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 22)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "SSI Daylight"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Location = New System.Drawing.Point(516, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 22)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "SSI Tungsten"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Location = New System.Drawing.Point(516, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 22)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Rg"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Location = New System.Drawing.Point(516, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 22)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "TM-30-18 Rf"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Tcp
        '
        Me.Label_Tcp.AutoSize = True
        Me.Label_Tcp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Tcp.Location = New System.Drawing.Point(3, 0)
        Me.Label_Tcp.Name = "Label_Tcp"
        Me.Label_Tcp.Size = New System.Drawing.Size(177, 22)
        Me.Label_Tcp.TabIndex = 0
        Me.Label_Tcp.Text = "Tcp [K]"
        Me.Label_Tcp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Tcp
        '
        Me.Result_Tcp.AutoSize = True
        Me.Result_Tcp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Tcp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Tcp.Location = New System.Drawing.Point(193, 3)
        Me.Result_Tcp.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Tcp.Name = "Result_Tcp"
        Me.Result_Tcp.Size = New System.Drawing.Size(90, 16)
        Me.Result_Tcp.TabIndex = 0
        Me.Result_Tcp.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Delta_uv
        '
        Me.Label_Delta_uv.AutoSize = True
        Me.Label_Delta_uv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Delta_uv.Location = New System.Drawing.Point(3, 22)
        Me.Label_Delta_uv.Name = "Label_Delta_uv"
        Me.Label_Delta_uv.Size = New System.Drawing.Size(177, 22)
        Me.Label_Delta_uv.TabIndex = 0
        Me.Label_Delta_uv.Text = "⊿uv"
        Me.Label_Delta_uv.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Illmi
        '
        Me.Label_Illmi.AutoSize = True
        Me.Label_Illmi.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Illmi.Location = New System.Drawing.Point(3, 44)
        Me.Label_Illmi.Name = "Label_Illmi"
        Me.Label_Illmi.Size = New System.Drawing.Size(177, 22)
        Me.Label_Illmi.TabIndex = 0
        Me.Label_Illmi.Text = "Illuminance [lx] /" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Luminous Exposure [lx･s]"
        Me.Label_Illmi.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_PeakWaveLength
        '
        Me.Label_PeakWaveLength.AutoSize = True
        Me.Label_PeakWaveLength.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_PeakWaveLength.Location = New System.Drawing.Point(3, 66)
        Me.Label_PeakWaveLength.Name = "Label_PeakWaveLength"
        Me.Label_PeakWaveLength.Size = New System.Drawing.Size(177, 22)
        Me.Label_PeakWaveLength.TabIndex = 0
        Me.Label_PeakWaveLength.Text = "Peak Wave Length [nm]"
        Me.Label_PeakWaveLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_X
        '
        Me.Label_X.AutoSize = True
        Me.Label_X.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_X.Location = New System.Drawing.Point(3, 88)
        Me.Label_X.Name = "Label_X"
        Me.Label_X.Size = New System.Drawing.Size(177, 22)
        Me.Label_X.TabIndex = 0
        Me.Label_X.Text = "Tristimulus Value X (X₁₀)"
        Me.Label_X.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Y
        '
        Me.Label_Y.AutoSize = True
        Me.Label_Y.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Y.Location = New System.Drawing.Point(3, 110)
        Me.Label_Y.Name = "Label_Y"
        Me.Label_Y.Size = New System.Drawing.Size(177, 22)
        Me.Label_Y.TabIndex = 0
        Me.Label_Y.Text = "Tristimulus Value Y (Y₁₀)"
        Me.Label_Y.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CIE1931x
        '
        Me.Label_CIE1931x.AutoSize = True
        Me.Label_CIE1931x.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CIE1931x.Location = New System.Drawing.Point(3, 154)
        Me.Label_CIE1931x.Name = "Label_CIE1931x"
        Me.Label_CIE1931x.Size = New System.Drawing.Size(177, 22)
        Me.Label_CIE1931x.TabIndex = 1
        Me.Label_CIE1931x.Text = "CIE1931 x (CIE1964 x₁₀)"
        Me.Label_CIE1931x.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Z
        '
        Me.Label_Z.AutoSize = True
        Me.Label_Z.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Z.Location = New System.Drawing.Point(3, 132)
        Me.Label_Z.Name = "Label_Z"
        Me.Label_Z.Size = New System.Drawing.Size(177, 22)
        Me.Label_Z.TabIndex = 1
        Me.Label_Z.Text = "Tristimulus Value Z (Z₁₀)"
        Me.Label_Z.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CIE1931y
        '
        Me.Label_CIE1931y.AutoSize = True
        Me.Label_CIE1931y.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CIE1931y.Location = New System.Drawing.Point(3, 176)
        Me.Label_CIE1931y.Name = "Label_CIE1931y"
        Me.Label_CIE1931y.Size = New System.Drawing.Size(177, 22)
        Me.Label_CIE1931y.TabIndex = 1
        Me.Label_CIE1931y.Text = "CIE1931 y (CIE1964 y₁₀)"
        Me.Label_CIE1931y.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CIE1931z
        '
        Me.Label_CIE1931z.AutoSize = True
        Me.Label_CIE1931z.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CIE1931z.Location = New System.Drawing.Point(3, 198)
        Me.Label_CIE1931z.Name = "Label_CIE1931z"
        Me.Label_CIE1931z.Size = New System.Drawing.Size(177, 22)
        Me.Label_CIE1931z.TabIndex = 1
        Me.Label_CIE1931z.Text = "CIE1931 z (CIE1964 z₁₀)"
        Me.Label_CIE1931z.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CIE1976u
        '
        Me.Label_CIE1976u.AutoSize = True
        Me.Label_CIE1976u.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CIE1976u.Location = New System.Drawing.Point(3, 220)
        Me.Label_CIE1976u.Name = "Label_CIE1976u"
        Me.Label_CIE1976u.Size = New System.Drawing.Size(177, 22)
        Me.Label_CIE1976u.TabIndex = 1
        Me.Label_CIE1976u.Text = "CIE1976 u' (u'₁₀)"
        Me.Label_CIE1976u.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CIE1976v
        '
        Me.Label_CIE1976v.AutoSize = True
        Me.Label_CIE1976v.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CIE1976v.Location = New System.Drawing.Point(3, 242)
        Me.Label_CIE1976v.Name = "Label_CIE1976v"
        Me.Label_CIE1976v.Size = New System.Drawing.Size(177, 22)
        Me.Label_CIE1976v.TabIndex = 1
        Me.Label_CIE1976v.Text = "CIE1976 v' (v'₁₀)"
        Me.Label_CIE1976v.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_DWL
        '
        Me.Label_DWL.AutoSize = True
        Me.Label_DWL.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_DWL.Location = New System.Drawing.Point(3, 264)
        Me.Label_DWL.Name = "Label_DWL"
        Me.Label_DWL.Size = New System.Drawing.Size(177, 22)
        Me.Label_DWL.TabIndex = 1
        Me.Label_DWL.Text = "Dominant Wavelength [nm]"
        Me.Label_DWL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_Purity
        '
        Me.Label_Purity.AutoSize = True
        Me.Label_Purity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_Purity.Location = New System.Drawing.Point(3, 286)
        Me.Label_Purity.Name = "Label_Purity"
        Me.Label_Purity.Size = New System.Drawing.Size(177, 22)
        Me.Label_Purity.TabIndex = 1
        Me.Label_Purity.Text = "Excitation Purity [%]"
        Me.Label_Purity.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_PPFD
        '
        Me.Label_PPFD.AutoSize = True
        Me.Label_PPFD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_PPFD.Location = New System.Drawing.Point(3, 308)
        Me.Label_PPFD.Name = "Label_PPFD"
        Me.Label_PPFD.Size = New System.Drawing.Size(177, 22)
        Me.Label_PPFD.TabIndex = 1
        Me.Label_PPFD.Text = "PPFD [umol･m⁻²･s⁻¹]"
        Me.Label_PPFD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Delta_uv
        '
        Me.Result_Delta_uv.AutoSize = True
        Me.Result_Delta_uv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Delta_uv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Delta_uv.Location = New System.Drawing.Point(193, 25)
        Me.Result_Delta_uv.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Delta_uv.Name = "Result_Delta_uv"
        Me.Result_Delta_uv.Size = New System.Drawing.Size(90, 16)
        Me.Result_Delta_uv.TabIndex = 0
        Me.Result_Delta_uv.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Illmi
        '
        Me.Result_Illmi.AutoSize = True
        Me.Result_Illmi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Illmi.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Illmi.Location = New System.Drawing.Point(193, 47)
        Me.Result_Illmi.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Illmi.Name = "Result_Illmi"
        Me.Result_Illmi.Size = New System.Drawing.Size(90, 16)
        Me.Result_Illmi.TabIndex = 0
        Me.Result_Illmi.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_PeakWaveLength
        '
        Me.Result_PeakWaveLength.AutoSize = True
        Me.Result_PeakWaveLength.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_PeakWaveLength.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_PeakWaveLength.Location = New System.Drawing.Point(193, 69)
        Me.Result_PeakWaveLength.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_PeakWaveLength.Name = "Result_PeakWaveLength"
        Me.Result_PeakWaveLength.Size = New System.Drawing.Size(90, 16)
        Me.Result_PeakWaveLength.TabIndex = 0
        Me.Result_PeakWaveLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_X
        '
        Me.Result_X.AutoSize = True
        Me.Result_X.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_X.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_X.Location = New System.Drawing.Point(193, 91)
        Me.Result_X.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_X.Name = "Result_X"
        Me.Result_X.Size = New System.Drawing.Size(90, 16)
        Me.Result_X.TabIndex = 0
        Me.Result_X.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Y
        '
        Me.Result_Y.AutoSize = True
        Me.Result_Y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Y.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Y.Location = New System.Drawing.Point(193, 113)
        Me.Result_Y.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Y.Name = "Result_Y"
        Me.Result_Y.Size = New System.Drawing.Size(90, 16)
        Me.Result_Y.TabIndex = 0
        Me.Result_Y.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Z
        '
        Me.Result_Z.AutoSize = True
        Me.Result_Z.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Z.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Z.Location = New System.Drawing.Point(193, 135)
        Me.Result_Z.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Z.Name = "Result_Z"
        Me.Result_Z.Size = New System.Drawing.Size(90, 16)
        Me.Result_Z.TabIndex = 0
        Me.Result_Z.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CIE1931x
        '
        Me.Result_CIE1931x.AutoSize = True
        Me.Result_CIE1931x.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CIE1931x.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CIE1931x.Location = New System.Drawing.Point(193, 157)
        Me.Result_CIE1931x.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CIE1931x.Name = "Result_CIE1931x"
        Me.Result_CIE1931x.Size = New System.Drawing.Size(90, 16)
        Me.Result_CIE1931x.TabIndex = 0
        Me.Result_CIE1931x.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CIE1931y
        '
        Me.Result_CIE1931y.AutoSize = True
        Me.Result_CIE1931y.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CIE1931y.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CIE1931y.Location = New System.Drawing.Point(193, 179)
        Me.Result_CIE1931y.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CIE1931y.Name = "Result_CIE1931y"
        Me.Result_CIE1931y.Size = New System.Drawing.Size(90, 16)
        Me.Result_CIE1931y.TabIndex = 0
        Me.Result_CIE1931y.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CIE1931z
        '
        Me.Result_CIE1931z.AutoSize = True
        Me.Result_CIE1931z.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CIE1931z.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CIE1931z.Location = New System.Drawing.Point(193, 201)
        Me.Result_CIE1931z.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CIE1931z.Name = "Result_CIE1931z"
        Me.Result_CIE1931z.Size = New System.Drawing.Size(90, 16)
        Me.Result_CIE1931z.TabIndex = 0
        Me.Result_CIE1931z.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CIE1976u
        '
        Me.Result_CIE1976u.AutoSize = True
        Me.Result_CIE1976u.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CIE1976u.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CIE1976u.Location = New System.Drawing.Point(193, 223)
        Me.Result_CIE1976u.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CIE1976u.Name = "Result_CIE1976u"
        Me.Result_CIE1976u.Size = New System.Drawing.Size(90, 16)
        Me.Result_CIE1976u.TabIndex = 0
        Me.Result_CIE1976u.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CIE1976v
        '
        Me.Result_CIE1976v.AutoSize = True
        Me.Result_CIE1976v.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CIE1976v.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CIE1976v.Location = New System.Drawing.Point(193, 245)
        Me.Result_CIE1976v.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CIE1976v.Name = "Result_CIE1976v"
        Me.Result_CIE1976v.Size = New System.Drawing.Size(90, 16)
        Me.Result_CIE1976v.TabIndex = 0
        Me.Result_CIE1976v.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_DWL
        '
        Me.Result_DWL.AutoSize = True
        Me.Result_DWL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_DWL.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_DWL.Location = New System.Drawing.Point(193, 267)
        Me.Result_DWL.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_DWL.Name = "Result_DWL"
        Me.Result_DWL.Size = New System.Drawing.Size(90, 16)
        Me.Result_DWL.TabIndex = 0
        Me.Result_DWL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_Purity
        '
        Me.Result_Purity.AutoSize = True
        Me.Result_Purity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_Purity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_Purity.Location = New System.Drawing.Point(193, 289)
        Me.Result_Purity.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_Purity.Name = "Result_Purity"
        Me.Result_Purity.Size = New System.Drawing.Size(90, 16)
        Me.Result_Purity.TabIndex = 0
        Me.Result_Purity.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_PPFD
        '
        Me.Result_PPFD.AutoSize = True
        Me.Result_PPFD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_PPFD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_PPFD.Location = New System.Drawing.Point(193, 311)
        Me.Result_PPFD.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_PPFD.Name = "Result_PPFD"
        Me.Result_PPFD.Size = New System.Drawing.Size(90, 16)
        Me.Result_PPFD.TabIndex = 0
        Me.Result_PPFD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_Ra
        '
        Me.Result_CRI_Ra.AutoSize = True
        Me.Result_CRI_Ra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_Ra.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_Ra.Location = New System.Drawing.Point(413, 3)
        Me.Result_CRI_Ra.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_Ra.Name = "Result_CRI_Ra"
        Me.Result_CRI_Ra.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_Ra.TabIndex = 0
        Me.Result_CRI_Ra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R1
        '
        Me.Result_CRI_R1.AutoSize = True
        Me.Result_CRI_R1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R1.Location = New System.Drawing.Point(413, 25)
        Me.Result_CRI_R1.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R1.Name = "Result_CRI_R1"
        Me.Result_CRI_R1.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R1.TabIndex = 0
        Me.Result_CRI_R1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R2
        '
        Me.Result_CRI_R2.AutoSize = True
        Me.Result_CRI_R2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R2.Location = New System.Drawing.Point(413, 47)
        Me.Result_CRI_R2.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R2.Name = "Result_CRI_R2"
        Me.Result_CRI_R2.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R2.TabIndex = 0
        Me.Result_CRI_R2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R3
        '
        Me.Result_CRI_R3.AutoSize = True
        Me.Result_CRI_R3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R3.Location = New System.Drawing.Point(413, 69)
        Me.Result_CRI_R3.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R3.Name = "Result_CRI_R3"
        Me.Result_CRI_R3.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R3.TabIndex = 0
        Me.Result_CRI_R3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R4
        '
        Me.Result_CRI_R4.AutoSize = True
        Me.Result_CRI_R4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R4.Location = New System.Drawing.Point(413, 91)
        Me.Result_CRI_R4.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R4.Name = "Result_CRI_R4"
        Me.Result_CRI_R4.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R4.TabIndex = 0
        Me.Result_CRI_R4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R5
        '
        Me.Result_CRI_R5.AutoSize = True
        Me.Result_CRI_R5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R5.Location = New System.Drawing.Point(413, 113)
        Me.Result_CRI_R5.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R5.Name = "Result_CRI_R5"
        Me.Result_CRI_R5.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R5.TabIndex = 0
        Me.Result_CRI_R5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R6
        '
        Me.Result_CRI_R6.AutoSize = True
        Me.Result_CRI_R6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R6.Location = New System.Drawing.Point(413, 135)
        Me.Result_CRI_R6.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R6.Name = "Result_CRI_R6"
        Me.Result_CRI_R6.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R6.TabIndex = 0
        Me.Result_CRI_R6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R7
        '
        Me.Result_CRI_R7.AutoSize = True
        Me.Result_CRI_R7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R7.Location = New System.Drawing.Point(413, 157)
        Me.Result_CRI_R7.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R7.Name = "Result_CRI_R7"
        Me.Result_CRI_R7.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R7.TabIndex = 0
        Me.Result_CRI_R7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R8
        '
        Me.Result_CRI_R8.AutoSize = True
        Me.Result_CRI_R8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R8.Location = New System.Drawing.Point(413, 179)
        Me.Result_CRI_R8.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R8.Name = "Result_CRI_R8"
        Me.Result_CRI_R8.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R8.TabIndex = 0
        Me.Result_CRI_R8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R9
        '
        Me.Result_CRI_R9.AutoSize = True
        Me.Result_CRI_R9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R9.Location = New System.Drawing.Point(413, 201)
        Me.Result_CRI_R9.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R9.Name = "Result_CRI_R9"
        Me.Result_CRI_R9.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R9.TabIndex = 0
        Me.Result_CRI_R9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R10
        '
        Me.Result_CRI_R10.AutoSize = True
        Me.Result_CRI_R10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R10.Location = New System.Drawing.Point(413, 223)
        Me.Result_CRI_R10.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R10.Name = "Result_CRI_R10"
        Me.Result_CRI_R10.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R10.TabIndex = 0
        Me.Result_CRI_R10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R11
        '
        Me.Result_CRI_R11.AutoSize = True
        Me.Result_CRI_R11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R11.Location = New System.Drawing.Point(413, 245)
        Me.Result_CRI_R11.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R11.Name = "Result_CRI_R11"
        Me.Result_CRI_R11.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R11.TabIndex = 0
        Me.Result_CRI_R11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R12
        '
        Me.Result_CRI_R12.AutoSize = True
        Me.Result_CRI_R12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R12.Location = New System.Drawing.Point(413, 267)
        Me.Result_CRI_R12.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R12.Name = "Result_CRI_R12"
        Me.Result_CRI_R12.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R12.TabIndex = 0
        Me.Result_CRI_R12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R13
        '
        Me.Result_CRI_R13.AutoSize = True
        Me.Result_CRI_R13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R13.Location = New System.Drawing.Point(413, 289)
        Me.Result_CRI_R13.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R13.Name = "Result_CRI_R13"
        Me.Result_CRI_R13.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R13.TabIndex = 0
        Me.Result_CRI_R13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R14
        '
        Me.Result_CRI_R14.AutoSize = True
        Me.Result_CRI_R14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R14.Location = New System.Drawing.Point(413, 311)
        Me.Result_CRI_R14.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R14.Name = "Result_CRI_R14"
        Me.Result_CRI_R14.Size = New System.Drawing.Size(90, 16)
        Me.Result_CRI_R14.TabIndex = 0
        Me.Result_CRI_R14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Result_CRI_R15
        '
        Me.Result_CRI_R15.AutoSize = True
        Me.Result_CRI_R15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Result_CRI_R15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Result_CRI_R15.Location = New System.Drawing.Point(413, 333)
        Me.Result_CRI_R15.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Result_CRI_R15.Name = "Result_CRI_R15"
        Me.Result_CRI_R15.Size = New System.Drawing.Size(90, 17)
        Me.Result_CRI_R15.TabIndex = 0
        Me.Result_CRI_R15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_Ra
        '
        Me.Label_CRI_Ra.AutoSize = True
        Me.Label_CRI_Ra.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_Ra.Location = New System.Drawing.Point(296, 0)
        Me.Label_CRI_Ra.Name = "Label_CRI_Ra"
        Me.Label_CRI_Ra.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_Ra.TabIndex = 2
        Me.Label_CRI_Ra.Text = "Average CRI Ra"
        Me.Label_CRI_Ra.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R1
        '
        Me.Label_CRI_R1.AutoSize = True
        Me.Label_CRI_R1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R1.Location = New System.Drawing.Point(296, 22)
        Me.Label_CRI_R1.Name = "Label_CRI_R1"
        Me.Label_CRI_R1.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R1.TabIndex = 2
        Me.Label_CRI_R1.Text = "CRI R1"
        Me.Label_CRI_R1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R2
        '
        Me.Label_CRI_R2.AutoSize = True
        Me.Label_CRI_R2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R2.Location = New System.Drawing.Point(296, 44)
        Me.Label_CRI_R2.Name = "Label_CRI_R2"
        Me.Label_CRI_R2.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R2.TabIndex = 2
        Me.Label_CRI_R2.Text = "CRI R2"
        Me.Label_CRI_R2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R3
        '
        Me.Label_CRI_R3.AutoSize = True
        Me.Label_CRI_R3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R3.Location = New System.Drawing.Point(296, 66)
        Me.Label_CRI_R3.Name = "Label_CRI_R3"
        Me.Label_CRI_R3.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R3.TabIndex = 2
        Me.Label_CRI_R3.Text = "CRI R3"
        Me.Label_CRI_R3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R4
        '
        Me.Label_CRI_R4.AutoSize = True
        Me.Label_CRI_R4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R4.Location = New System.Drawing.Point(296, 88)
        Me.Label_CRI_R4.Name = "Label_CRI_R4"
        Me.Label_CRI_R4.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R4.TabIndex = 2
        Me.Label_CRI_R4.Text = "CRI R4"
        Me.Label_CRI_R4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R5
        '
        Me.Label_CRI_R5.AutoSize = True
        Me.Label_CRI_R5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R5.Location = New System.Drawing.Point(296, 110)
        Me.Label_CRI_R5.Name = "Label_CRI_R5"
        Me.Label_CRI_R5.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R5.TabIndex = 2
        Me.Label_CRI_R5.Text = "CRI R5"
        Me.Label_CRI_R5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R6
        '
        Me.Label_CRI_R6.AutoSize = True
        Me.Label_CRI_R6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R6.Location = New System.Drawing.Point(296, 132)
        Me.Label_CRI_R6.Name = "Label_CRI_R6"
        Me.Label_CRI_R6.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R6.TabIndex = 2
        Me.Label_CRI_R6.Text = "CRI R6"
        Me.Label_CRI_R6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R7
        '
        Me.Label_CRI_R7.AutoSize = True
        Me.Label_CRI_R7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R7.Location = New System.Drawing.Point(296, 154)
        Me.Label_CRI_R7.Name = "Label_CRI_R7"
        Me.Label_CRI_R7.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R7.TabIndex = 2
        Me.Label_CRI_R7.Text = "CRI R7"
        Me.Label_CRI_R7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R8
        '
        Me.Label_CRI_R8.AutoSize = True
        Me.Label_CRI_R8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R8.Location = New System.Drawing.Point(296, 176)
        Me.Label_CRI_R8.Name = "Label_CRI_R8"
        Me.Label_CRI_R8.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R8.TabIndex = 2
        Me.Label_CRI_R8.Text = "CRI R8"
        Me.Label_CRI_R8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R9
        '
        Me.Label_CRI_R9.AutoSize = True
        Me.Label_CRI_R9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R9.Location = New System.Drawing.Point(296, 198)
        Me.Label_CRI_R9.Name = "Label_CRI_R9"
        Me.Label_CRI_R9.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R9.TabIndex = 2
        Me.Label_CRI_R9.Text = "CRI R9"
        Me.Label_CRI_R9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R10
        '
        Me.Label_CRI_R10.AutoSize = True
        Me.Label_CRI_R10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R10.Location = New System.Drawing.Point(296, 220)
        Me.Label_CRI_R10.Name = "Label_CRI_R10"
        Me.Label_CRI_R10.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R10.TabIndex = 2
        Me.Label_CRI_R10.Text = "CRI R10"
        Me.Label_CRI_R10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R11
        '
        Me.Label_CRI_R11.AutoSize = True
        Me.Label_CRI_R11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R11.Location = New System.Drawing.Point(296, 242)
        Me.Label_CRI_R11.Name = "Label_CRI_R11"
        Me.Label_CRI_R11.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R11.TabIndex = 2
        Me.Label_CRI_R11.Text = "CRI R11"
        Me.Label_CRI_R11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R12
        '
        Me.Label_CRI_R12.AutoSize = True
        Me.Label_CRI_R12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R12.Location = New System.Drawing.Point(296, 264)
        Me.Label_CRI_R12.Name = "Label_CRI_R12"
        Me.Label_CRI_R12.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R12.TabIndex = 2
        Me.Label_CRI_R12.Text = "CRI R12"
        Me.Label_CRI_R12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R13
        '
        Me.Label_CRI_R13.AutoSize = True
        Me.Label_CRI_R13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R13.Location = New System.Drawing.Point(296, 286)
        Me.Label_CRI_R13.Name = "Label_CRI_R13"
        Me.Label_CRI_R13.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R13.TabIndex = 2
        Me.Label_CRI_R13.Text = "CRI R13"
        Me.Label_CRI_R13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R14
        '
        Me.Label_CRI_R14.AutoSize = True
        Me.Label_CRI_R14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R14.Location = New System.Drawing.Point(296, 308)
        Me.Label_CRI_R14.Name = "Label_CRI_R14"
        Me.Label_CRI_R14.Size = New System.Drawing.Size(104, 22)
        Me.Label_CRI_R14.TabIndex = 2
        Me.Label_CRI_R14.Text = "CRI R14"
        Me.Label_CRI_R14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label_CRI_R15
        '
        Me.Label_CRI_R15.AutoSize = True
        Me.Label_CRI_R15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_CRI_R15.Location = New System.Drawing.Point(296, 330)
        Me.Label_CRI_R15.Name = "Label_CRI_R15"
        Me.Label_CRI_R15.Size = New System.Drawing.Size(104, 23)
        Me.Label_CRI_R15.TabIndex = 2
        Me.Label_CRI_R15.Text = "CRI R15"
        Me.Label_CRI_R15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabPageSpectrum
        '
        Me.TabPageSpectrum.Controls.Add(Me.ChartSpectrum)
        Me.TabPageSpectrum.Location = New System.Drawing.Point(4, 22)
        Me.TabPageSpectrum.Name = "TabPageSpectrum"
        Me.TabPageSpectrum.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageSpectrum.Size = New System.Drawing.Size(740, 359)
        Me.TabPageSpectrum.TabIndex = 1
        Me.TabPageSpectrum.Text = "Spectrum"
        Me.TabPageSpectrum.UseVisualStyleBackColor = True
        '
        'ChartSpectrum
        '
        ChartArea1.AxisX.Crossing = -1.7976931348623157E+308R
        ChartArea1.AxisX.LabelStyle.Interval = 50.0R
        ChartArea1.AxisX.MajorGrid.Interval = 50.0R
        ChartArea1.AxisX.MajorTickMark.Interval = 50.0R
        ChartArea1.AxisX.Maximum = 780.0R
        ChartArea1.AxisX.Minimum = 380.0R
        ChartArea1.AxisX.MinorGrid.Enabled = True
        ChartArea1.AxisX.MinorGrid.Interval = 5.0R
        ChartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.LightGray
        ChartArea1.AxisY.MinorGrid.Enabled = True
        ChartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.LightGray
        ChartArea1.Name = "ChartAreaSpectrum"
        Me.ChartSpectrum.ChartAreas.Add(ChartArea1)
        Me.ChartSpectrum.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartSpectrum.Location = New System.Drawing.Point(3, 3)
        Me.ChartSpectrum.Name = "ChartSpectrum"
        Series1.ChartArea = "ChartAreaSpectrum"
        Series1.Name = "Dummy"
        Series1.Points.Add(DataPoint1)
        Me.ChartSpectrum.Series.Add(Series1)
        Me.ChartSpectrum.Size = New System.Drawing.Size(734, 353)
        Me.ChartSpectrum.TabIndex = 0
        Me.ChartSpectrum.Text = "Chart1"
        '
        'TabPageTM30
        '
        Me.TabPageTM30.Controls.Add(Me.TableLayoutPanelTM30)
        Me.TabPageTM30.Location = New System.Drawing.Point(4, 22)
        Me.TabPageTM30.Name = "TabPageTM30"
        Me.TabPageTM30.Size = New System.Drawing.Size(740, 359)
        Me.TabPageTM30.TabIndex = 2
        Me.TabPageTM30.Text = "TM-30"
        Me.TabPageTM30.UseVisualStyleBackColor = True
        '
        'TableLayoutPanelTM30
        '
        Me.TableLayoutPanelTM30.ColumnCount = 3
        Me.TableLayoutPanelTM30.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanelTM30.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanelTM30.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanelTM30.Controls.Add(Me.ChartTM30, 1, 1)
        Me.TableLayoutPanelTM30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanelTM30.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanelTM30.Name = "TableLayoutPanelTM30"
        Me.TableLayoutPanelTM30.RowCount = 3
        Me.TableLayoutPanelTM30.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanelTM30.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanelTM30.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanelTM30.Size = New System.Drawing.Size(740, 359)
        Me.TableLayoutPanelTM30.TabIndex = 1
        '
        'ChartTM30
        '
        ChartArea2.AxisX.Interval = 0.5R
        ChartArea2.AxisX.LabelStyle.Enabled = False
        ChartArea2.AxisX.MajorTickMark.Enabled = False
        ChartArea2.AxisX.Maximum = 1.5R
        ChartArea2.AxisX.Minimum = -1.5R
        ChartArea2.AxisY.Interval = 0.5R
        ChartArea2.AxisY.LabelStyle.Enabled = False
        ChartArea2.AxisY.MajorTickMark.Enabled = False
        ChartArea2.AxisY.Maximum = 1.5R
        ChartArea2.AxisY.Minimum = -1.5R
        ChartArea2.Name = "ChartArea1"
        Me.ChartTM30.ChartAreas.Add(ChartArea2)
        Me.ChartTM30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartTM30.Location = New System.Drawing.Point(23, 23)
        Me.ChartTM30.Name = "ChartTM30"
        Series2.BorderWidth = 3
        Series2.ChartArea = "ChartArea1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series2.Color = System.Drawing.Color.Red
        Series2.IsVisibleInLegend = False
        Series2.Name = "SeriesTest"
        Series2.Points.Add(DataPoint2)
        Series3.BorderWidth = 3
        Series3.ChartArea = "ChartArea1"
        Series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series3.Color = System.Drawing.Color.Black
        Series3.Name = "SeriesRefer"
        Series3.Points.Add(DataPoint3)
        Me.ChartTM30.Series.Add(Series2)
        Me.ChartTM30.Series.Add(Series3)
        Me.ChartTM30.Size = New System.Drawing.Size(694, 313)
        Me.ChartTM30.TabIndex = 0
        Me.ChartTM30.Text = "Chart1"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Label_Status, Me.Label_Ring})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 547)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StatusStrip1.Size = New System.Drawing.Size(754, 24)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 1
        '
        'Label_Status
        '
        Me.Label_Status.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.Label_Status.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me.Label_Status.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.Label_Status.Name = "Label_Status"
        Me.Label_Status.Size = New System.Drawing.Size(369, 19)
        Me.Label_Status.Spring = True
        Me.Label_Status.Text = "-"
        '
        'Label_Ring
        '
        Me.Label_Ring.AutoSize = False
        Me.Label_Ring.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.Label_Ring.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me.Label_Ring.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.Label_Ring.Name = "Label_Ring"
        Me.Label_Ring.Size = New System.Drawing.Size(369, 19)
        Me.Label_Ring.Spring = True
        Me.Label_Ring.Text = "-"
        '
        'IntervalTimer
        '
        Me.IntervalTimer.Interval = 500
        '
        'Combo_MeasMethod
        '
        Me.Combo_MeasMethod.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Combo_MeasMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_MeasMethod.FormattingEnabled = True
        Me.Combo_MeasMethod.Items.AddRange(New Object() {"Single Mode", "Continuous Mode"})
        Me.Combo_MeasMethod.Location = New System.Drawing.Point(149, 108)
        Me.Combo_MeasMethod.Name = "Combo_MeasMethod"
        Me.Combo_MeasMethod.Size = New System.Drawing.Size(198, 20)
        Me.Combo_MeasMethod.TabIndex = 3
        '
        'Label_MeasMethod
        '
        Me.Label_MeasMethod.AutoSize = True
        Me.Label_MeasMethod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label_MeasMethod.Location = New System.Drawing.Point(3, 104)
        Me.Label_MeasMethod.Name = "Label_MeasMethod"
        Me.Label_MeasMethod.Size = New System.Drawing.Size(140, 29)
        Me.Label_MeasMethod.TabIndex = 4
        Me.Label_MeasMethod.Text = "Measuring Method"
        Me.Label_MeasMethod.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frm_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 571)
        Me.Controls.Add(Me.LayoutPanel_Form)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(770, 590)
        Me.Name = "frm_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SEKONIC C-7000 SDK Sample Program"
        Me.LayoutPanel_Form.ResumeLayout(False)
        Me.Group_Configuration.ResumeLayout(False)
        Me.LayoutPanel_Condition.ResumeLayout(False)
        Me.LayoutPanel_Condition.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPageText.ResumeLayout(False)
        Me.LayoutPanel_Result.ResumeLayout(False)
        Me.LayoutPanel_Result.PerformLayout()
        Me.TabPageSpectrum.ResumeLayout(False)
        CType(Me.ChartSpectrum, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageTM30.ResumeLayout(False)
        Me.TableLayoutPanelTM30.ResumeLayout(False)
        CType(Me.ChartTM30, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LayoutPanel_Form As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Group_Configuration As System.Windows.Forms.GroupBox
    Friend WithEvents LayoutPanel_Condition As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label_FieldViewe As System.Windows.Forms.Label
    Friend WithEvents Label_MeasuringMode As System.Windows.Forms.Label
    Friend WithEvents Label_ExposureTime As System.Windows.Forms.Label
    Friend WithEvents Label_ShutterSpeed As System.Windows.Forms.Label
    Friend WithEvents Combo_FieldView As System.Windows.Forms.ComboBox
    Friend WithEvents Combo_MeasuringMode As System.Windows.Forms.ComboBox
    Friend WithEvents Combo_ShutterSpeed As System.Windows.Forms.ComboBox
    Friend WithEvents LayoutPanel_Result As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label_Tcp As System.Windows.Forms.Label
    Friend WithEvents Result_Tcp As System.Windows.Forms.Label
    Friend WithEvents Label_Delta_uv As System.Windows.Forms.Label
    Friend WithEvents Label_Illmi As System.Windows.Forms.Label
    Friend WithEvents Label_PeakWaveLength As System.Windows.Forms.Label
    Friend WithEvents Label_X As System.Windows.Forms.Label
    Friend WithEvents Label_Y As System.Windows.Forms.Label
    Friend WithEvents Label_CIE1931x As System.Windows.Forms.Label
    Friend WithEvents Label_Z As System.Windows.Forms.Label
    Friend WithEvents Label_CIE1931y As System.Windows.Forms.Label
    Friend WithEvents Label_CIE1931z As System.Windows.Forms.Label
    Friend WithEvents Label_CIE1976u As System.Windows.Forms.Label
    Friend WithEvents Label_CIE1976v As System.Windows.Forms.Label
    Friend WithEvents Label_DWL As System.Windows.Forms.Label
    Friend WithEvents Label_Purity As System.Windows.Forms.Label
    Friend WithEvents Label_PPFD As System.Windows.Forms.Label
    Friend WithEvents Result_Delta_uv As System.Windows.Forms.Label
    Friend WithEvents Result_Illmi As System.Windows.Forms.Label
    Friend WithEvents Result_PeakWaveLength As System.Windows.Forms.Label
    Friend WithEvents Result_X As System.Windows.Forms.Label
    Friend WithEvents Result_Y As System.Windows.Forms.Label
    Friend WithEvents Result_Z As System.Windows.Forms.Label
    Friend WithEvents Result_CIE1931x As System.Windows.Forms.Label
    Friend WithEvents Result_CIE1931y As System.Windows.Forms.Label
    Friend WithEvents Result_CIE1931z As System.Windows.Forms.Label
    Friend WithEvents Result_CIE1976u As System.Windows.Forms.Label
    Friend WithEvents Result_CIE1976v As System.Windows.Forms.Label
    Friend WithEvents Result_DWL As System.Windows.Forms.Label
    Friend WithEvents Result_Purity As System.Windows.Forms.Label
    Friend WithEvents Result_PPFD As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_Ra As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R1 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R2 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R3 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R4 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R5 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R6 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R7 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R8 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R9 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R10 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R11 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R12 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R13 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R14 As System.Windows.Forms.Label
    Friend WithEvents Result_CRI_R15 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_Ra As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R1 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R2 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R3 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R4 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R5 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R6 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R7 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R8 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R9 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R10 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R11 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R12 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R13 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R14 As System.Windows.Forms.Label
    Friend WithEvents Label_CRI_R15 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label_Ring As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents IntervalTimer As System.Windows.Forms.Timer
    Friend WithEvents Button_Connect As System.Windows.Forms.Button
    Friend WithEvents Button_DisConnect As System.Windows.Forms.Button
    Friend WithEvents Button_MeasStart As System.Windows.Forms.Button
    Friend WithEvents Button_StandbyStop As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPageText As TabPage
    Friend WithEvents TabPageSpectrum As TabPage
    Friend WithEvents TabPageTM30 As TabPage
    Friend WithEvents ChartSpectrum As DataVisualization.Charting.Chart
    Friend WithEvents ChartTM30 As DataVisualization.Charting.Chart
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Result_TLMF As Label
    Friend WithEvents Result_TLCI As Label
    Friend WithEvents Result_SSI2 As Label
    Friend WithEvents Result_SSI1 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Result_SSId As Label
    Friend WithEvents Result_SSIt As Label
    Friend WithEvents Result_TM30Rg As Label
    Friend WithEvents Result_TM30Rf As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TableLayoutPanelTM30 As TableLayoutPanel
    Friend WithEvents Combo_ExposureTime As ComboBox
    Friend WithEvents Label_MeasMethod As Label
    Friend WithEvents Combo_MeasMethod As ComboBox
End Class
