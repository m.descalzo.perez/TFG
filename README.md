# Engineering of a system to emit visual stimuli to patients with migraine

En aquest repositori es troben diferents fitxers relacionats amb el treball de final del grau d'Enginyeria Electrònica de Telecomunicacions de Miguel Descalzo Pérez. Grau impartit a la Facultat de Física de la Universitat de Barcelona.

Aquest treball està sent realitzat durant el semestre de Primavera del 2020 i actualment es troba **acabat**.

El treball forma part d'una col·laboració entre el Vall d'Hebron Institut de Recerca ([VHIR](http://www.vhir.org)) i la Universitat de Barcelona ([UB](https://www.ub.edu/web/ub/ca/))

![logo UB](./images/logo-ub.jpg)			![logo VHIR](./images/logo-VHIR.png)

### Objectives for the system

- ​	**1.** **Achieve visual stimulation of patients and controls to obtain their discomfort perception thresholds**
  - 1.1 Find a light source able to emit enough intensity. Past studies have gone as far as 23000 lux.
  - 1.2 Find a source able to change light intensity in small steps. Past studies have gone as precise as 5 lx/step.
  - 1.3 Get a machine capable of presenting the same stimuli with time independence. We want to 	calibrate the system once and know the stimuli presented from there on will be a good one.

- ​	**2. Make sure the system can’t cause any type of ocular injury.**
  - 2.1 Get information about potential eye injuries
  - 2.2 Get information about  how to prevent the injuries
  - 2.3 Program precautions to prevent creating hazardous stimuli by error.
  - 2.4 Actively measure the emitted light to ensure the safety of the system when the test is 	running.

## Situació actual del projecte

**old**

La solució triada que està desenvolupant-se actualment per a complir **l'objectiu 1** és fer servir un projector convencional com a font de llum controlada amb un programa escrit en _Matlab_. Per a conèixer la luminància que el projector emet en cada moment es fa servir el SPECTROMASTER C-7000 de SEKONIC.

SEKONIC ofereix un SDK de _VisualBasic_ per a plataformes Windows que permet controlar el seu C-7000. Per aquest motiu es fa servir _VisualBasic_ per a realitzar les mesures de luminància. Aquest SDK del que s'ha fet referència es troba a la carpeta C7000_SDK d'aquest mateix repositori.

S'ha programat un controlador pel C-7000 en VB.NET i una comunicació via TCP/IP que permet des de _Matlab_ encomanar la presa de mesures i lectura de resultats.



Actualment s'esta treballant en refer el _script_ per a calibrar la luminància d'una font controlada (el projector), ja que s'ha perdut l'acces a la màquina on es tenia guardat aquest.

De manera global, s'està treballant en la creació d'un mètode de calibració del sistema, és a dir, una manera d'aconseguir que el projector tingui la mateixa emitància lluminosa al llarg del temps i que es pugui seleccionar de manera fiable la luminància desitjada.

**old**